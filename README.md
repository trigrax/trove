Here is some of the custom code and configs I use on my desktop.
It's all built for myself, without much concern for reusability. Still, some of it might turn out useful to others.
If you want help with this, feel free to ask me.


## Some highlights

* `bin`: This is on my `$PATH`.

  * `debtweak`: Locally patch Debian packages.
  
  * `jl`: JSON-line logs formatter, like various existing `jl`/`klp` tools but nicer.
  
  * `jlselect`: Quick filters for JSON-line logs (best in a pipeline after `jlcoerce`).

  * `musicprep`: Convert and organize downloaded music files.

  * `wshuf`: Like `shuf`, but earlier lines or newer files get more weight.

  * `xw`: Run a command detached from the terminal and environment.

* `py`: This is on my `$PYTHONPATH`.

  * `srub.py`: No-frills dialogs and helpers for GTK 3.

  * `trapdoor.py`: Helps with building Python programs interactively (à la Lisp) via Emacs's Python shell support.

  * `tarusa`: Map exploration tool.
  
  * `web`: WSGI scripts for the local Web server.
    
    * `opensearch.py`: Generate OpenSearch descriptions for custom search engines, e.g. for Firefox.
  
    * `quickdoc.py`: Universal stateless keyword search in document outlines.

  * `windir.py`: Custom window tiling and control.

* `setup/share`: Configs and miscellanea.

  * `emacs/init.el`: Some Emacs functions.
  
  * `www`: Local Web server.

  * `genmon`: Scripts for the Xfce4 genmon plugin.

    * `watchping`: See when your train is within mobile Internet service zone.
      (Requires `debtweak/xfce4-genmon-plugin.diff`.)
