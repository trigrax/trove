import ast
import dataclasses
import html
import json
import os
from pathlib import Path
import pkgutil
import re
import subprocess
import urllib.parse

import gi
import xdg.BaseDirectory
import yaml

gi.require_version('GLib', '2.0')
gi.require_version('Gdk', '3.0')
gi.require_version('Gio', '2.0')
gi.require_version('Gtk', '3.0')
gi.require_version('WebKit2', '4.0')
from gi.repository import GLib, Gdk, Gio, Gtk, WebKit2

import srub
import trapdoor     # pylint: disable=unused-import
import util.gtk
import util.logs
import util.url
import windir

from lowbrow import completion

log = util.logs.getLogger(__name__)

cache = Path(xdg.BaseDirectory.save_cache_path('lowbrow'))

app = Gtk.Application.get_default


class Application(Gtk.Application):

    def __init__(self, *args, **kwargs):
        GLib.set_prgname('lowbrow')
        super().__init__(*args, application_id=(None if os.environ.get('DEBUG') else 'org.codeberg.trigrax.lowbrow'),
                         flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE, **kwargs)

        self.add_main_option(long_name='place', short_name=0,
                             flags=GLib.OptionFlags.NONE, arg=GLib.OptionArg.STRING,
                             description='Use window at the specified place', arg_description=None)

        self.add_main_option(long_name='activate-existing', short_name=0,
                             flags=GLib.OptionFlags.NONE, arg=GLib.OptionArg.NONE,
                             description='Activate existing tab, if any', arg_description=None)

        self.sites_config = self.shortcuts = self.user_content_manager = None
        self.web_contexts = {}
        self.completion = {}

    @property
    def c(self):
        return self.get_active_window()

    def do_startup(self, *_):   # pylint: disable=arguments-differ
        srub.install_excepthook()
        srub.maybe_dark()
        Gtk.Application.do_startup(self)
        srub.set_accels(self, [
            ('<Alt>F5', self.load_config),
            ('<Alt>F11', util.gtk.toggle_dark_theme),
        ])
        self.user_content_manager = WebKit2.UserContentManager()
        self.user_content_manager.register_script_message_handler('g')
        self.user_content_manager.connect('script-message-received::g', self.on_script_message_received)
        self.load_config()

    def load_config(self):
        self.sites_config = {}
        self.shortcuts = {}
        self.completion = {}
        self.user_content_manager.remove_all_style_sheets()

        for d in xdg.BaseDirectory.load_config_paths('lowbrow'):
            for path in Path(d).iterdir():
                if path.name == 'fonts.conf':
                    os.environ['FONTCONFIG_FILE'] = str(path)

                if path.name == 'sites.yaml':
                    self.add_sites_config(path)

                if path.name == 'shortcuts.yaml':
                    self.shortcuts |= yaml.safe_load(path.read_text())

                if path.name == 'user.css':
                    self.add_style_sheets(path)

        for win in self.get_windows():
            win.reload_settings()

    def add_sites_config(self, path):
        for realm, cfg in yaml.safe_load(path.read_text()).items():
            base = self.sites_config[inherit].copy() if (inherit := cfg.pop('inherit', '.')) else {}
            self.sites_config[realm] = base | cfg

    def add_style_sheets(self, path):
        defines = {}
        for part in path.read_text().split('\n}\n')[:-1]:
            head, _, body = part.partition('{\n')
            _, _, head = head.rpartition('*/')
            body = re.sub(r'@include (.+)', lambda m: defines[m[1]], body)
            match head.split():
                case ('@define', name):
                    defines[name] = body
                case ('@for', *words):
                    allow = [ast.literal_eval(qpat) for word in words if word == (qpat := word.removeprefix('!'))]
                    block = [ast.literal_eval(qpat) for word in words if word != (qpat := word.removeprefix('!'))]
                    self.user_content_manager.add_style_sheet(WebKit2.UserStyleSheet(
                        body,
                        WebKit2.UserContentInjectedFrames.ALL_FRAMES, WebKit2.UserStyleLevel.USER,
                        allow or None, block))

    def do_command_line(self, cmdline):     # pylint: disable=arguments-differ
        arguments = cmdline.get_arguments()
        options = cmdline.get_options_dict().end().unpack()
        log.debug('Application.command_line: arguments=%r, options=%r', arguments, options)
        win = windir.best_window(self, Window, options, default_place=windir.bulk)
        for uri in arguments[1:]:
            if 'activate-existing' in options and win.activate_tab(uri):
                continue
            win.open_new_tab(uri=preprocess_uri(uri))
        if win.notebook.get_n_pages() == 0:
            win.open_new_tab()
        win.present()
        return 0

    def web_context(self, options):
        if (ctx := self.web_contexts.get(k := dataclasses.astuple(options))) is None:
            ctx = self.web_contexts[k] = make_web_context(options)
        return ctx

    def web_context_for(self, uri):
        return self.web_context(WebContextOptions(**self.site_config(uri).get('web_context_options', {})))

    def site_config(self, uri):
        for realm in reversed(util.url.realms(uri or '')):  # pylint: disable=bad-reversed-sequence
            if cfg := self.sites_config.get(realm):
                break
        else:
            cfg = self.sites_config['.']
        log.debug('site_config: %s: %r', uri, cfg)
        return cfg

    def on_script_message_received(self, _, v):
        obj = json.loads(v.get_js_value().to_json(indent=0))
        log.debug('on_script_message_received: %r', obj)
        getattr(self, f'handle_message_{obj.pop("topic")}')(**obj)

    def handle_message_new_tab(self, **kwargs):
        self.get_active_window().open_new_tab(**kwargs)


class Window(Gtk.ApplicationWindow):

    def __init__(self, application, **kwargs):
        super().__init__(application=application, **kwargs)
        self.app = application
        util.gtk.destroy_on_delete(self)

        self.notebook = Gtk.Notebook(expand=True, show_tabs=False, scrollable=True, enable_popup=True,
                                     group_name='Lowbrow')
        self.notebook.connect('switch-page', self.on_switch_page)
        self.notebook.connect('page-removed', self.on_page_removed)
        self.notebook.connect('create-window', self.on_create_window)
        self.add(self.notebook)
        self.notebook.show_all()

        srub.set_accels(self, [
            ('F10', self.tabs_menu),
            ('<Control>t', self.open_new_tab),
            ('<Control><Alt>t', self.open_new_tab_with_options),
            ('<Control>w', self.close_tab),
            ('<Control><Alt>KP_Left', self.toss_tab_to_left),
            ('<Control><Alt>KP_Up', self.toss_tab_to_bulk),
            ('<Control>d', self.detach_tab),
            ('<Control>e', self.open_externally),
            ('<Control>l', self.toggle_location_bar),
            ('KP_Begin', self.locate_up),
            ('<Alt>KP_Begin', self.clone_location),
            ('<Control>KP_Prior', self.previous_tab),
            ('<Control>KP_Next', self.next_tab),
            ('<Control>KP_Home', self.first_tab),
            ('<Control>KP_End', self.last_tab),
            ('F5', self.reload),
            ('<Shift>F5', self.reload_bypass_cache),
            ('<Control>Escape', self.stop_loading),
            ('<Alt>Tab', self.complete_uri),
            ('<Alt>Left', self.go_back),
            ('<Alt>Right', self.go_forward),
            ('F6', self.toggle_caret),
            ('<Control>minus', self.zoom_out),
            ('<Control>equal', self.zoom_in),
            ('F7', self.toggle_zoom_text_only),
            ('<Alt><Control>minus', self.decrease_min_font_size),
            ('<Alt><Control>equal', self.increase_min_font_size),
            ('<Control>f', self.find),
            ('<Alt>j', self.hit_a_hint),
            ('<Alt>l', self.hit_a_hint, 'new-tab'),
            ('KP_Insert', self.clear_selection),
            ('<Control>r', self.readable),
            ('F2', self.run_site_action, 'F2'),
            ('F3', self.run_site_action, 'F3'),
            ('F4', self.run_site_action, 'F4'),
        ])

    def tabs_menu(self):
        menu = Gtk.Menu()
        cur = self.notebook.get_current_page()
        for i in range(self.notebook.get_n_pages()):
            tab = self.notebook.get_nth_page(i)
            title = tab.view.get_title()
            num = (num := str(i + 1))[:-1] + '_' + num[-1]
            menu.add(item := Gtk.MenuItem.new_with_mnemonic(f'{num}\t{title}'))
            if i == cur:
                (label := item.get_child()).set_markup('<b>' + item.get_label() + '</b>')
                label.set_use_underline(True)   # seems to get reset by set_markup
            item.connect('activate', self.on_tabs_menu_item_activate, i)
        menu.show_all()
        menu.popup_at_widget(self.notebook, Gdk.Gravity.SOUTH, Gdk.Gravity.SOUTH, None)

    def on_tabs_menu_item_activate(self, _, i):
        self.notebook.set_current_page(i)

    def open_new_tab(self, uri=None, go=True, web_context=...):
        tab = Tab(self.app, self.app.web_context_for(uri) if web_context is ... else web_context)
        tab.show_all()
        self.notebook.set_current_page(self.notebook.insert_page(tab, Gtk.Label(),
                                                                 self.notebook.get_current_page() + 1))
        self.notebook.set_tab_reorderable(tab, True)
        self.notebook.set_tab_detachable(tab, True)
        if uri and go:
            tab.go_to_uri(uri)
        else:
            tab.entry.set_text(uri or '')
            tab.entry.grab_focus()
            tab.entry.set_position(-1)
        return tab

    def open_new_tab_with_options(self):
        if options := srub.FormDialog(WebContextOptions, parent=self).do():
            self.open_new_tab(web_context=self.app.web_context(options))

    def activate_tab(self, uri):
        for i, tab in enumerate(self.notebook.get_children()):
            if tab.view.get_uri().startswith(uri):
                self.notebook.set_current_page(i)
                return True
        return False

    def close_tab(self):
        tab = self.current_tab()
        self.notebook.remove_page(self.notebook.get_current_page())
        tab.destroy()

    def current_tab(self):
        return self.notebook.get_nth_page(self.notebook.get_current_page())

    def reload_settings(self):
        for tab in self.notebook.get_children():
            tab.reload_settings()

    @property
    def c(self):
        return self.current_tab()

    def previous_tab(self):
        self.notebook.prev_page()

    def next_tab(self):
        self.notebook.next_page()

    def first_tab(self):
        self.notebook.set_current_page(0)

    def last_tab(self):
        self.notebook.set_current_page(-1)

    def on_switch_page(self, _notebook, tab, i):
        log.debug('on_switch_page: %r', i)
        self.set_title(tab.view.get_title() or '')
        tab.view.grab_focus()

    def on_page_removed(self, notebook, _tab, _i):
        if notebook.get_n_pages() == 0:
            self.destroy()

    def on_create_window(self, _notebook, _tab, _x, _y):
        return self.detach_target()

    def toss_tab_to_left(self):
        self.toss_tab_to(windir.left)

    def toss_tab_to_bulk(self):
        self.toss_tab_to(windir.bulk)

    def toss_tab_to(self, place):
        tab = self.current_tab()
        self.notebook.detach_tab(tab)
        if windir.is_placed(self, windir.left):
            self.iconify()
        target = windir.best_window(self.app, self.__class__, default_place=place)
        target.notebook.append_page(tab)
        target.notebook.set_current_page(-1)
        target.present()

    def detach_tab(self):
        tab = self.current_tab()
        self.notebook.detach_tab(tab)
        self.detach_target().append_page(tab)
        tab.update_title()

    def detach_target(self):
        (win := Window(self.app)).present()
        windir.put(win, windir.bulk)
        return win.notebook

    def update_title(self):
        self.set_title(self.current_tab().view.get_title() or '')

    def open_externally(self):
        tab = self.current_tab()
        open_externally(tab.view.get_uri())
        self.notebook.remove_page(self.notebook.page_num(tab))
        tab.destroy()

    def toggle_location_bar(self):          self.current_tab().toggle_location_bar()
    def locate_up(self):                    self.current_tab().locate_up()
    def clone_location(self):               self.current_tab().clone_location()
    def reload(self):                       self.current_tab().view.reload()
    def reload_bypass_cache(self):          self.current_tab().view.reload_bypass_cache()
    def stop_loading(self):                 self.current_tab().view.stop_loading()
    def complete_uri(self):                 self.current_tab().complete_uri()
    def go_back(self):                      self.current_tab().view.go_back()
    def go_forward(self):                   self.current_tab().view.go_forward()
    def find(self):                         self.current_tab().find()
    def hit_a_hint(self, action='default'): self.current_tab().hit_a_hint(action)
    def clear_selection(self):              self.current_tab().clear_selection()
    def readable(self):                     self.current_tab().readable()
    def run_site_action(self, key):         self.current_tab().run_site_action(key)
    def zoom_out(self):                     self.current_tab().view.props.zoom_level -= 0.1
    def zoom_in(self):                      self.current_tab().view.props.zoom_level += 0.1
    def decrease_min_font_size(self):       self.current_tab().settings.props.minimum_font_size -= 1
    def increase_min_font_size(self):       self.current_tab().settings.props.minimum_font_size += 1
    def toggle_caret(self):                 self.current_tab().settings.props.enable_caret_browsing ^= True
    def toggle_zoom_text_only(self):        self.current_tab().settings.props.zoom_text_only ^= True


class Tab(Gtk.Box):

    def __init__(self, application, web_context, **kwargs):
        super().__init__(orientation=Gtk.Orientation.VERTICAL, expand=True, **kwargs)
        self.app = application

        self.entry = Gtk.Entry(margin=6)
        self.entry.connect('activate', self.on_entry_activate)
        self.entry.connect('key-press-event', self.on_entry_key_press_event)
        self.add(self.entry)

        self.view = WebKit2.WebView(expand=True, web_context=web_context, user_content_manager=self.app.user_content_manager)
        self.view.connect('notify::uri', self.on_notify_uri)
        self.view.connect('notify::title', self.on_notify_title)
        self.view.connect('notify::is-loading', self.on_notify_load_progress)
        self.view.connect('notify::estimated-load-progress', self.on_notify_load_progress)
        self.view.connect('decide-policy', self.on_decide_policy)
        self.view.get_find_controller().connect('found-text', self.on_found_text)
        self.view.get_find_controller().connect('failed-to-find-text', self.on_failed_to_find_text)
        self.add(self.view)
        self.site_config = self.settings = None
        self.reload_settings()

        self.search_bar = Gtk.SearchBar(show_close_button=True)
        self.add(self.search_bar)
        self.search_bar.add(search_box := Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=24))
        self.search_entry = Gtk.SearchEntry()
        self.search_entry.connect('search-changed', self.on_search_changed)
        self.search_entry.connect('previous-match', self.on_previous_match)
        self.search_entry.connect('next-match', self.on_next_match)
        self.search_entry.connect('stop-search', self.on_stop_search)
        search_box.add(self.search_entry)
        self.search_bar.connect_entry(self.search_entry)
        self.search_label = Gtk.Label(width_request=32)
        search_box.add(self.search_label)

    @property
    def win(self):
        return self.get_toplevel()

    def on_entry_activate(self, entry):
        if expanded := self.app.shortcuts.get((text := entry.get_text()).lower()):
            entry.set_text(expanded)
            entry.set_position(-1)
        else:
            self.go_to_uri(preprocess_uri(text))

    def on_entry_key_press_event(self, entry, event):
        name = Gdk.keyval_name(event.keyval)

        if name == 'Escape':
            if entry.get_text() == (uri := self.view.get_uri()):
                entry.hide()
                self.view.grab_focus()
            elif uri:
                entry.set_text(uri)
            return True

        if name == 'KP_Begin':
            self.locate_up()
            return True

        return False

    def on_decide_policy(self, _, decision, decision_type):
        log.debug('on_decide_policy: decision=%r, decision_type=%r', decision, decision_type)

        if decision_type is WebKit2.PolicyDecisionType.NAVIGATION_ACTION:
            log.debug('on_decide_policy: mouse_button=%r, modifiers=%r', decision.props.mouse_button, decision.props.modifiers)
            if method := {Gdk.ModifierType.CONTROL_MASK: self.win.open_new_tab}.get(decision.props.modifiers):
                decision.ignore()
                method(uri=decision.props.request.props.uri)
                return True

        if decision_type is WebKit2.PolicyDecisionType.NEW_WINDOW_ACTION:
            decision.ignore()
            self.win.open_new_tab(uri=decision.props.request.props.uri)
            return True

        return False

    def go_to_uri(self, uri):
        self.view.load_uri(uri)
        self.entry.hide()
        self.view.grab_focus()

    def on_notify_uri(self, view, _prop):
        self.entry.set_text(view.get_uri())
        self.reload_settings()

    def on_notify_load_progress(self, view, _prop):
        self.entry.set_progress_fraction(0.1 + 0.8 * view.get_estimated_load_progress()
                                         if view.is_loading() else 0)

    def reload_settings(self):
        cfg = self.app.site_config(self.view.get_uri())
        if cfg is self.site_config:
            return
        log.debug('reload_settings: config changed')
        self.site_config = cfg
        self.settings = WebKit2.Settings()
        for k, v in self.site_config.items():
            if hasattr(self.settings.props, k):
                setattr(self.settings.props, k, v)
        self.view.set_settings(self.settings)

    def on_notify_title(self, _view, _prop):
        self.update_title()

    def update_title(self):
        self.win.notebook.set_tab_label_text(self, self.view.get_title())
        self.win.update_title()

    def toggle_location_bar(self):
        if self.entry.is_focus():
            self.entry.hide()
            self.view.grab_focus()
        else:
            self.entry.show()
            self.entry.grab_focus()

    def locate_up(self):
        already_visible = self.entry.is_visible()
        self.entry.show()
        self.entry.grab_focus()
        self.entry.set_position(-1)
        if already_visible:
            self.entry.emit('delete-from-cursor', Gtk.DeleteType.WORD_ENDS, -1)

    def clone_location(self):
        new_tab = self.win.open_new_tab(web_context=self.view.get_context())
        new_tab.entry.set_text(self.entry.get_text())
        new_tab.entry.set_position(-1)

    def complete_uri(self):
        completion.complete(self.app, self.entry)

    def find(self):
        self.search_bar.set_search_mode(True)
        self.search_entry.grab_focus()

    def on_search_changed(self, entry):
        if needle := entry.get_text():
            self.view.get_find_controller().search(
                needle,
                WebKit2.FindOptions.CASE_INSENSITIVE if needle.islower() else WebKit2.FindOptions.NONE,
                max_match_count=9000)
            self.search_label.set_text('...')

    def on_found_text(self, _controller, match_count):
        self.search_label.set_text(str(match_count) if match_count < GLib.MAXUINT else '>9000')

    def on_failed_to_find_text(self, _controller):
        self.search_label.set_text('—')

    def on_previous_match(self, _entry):
        self.view.get_find_controller().search_previous()

    def on_next_match(self, _entry):
        self.view.get_find_controller().search_next()

    def on_stop_search(self, _entry):
        self.view.get_find_controller().search_finish()
        self.search_label.set_text('')
        self.view.grab_focus()

    def call_javascript(self, resource_name, *args, callback=None):
        if callback:
            def real_callback(_, task):
                log.debug('call_javascript: real_callback: %r', task)
                callback(self.view.evaluate_javascript_finish(task))
        else:
            real_callback = None

        js = resource(resource_name).rstrip() + '(' + json.dumps(args)[1:-1] + ')'
        log.debug('call_javascript: %r', js)
        self.view.evaluate_javascript(js, len(js.encode()), world_name=None, source_uri='about:lowbrow',
                                      cancellable=None, callback=real_callback)

    def hit_a_hint(self, action='default'):
        self.call_javascript('hint.js', action)

    def clear_selection(self):
        self.call_javascript('clear-selection.js')

    def readable(self):
        target_path = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD).wait_for_text()
        self.call_javascript('readable.js', target_path, resource('readable.css'))

    def run_site_action(self, key):
        if js := self.app.site_config(self.view.get_uri()).get('actions', {}).get(key):
            log.debug('run_site_action: %s: %s', key, js)
            self.view.run_javascript(js)


def resource(name):
    return pkgutil.get_data('lowbrow', name).decode()


def preprocess_uri(uri):
    if uri != (uri := uri.lstrip('@')):
        uri = subprocess.run(uri, shell=True, capture_output=True, text=True, check=True).stdout.rstrip()
    if uri.startswith('/'):
        uri = 'file://' + uri
    if not re.match(r'^[a-z+]+:', uri):
        uri = 'http://' + uri
    return uri


def make_web_context(options):
    data_manager = WebKit2.WebsiteDataManager(base_cache_directory=str(cache / 'cache'),
                                              base_data_directory=str(cache / 'data'))
    data_manager.get_cookie_manager().set_persistent_storage(str(cache / 'cookies'),
                                                             WebKit2.CookiePersistentStorage.TEXT)
    if options.ignore_tls_errors:
        data_manager.set_tls_errors_policy(WebKit2.TLSErrorsPolicy.IGNORE)
    ctx = WebKit2.WebContext.new_with_website_data_manager(data_manager)
    if options.proxy_port:
        ctx.set_network_proxy_settings(WebKit2.NetworkProxyMode.CUSTOM,
                                       WebKit2.NetworkProxySettings(f'http://localhost:{options.proxy_port}'))
    ctx.register_uri_scheme('http+post', http_post_handler)
    ctx.register_uri_scheme('https+post', https_post_handler)
    return ctx


@dataclasses.dataclass
class WebContextOptions:
    proxy_port: int = None
    ignore_tls_errors: bool = False


def http_post_handler(req, scheme='http'):
    log.info('http_post_handler: %s', uri := req.get_uri())
    (target, payload) = uri.split('!')
    target = target.replace(f'{scheme}+post://', f'{scheme}://')
    inputs = ''.join(f'<input type=hidden name="{html.escape(name)}" value="{html.escape(value)}">'
                     for (name, value) in urllib.parse.parse_qsl(payload))
    page = ('''<html>
                 <head><script>window.onload = function() { document.forms[0].submit() }</script></head>
                 <body><form method=post action="''' + html.escape(target) + '">' + inputs + '''</form></body>
               </html>''').encode()
    req.finish(Gio.MemoryInputStream.new_from_data(page), stream_length=len(page),
               content_type='text/html; charset=utf-8')

def https_post_handler(req):
    http_post_handler(req, scheme='https')


def open_externally(uri):
    subprocess.run(('xw', 'xdg-open', uri), check=True)
