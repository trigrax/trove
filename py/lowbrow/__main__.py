import sys

import lowbrow.app


if __name__ == '__main__':
    lowbrow.app.Application().run(sys.argv)
