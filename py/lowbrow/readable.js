(function (path, css) {
  var target = document.evaluate(path, document, null, XPathResult.ANY_UNORDERED_NODE_TYPE, null).singleNodeValue;
  document.body.replaceChildren(...target.children);
  document.head.appendChild(document.createElement('style')).innerHTML = css;
})
