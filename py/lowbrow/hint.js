(function(action) {
  const hintCharacters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'.split('');
  const hints = [];

  function getClickableElements() {
    return Array.from(document.querySelectorAll('a, button, input, textarea, [role="button"]'))
      .filter(el => {
        const rect = el.getBoundingClientRect();
        return el.checkVisibility({contentVisibilityAuto: true, opacityProperty: true, visibilityProperty: true}) &&
          rect.top >= 0 && rect.left >= 0 &&
          rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
          rect.right <= (window.innerWidth || document.documentElement.clientWidth);
      });
  }

  function createHintElement(char, target) {
    const hintElement = document.createElement('span');
    hintElement.textContent = char;
    hintElement.style.position = 'absolute';
    hintElement.style.backgroundColor = 'yellow';
    hintElement.style.color = 'black';
    hintElement.style.fontSize = '12px';
    hintElement.style.padding = '2px';
    hintElement.style.border = '1px solid black';
    hintElement.style.zIndex = 999;

    const rect = target.getBoundingClientRect();
    hintElement.style.top = `${rect.top + window.scrollY}px`;
    hintElement.style.left = `${rect.left + window.scrollX}px`;

    document.body.appendChild(hintElement);
    return hintElement;
  }

  function showHints(elements, firstChar) {
    elements.forEach((el, i) => {
      const origin = firstChar ? hintCharacters.indexOf(firstChar) : 0;
      const hintChar = hintCharacters[(origin + i) % hintCharacters.length];
      const hintElement = createHintElement(hintChar, el);
      hints.push({char: hintChar, element: el, hintElement: hintElement});
    });
    document.addEventListener('keydown', handleKeydown, {once: true});
  }

  function removeHints() {
    hints.forEach(hint => hint.hintElement.remove());
    hints.length = 0;
  }

  function handleKeydown(event) {
    console.debug('handleKeydown', event.key, event.code, event.altKey, event.ctrlKey, event.shiftKey, event.metaKey);
    const char = event.key.toUpperCase();
    const matches = hints.reduce(function(ms, hint) {
      if (char === hint.char || event.code === 'Key' + hint.char) return ms.concat(hint);
      else return ms;
    }, []);
    removeHints();
    if (matches.length > 1) showHints(matches.map(h => h.element), matches[0].char);
    else if (matches.length === 1) click(matches[0].element);
    event.preventDefault();
  }

  function click(el) {
    switch (action) {
    case 'new-tab':
      window.webkit.messageHandlers.g.postMessage({topic: 'new_tab', uri: el.href});
      break;

    default:
      if (el.click) el.click();
      if (el.select) el.select();
      break;
    }
  }

  showHints(getClickableElements());
})
