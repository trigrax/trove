from pathlib import Path
from urllib.parse import urljoin

import bs4
import gi
import requests
import requests_file

gi.require_version('GLib', '2.0')
gi.require_version('Gtk', '3.0')
from gi.repository import GLib, Gtk

from util.format import shorten_seq
import util.gtk
import util.logs

log = util.logs.getLogger(__name__)


def complete(app, entry):
    prefix = entry.get_text()
    log.debug('complete: %s', prefix)
    if not (items := app.completion.get(prefix)):
        items = app.completion[prefix] = load(app, prefix)
    setup(entry, prefix, items)


def load(app, prefix):
    log.debug('load: %s', prefix)

    if prefix == '':
        log.info('loaded from shortcuts')
        return list(app.shortcuts.values())

    if (p := prefix.removeprefix('file://')) != prefix and (path := Path(p)).is_dir():
        items = [f'file://{item}' for item in path.rglob('*')]
        log.info('loaded from dir %s: %s', path, shorten_seq(items))
        return items

    return sorted(set(load_html(prefix, prefix)))


def load_html(prefix, url, seen=None):
    if url in (seen := seen or set()):
        return []
    seen.add(url)

    try:
        resp = session.get(url, timeout=5)
        resp.raise_for_status()
        soup = bs4.BeautifulSoup(resp.text, 'html5lib')
    except Exception:
        log.exception('failed to load from HTML %s', url)
        return []

    items = []

    for a in soup.find_all('a'):
        if (href := a.get('href')) and (item := urljoin(resp.url, href)).startswith(prefix) and item not in items:
            items.append(item)
    log.info('loaded from HTML %s: %s', url, shorten_seq(items))

    # Hack for pkgsite
    containers = soup.find_all('section', class_='Homepage-modules') + soup.find_all(class_='UnitDirectories')
    for container in containers:
        for a in container.find_all('a'):
            if href := a.get('href'):
                items.extend(load_html(prefix, urljoin(resp.url, href), seen))
                items.extend(load_html(prefix, urljoin(resp.url, href) + '?tab=imports', seen))

    return items


session = requests.Session()
session.mount('file://', requests_file.FileAdapter())
session.headers['User-Agent'] = 'lowbrow python-requests'


def setup(entry, prefix, items):
    log.debug('setup: %s: %s', prefix, shorten_seq(items))
    completion = Gtk.EntryCompletion(popup_set_width=False, minimum_key_length=0,
                                     model=(store := Gtk.ListStore(str, str)))
    completion.set_text_column(0)    # passing this to the constructor doesn't work
    completion.set_match_func(match_func, normfold(prefix))
    for uri in items:
        store.append((uri, normfold(uri)))

    # Once an entry is selected, close the completion pop-up, otherwise it "eats" the next Alt+Tab.
    completion.connect_after('match-selected', lambda _, _m, _it: completion.get_entry().set_completion(None))

    entry.set_completion(completion)
    entry.set_position(-1)
    entry.emit('insert-at-cursor', ' ')


def match_func(completion, key, it, prefix):
    if not key.startswith(prefix):
        log.debug('match_func: %r has no prefix %r', key, prefix)
        return False

    needle = key.removeprefix(prefix)
    haystack = completion.get_model()[it][1].removeprefix(prefix)
    log.debug('match_func: %r: %r: %r vs. %r', prefix, key, needle, haystack)
    return all(word in haystack for word in GLib.str_tokenize_and_fold(needle)[0])


def normfold(s):
    return GLib.utf8_casefold(norm := GLib.utf8_normalize(s, len(s), GLib.NormalizeMode.DEFAULT), len(norm))
