import dataclasses
from dataclasses import dataclass
from datetime import datetime, timedelta

from munch import munchify
import requests

import util.pango
import util.url

from gornica import berries
from gornica.berries import small

session = requests.Session()


def load(bs, posts):
    by_url = {b.url: b for b in bs if isinstance(b, Post)}
    for new in posts:
        assert new.expires
        if old := by_url.get(new.url):
            for field in dataclasses.fields(old):
                if field.name not in ('tags', 'quality', 'comment', 'trace'):
                    setattr(old, field.name, getattr(new, field.name))
        else:
            bs.insert(0, new)


def fetch_2ch_board(board, keep_for=0):
    root = 'https://2ch.hk/' + board
    resp = session.get(root + '/catalog.json')
    resp.raise_for_status()
    expires = datetime.now() + timedelta(days=keep_for)
    for info in munchify(resp.json()).threads:
        yield Post(url=f'{root}/res/{info.num}.html',
                   title=None,
                   posted=datetime.fromtimestamp(info.timestamp),
                   expires=expires,
                   content=info.comment,
                   replies=info.get('posts_count', 1) - 1,
                   updated=datetime.fromtimestamp(info.lasthit))


@dataclass(eq=False)
class Post(berries.Berry):

    url: str = ...
    source: str = ...
    posted: datetime = ...
    expires: datetime = ...
    content: str = None
    replies: int = None
    updated: datetime = None

    def _before_title(self):
        yield small(util.url.realm(self.source))

        if (age := datetime.now() - self.posted) < timedelta(hours=12):
            t = int(age.total_seconds())
            m = int(t / 60) % 60
            h = int(int(t / 60) / 60)
            yield small(f'{h} hours {m} minutes ago' if h else f'{m} minutes ago')
        else:
            yield small(self.posted.strftime('%a %H:%M'))

    def _after_title(self):
        if self.content:
            yield util.pango.MarkupConverter.convert(self.content)

    def visit(self, app):
        berries.open_browser(berries.firefox, self.url)
        self.trace['visited'] = datetime.now()
        app.save_berries()

    def drop(self, app):
        self.trace['dropped'] = datetime.now()
        app.save_berries()
