from collections import namedtuple
from datetime import datetime
import inspect
import itertools
import os
from pathlib import Path
import random
import subprocess

import gi

gi.require_version('GLib', '2.0')
gi.require_version('GdkPixbuf', '2.0')
gi.require_version('Gio', '2.0')
gi.require_version('Gtk', '3.0')
gi.require_version('Pango', '1.0')
from gi.repository import GLib, GdkPixbuf, Gio, Gtk, Pango

import srub
import trapdoor     # pylint: disable=unused-import
from util import ínspect
import util.edit
import util.gtk
from util.iter import find, first
import util.logs
import util.strings
import util.url
import weighted
import windir

from gornica import berries, picker, music

log = util.logs.getLogger(__name__)

app = Gtk.Application.get_default


class Application(Gtk.Application):

    def __init__(self, *args, **kwargs):
        GLib.set_prgname('gornica')
        super().__init__(*args, application_id=(None if os.environ.get('DEBUG') else 'org.codeberg.trigrax.gornica'),
                         flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE, **kwargs)

        self.add_main_option(long_name='sightsee', short_name=0,
                             flags=GLib.OptionFlags.NONE, arg=GLib.OptionArg.NONE,
                             description='Show sight', arg_description=None)

        self.add_main_option(long_name='flyby', short_name=0,
                             flags=GLib.OptionFlags.NONE, arg=GLib.OptionArg.NONE,
                             description='Show flyby', arg_description=None)

        self.add_main_option(long_name='place', short_name=0,
                             flags=GLib.OptionFlags.NONE, arg=GLib.OptionArg.STRING,
                             description='Use window at the specified place', arg_description=None)

        self.static_berries = self.mood = self.sight_window = None
        self.dynamic_berries = {}
        self.history = History()
        self.sight_window = None
        self.flyby_count = 3

    @property
    def c(self):
        return self.get_active_window()

    def do_startup(self, *_):   # pylint: disable=arguments-differ
        srub.install_excepthook()
        srub.dark()
        Gtk.Application.do_startup(self)
        self.set_accels()
        self.mood = berries.Mood(title=None)
        self.load_berries()
        GLib.timeout_add_seconds(30, self.poll_current_song)
        self.init_sight_window()

    def do_shutdown(self, *_):  # pylint: disable=arguments-differ
        berries.set_backdrop(...)
        Gtk.Application.do_shutdown(self)

    def set_accels(self):
        srub.set_accels(self, [
            ('<Control>grave', self.open_console),
            ('<Alt>F11', self.edit_mood),
            ('<Control>R', self.reload_songs),
            ('<Control>Q', self.quit),
        ])

    def init_sight_window(self):
        if self.sight_window is not None:
            self.sight_window.destroy()
        self.sight_window = SightWindow(self)

    def do_command_line(self, cmdline):     # pylint: disable=arguments-differ
        log.debug('Application.command_line')

        options = cmdline.get_options_dict().end().unpack()

        if 'sightsee' in options:
            self.sight_window.present()
            hide_mouse()
        elif 'flyby' in options:
            self.flyby()
        else:
            windir.best_window(self, Window, options, default_place=windir.center).present()

        return 0

    def open_console(self):
        srub.open_console(Path(__file__).parent / 'app.scratch.py')

    @srub.cancelable
    def load_berries(self):
        with srub.Progresser('загружаю') as pr:
            self.static_berries = berries.load_static()
            berries.load_songs(self.dynamic_berries, pr)

    @srub.cancelable
    def reload_songs(self):
        with srub.Progresser('перезагружаю') as pr:
            berries.load_songs(self.dynamic_berries, pr)

    def save_berries(self):
        berries.dump_static(self.static_berries)

    @property
    def berries(self):
        yield from self.static_berries
        yield from self.dynamic_berries.values()

    def edit_mood(self):
        self.mood.enable(self)

    def add_berry(self, cls=None, **overrides):
        if not cls:
            candidates = ínspect.recursive_subclasses(berries.Berry)
            cls = srub.ListDialog(candidates, [cls.__name__ for cls in candidates], parent=self.c).do()
            if cls is quit:
                return False

        overrides.setdefault('trace', dict(added=datetime.now()))
        if not (berry := srub.FormDialog(cls, overrides=overrides, parent=self.c).do()):
            return False
        berry.trace = berry.trace or {}     # allow skipping `added` by deleting the entire trace
        self.history.record(History.edited, berry)

        self.static_berries.insert(0, berry)
        self.save_berries()
        return True

    def remove_berry(self, berry):
        self.static_berries.remove(berry)
        self.save_berries()

    def bulk_edit_berries(self):
        bs = list(self.get_active_window().stack.get_visible_child().items)
        assert all(b in self.static_berries for b in bs)
        r = util.edit.inplace(bs)
        if r is None:
            return
        for b in r:
            self.static_berries.remove(b)
        self.save_berries()

    def run_wish(self, wish, strict=False):
        return picker.Picker(self.berries, self.history, self.mood, wish, strict)

    def get_preset(self, title):
        for b in self.static_berries:
            if isinstance(b, berries.Preset) and b.title == title:
                return b
        return None

    def flatten(self, wbs):
        return weighted.interleave([
            (w, (self.flatten(self.run_wish(b.wish).weighted)
                 if isinstance(b, berries.Preset) and b.wish
                 else [b]))
            for (w, b) in wbs
        ])

    def pick_margin_fillers(self, pic):
        friends = [b for b in self.static_berries if isinstance(b, berries.Picture) and b is not pic and
                   b.added and pic.added and abs((b.added - pic.added).total_seconds()) < 180 and
                   b.source and b.source == pic.source and
                   b.quality == 0]
        others = (b for b in self.run_wish(self.get_preset('взор по бокам').wish) if b is not pic)
        return itertools.islice(itertools.chain(friends, others), 2)

    def flyby(self):
        windir.load()
        if windir.activate(class_group='vlc'):
            return

        flybys = list(itertools.islice(self.run_wish(self.get_preset('облёт').wish), self.flyby_count))
        for flyby in flybys:
            self.history.record(History.watched, flyby)
        berries.show_videos(*(flyby.url for flyby in flybys))

    def poll_current_song(self):
        if (mpc := music.MPDClient()).status().get('state') == 'play':
            info = mpc.currentsong()  # pylint: disable=no-member
            log.debug('poll_current_song: %r', info)
            if song := first(b for b in self.berries if getattr(b, 'path', None) == info.get('file')):
                if song not in self.history.collect(History.played, last=1):
                    self.history.record(History.played, song)
        return True


class Window(Gtk.ApplicationWindow):

    def __init__(self, application, **kwargs):
        super().__init__(application=application, title='Горница', **kwargs)
        self.app = application
        util.gtk.destroy_on_delete(self)
        self.set_accels()

        self.stack = Gtk.Stack(expand=True)
        self.add(self.stack)
        self.stack.show()

    @property
    def c(self):
        return self.stack.get_visible_child()

    def set_accels(self):
        srub.set_accels(self, [
            ('Escape', self.close_display),
            ('<Control>equal', self.toggle_details),
            ('<Alt>F1', self.actions_menu),
            ('F2', self.execute_action, 2),
            ('F3', self.execute_action, 3),
            ('F4', self.execute_action, 4),
            ('F6', self.app.add_berry),
            ('F7', self.edit_berry),
            ('<Shift>F7', self.app.bulk_edit_berries),
            ('F5', self.new_wish),
            ('<Control>F5', self.new_wish, 'strict'),
            ('<Shift>F5', self.display_more),
            ('F8', self.flatten),
            ('F9', self.display_recent),
            ('<Alt>F9', self.display_watched),
            ('<Control>F9', self.display_history),
            ('F10', self.display_shortcuts),
            ('F11', self.display_moods),
            ('F12', self.display_top),
            ('<Control>0', self.display_preset, 'взор I'),
            ('<Control>1', self.display_preset, 'гуд I'),
            ('<Control>2', self.display_preset, 'гуд II'),
            ('<Control>3', self.display_preset, 'гуд III'),
            ('<Control>I', self.simulate),
        ])

    def display(self, view):
        view.show_all()
        view.details_column.set_visible(False)
        self.stack.add(view)
        self.stack.set_visible_child(view)
        util.gtk.focus(view)

    def close_display(self):
        if children := self.stack.get_children():
            children.pop().destroy()
            if children:
                self.stack.set_visible_child(prev := children[-1])
                util.gtk.focus(prev)
        else:
            self.destroy()

    def display_wish(self, wish, strict=False):
        self.app.history.wishes[wish] = None
        self.display(View(self, self.app.run_wish(wish, strict)))

    def display_items(self, items):
        self.display(View(self, items))

    def display_shortcuts(self):
        classes = (berries.Preset, berries.Directive)
        history = self.app.history.collect(cls=classes)
        shortcuts = [b for b in self.app.static_berries if isinstance(b, classes)]
        shortcuts.sort(key=lambda b: (find(history, b, sentinel=999), b.title))
        self.display_items(shortcuts)

    def display_preset(self, title):
        self.display_wish(self.app.get_preset(title).wish)

    def display_moods(self):
        moods = [b for b in self.app.static_berries if isinstance(b, berries.Mood)]
        moods.sort(key=lambda b: b.title)
        self.display_items(moods)

    def display_top(self):
        self.display_preset('верх')

    def display_recent(self):
        self.display_items(self.app.history.collect(action=set(History.actions) - {History.watched}))

    def display_watched(self):
        self.display_items(self.app.history.collect(action=History.watched))

    def display_history(self):
        if (action := srub.ListDialog(History.actions, parent=self).do()) is not quit:
            self.display_items(self.app.history.collect(action))

    def new_wish(self, strict=False):
        if wish := self.prompt_wish():
            self.display_wish(wish, bool(strict))

    def prompt_wish(self, wish=None):
        return srub.EntryDialog(wish, history=self.app.history.wishes,
                                title='Чего?', default_width=800, parent=self).do()

    def toggle_details(self):
        self.stack.get_visible_child().details_column.props.visible ^= True

    def execute_action(self, k=1, path=None):
        entry = self.stack.get_visible_child().active_entry(path)
        self._execute_action(entry, berries.actions(entry.item, k-1))

    def actions_menu(self):
        entry = self.stack.get_visible_child().active_entry()
        if (action := srub.ListDialog(actions := berries.actions(entry.item),
                                      texts=[util.strings.name_to_title(act.__name__) for act in actions],
                                      parent=self).do()) is not quit:
            self._execute_action(entry, action)

    def _execute_action(self, entry, action):
        args = tuple(obj
                     for name in inspect.signature(action).parameters
                     if (obj := dict(self=entry.item, app=self.app, win=self).get(name)))
        self.app.history.record(History.activated, entry.item)
        match action(*args):
            case str(wish):
                self.display_wish(wish)
            case list(more):
                self.display_items(more)
            case False:
                self.stack.get_visible_child().remove_entry(entry)
            case x if x is quit:
                self.close_display()
            case _:
                self.stack.get_visible_child().update_entry(entry)

    def edit_berry(self):
        view = self.stack.get_visible_child()
        entry = view.active_entry()
        if entry.item not in self.app.static_berries:
            srub.MessageDialog(Gtk.MessageType.WARNING, 'Эта ягода не хранится', parent=self).do()

        if (item := srub.FormDialog(entry.item, allow_remove=True, parent=self).do()) is entry.item:
            view.update_entry(entry)
            self.app.save_berries()
            self.app.history.record(History.edited, item)
        elif item is Gtk.ResponseType.REJECT:
            view.remove_entry(entry)
            self.app.remove_berry(entry.item)

    def display_more(self):
        self.stack.get_visible_child().display_more()

    def flatten(self):
        self.display_items(self.app.flatten(self.stack.get_visible_child().source.weighted))

    def simulate(self):
        results = (view := self.stack.get_visible_child()).source.simulate(view.active_entry().item)
        srub.ListDialog(results, title='Прогнали', parent=self).do()


class View(Gtk.ScrolledWindow):

    def __init__(self, window, source):
        self.win = window
        self.debug_attrs = []
        super().__init__(expand=True)

        self.tree = Gtk.TreeView(expand=True, headers_visible=False, search_column=1)
        self.details_column = Gtk.TreeViewColumn('Общее', Gtk.CellRendererText(size_points=12, xpad=6), text=2)
        self.tree.append_column(self.details_column)
        style = dict(xpad=12, ypad=40, yalign=0)
        self.tree.append_column(Gtk.TreeViewColumn('Облик', Gtk.CellRendererPixbuf(**style), pixbuf=3))
        self.tree.append_column(column := Gtk.TreeViewColumn(
            'Опись', renderer := Gtk.CellRendererText(wrap_mode=Pango.WrapMode.WORD, **style), markup=4))
        column.connect('notify::width', lambda _col, _: renderer.set_property('wrap-width', column.get_width()))
        self.tree.connect('row-activated', self._on_row_activated)
        self.add(self.tree)

        self.source = source
        self._iter = iter(source)
        self.items = []
        self.store = Gtk.ListStore(int, str, str, GdkPixbuf.Pixbuf, str)
        self.display_more()
        self.tree.set_model(self.store)

    @property
    def c(self):
        return self.active_entry().item

    def display_more(self, n=40):
        for item in itertools.islice(self._iter, n):
            it = self.store.append((len(self.items), '', '', None, ''))
            self.items.append(item)
            self._render_item(self.store[it], item)

    def _render_item(self, row, item):
        row[1] = item.title or ''
        row[2] = '\n'.join((str(item.quality), *sorted(item.tags)))
        row[3] = item.make_pixbuf(width=300) if hasattr(item, 'make_pixbuf') else None
        row[4] = item.description
        if self.debug_attrs:
            row[5] += '\n' + berries.small(', '.join(f'{name}={getattr(item, name, "?")}'
                                                     for name in self.debug_attrs))

    def _on_row_activated(self, _, path, _column):
        self.win.execute_action(path=path)

    def active_entry(self, path=None):
        store, paths = self.tree.get_selection().get_selected_rows()
        it = store.get_iter(path or paths[0])
        item = self.items[store[it][0]]
        return Entry(it, item)

    def update_entry(self, entry):
        self._render_item(self.store[entry.it], entry.item)

    def remove_entry(self, entry):
        self.store.remove(entry.it)


Entry = namedtuple('Entry', ('it', 'item'))


class History:

    actions = [
        edited := 'edited',
        activated := 'activated',
        played := 'played',
        watched := 'watched',
    ]

    def __init__(self):
        self.wishes = srub.make_history()
        self.log = []

    @property
    def c(self): return self.log[-1][-1]

    def record(self, action, x):
        self.log.append((datetime.now(), action, x))

    def select(self, action=None, cls=None, last=-1, past=None):
        now = datetime.now()
        for (t, act, x) in reversed(self.log):
            if last == 0:
                break
            if past and now - t > past:
                break
            if action and not (act in action if isinstance(action, (tuple, list, set)) else act == action):
                continue
            if cls and not isinstance(x, cls):
                continue
            yield (t, act, x)
            last -= 1

    def collect(self, action=None, cls=None, last=-1, past=None):
        return [x for (_, _, x) in self.select(action, cls, last, past)]


class SightWindow(Gtk.ApplicationWindow):

    def __init__(self, application, **kwargs):
        super().__init__(application=application, title='Взор', **kwargs)
        self.app = application
        self.set_accels()
        self.image = Gtk.Image(expand=True)
        self.add(self.image)
        self.image.show()
        self.fullscreen()

        self.timeout_id = self.pics = None
        self.fill()

    def set_accels(self):
        srub.set_accels(self, [
            ('Escape', self.hide),
            ('Return', self.add_pics),
            ('Delete', self.remove_pic),
            ('BackSpace', self.replace_pic),
            ('F5', self.fill),
            ('F7', self.edit_pic),
            ('Left', self.show_prev),
            ('Right', self.show_next),
            ('Up', self.shuffle),
            ('Down', self.throw),
        ] + [(str(k), self.slideshow, k) for k in range(10)])

    def fill(self):
        self.slideshow(None)
        self.pics = []
        self.add_pics(9)
        self.show_next()

    def add_pics(self, n=1):
        for pic in self.app.run_wish(self.app.get_preset('взор I').wish):
            if self.add_pic(pic):
                n -= 1
                if n == 0:
                    break

    def add_pic(self, pic):
        if pic in [p.picture for p in self.pics]:
            return False
        if not (pixbuf := self.make_pixbuf(pic)):
            return False
        self.pics.append(SightPicture(pic, pixbuf))
        self.app.history.record(History.watched, pic)
        self.show_next()
        return True

    def remove_pic(self):
        if len(self.pics) == 1:
            return
        self.show_next()
        self.pics.pop(1)

    def replace_pic(self):
        self.add_pics(1)
        self.pics.pop(1)

    def edit_pic(self):
        pic = self.pics[0].picture
        if (r := srub.FormDialog(pic, allow_remove=True, parent=self).do()) is pic:
            self.render_current()
            self.app.save_berries()
            self.app.history.record(History.edited, pic)
        elif r is Gtk.ResponseType.REJECT:
            self.remove_pic()
            self.app.remove_berry(pic)

    def make_pixbuf(self, pic):
        monitor = util.gtk.monitor_geometry()
        fit = pic.make_pixbuf(monitor.width, monitor.height)
        if (margin := (monitor.width - fit.get_width()) / 2) > 500:
            fillers = self.app.pick_margin_fillers(pic)
            left, right = (filler.make_narrow_pixbuf(margin, monitor.height) for filler in fillers)
            pixbuf = GdkPixbuf.Pixbuf.new(fit.props.colorspace, fit.props.has_alpha,
                                          fit.props.bits_per_sample, monitor.width, monitor.height)
            left.copy_area(0, 0, margin, monitor.height, pixbuf, 0, 0)
            fit.copy_area(0, 0, fit.get_width(), monitor.height, pixbuf, margin, 0)
            right.copy_area(0, 0, margin, monitor.height, pixbuf, monitor.width - margin, 0)
        elif margin > 0:
            pixbuf = pic.make_pixbuf(monitor.width)
            pixbuf = pixbuf.new_subpixbuf(0, (pixbuf.get_height() - monitor.height) / 2,
                                          monitor.width, monitor.height)
        elif fit.get_height() < monitor.height:
            pixbuf = pic.make_narrow_pixbuf(monitor.width, monitor.height)
        else:
            pixbuf = fit
        log.debug('make_pixbuf: %s: %dx%d -> %dx%d', pic.url,
                  fit.get_width(), fit.get_height(), pixbuf and pixbuf.get_width(), pixbuf and pixbuf.get_height())
        return pixbuf

    def slideshow(self, k):
        if self.timeout_id is not None:
            GLib.source_remove(self.timeout_id)
            self.timeout_id = None
        if k:
            self.timeout_id = GLib.timeout_add_seconds(2 * k**2, lambda *_: [self.show_next()])

    def show_prev(self):
        self.pics.append(self.pics.pop(0))
        self.render_current()

    def show_next(self):
        self.pics.insert(0, self.pics.pop())
        self.render_current()

    def shuffle(self):
        random.shuffle(self.pics)
        self.render_current()

    def render_current(self):
        self.image.set_from_pixbuf(self.pics[0].pixbuf)

    def throw(self):
        self.pics[0].picture.set_backdrop_left()
        self.hide()


SightPicture = namedtuple('SightPicture', ('picture', 'pixbuf'))


def hide_mouse():
    monitor = util.gtk.monitor_geometry()
    subprocess.run(('xdotool', 'mousemove', '0', str(monitor.height)), check=True)
