import copy
import dataclasses
from dataclasses import dataclass, field
from datetime import datetime, timedelta
import inspect
from pathlib import Path
import random
import subprocess
import tempfile

import dateutil.parser
import gi
import yaml

gi.require_version('Gdk', '3.0')
gi.require_version('GdkPixbuf', '2.0')
gi.require_version('Gtk', '3.0')
from gi.repository import Gdk, GdkPixbuf, Gtk
from gi.repository.GLib import markup_escape_text as escape

import srub
import tamala
from util import logs, ýaml
import util.iter
from util.format import format_timedelta
import windir

from gornica import music

log = logs.getLogger(__name__)

berries_store = Path.home() / 'my/fair/berries.yaml'

show_missing = False


def load_static():
    berries = yaml.unsafe_load(berries_store.read_text())
    log.info('loaded %d berries', len(berries))

    fresh = []
    for b in berries:
        if b.expires and b.expires < datetime.now():
            log.info('prune stale %r', b)
        else:
            fresh.append(b)
    log.info('pruned to %d fresh berries', len(fresh))

    return fresh


def dump_static(berries):
    berries_store.write_text(ýaml.dump(berries, spacing=1))
    log.info('dumped %d berries', len(berries))


white   = '#FFFFFF'
red     = '#FFC0B3'
yellow  = '#FFD9B3'
green   = '#B3FFB3'
teal    = '#B3FFE6'
blue    = '#B3E5FF'
magenta = '#CCB3FF'


# pylint: disable=inconsistent-return-statements


@dataclass(eq=False)
class Berry:

    title: str
    tags: set[str] = field(default_factory=set)
    quality: int = 3        # 1 through 5
    comment: str = None
    trace: dict = field(default_factory=dict)

    title_color = white

    @property
    def added(self):
        return self.trace.get('added')

    @property
    def expires(self):
        return None

    @property
    def description(self):
        actual_title = f'<span color="{self.title_color}">{big(self.title)}</span>' if self.title else None
        title = ['  '.join((*self._pre_title(), actual_title, *self._post_title()))] if actual_title else ()
        comment = [small(self.comment)] if self.comment else ()
        return '\n'.join((line for line in (*self._before_title(), *title, *comment, *self._after_title()) if line))

    def _before_title(self): return ()
    def _pre_title(self):    return ()
    def _post_title(self):   return ()
    def _after_title(self):  return ()


def actions(berry, i=None):
    methods = [m for (name, m) in inspect.getmembers(berry, inspect.ismethod) if not name.startswith('_')]
    methods.sort(key=lambda m: m.__func__.__code__.co_firstlineno)
    return methods if i is None else methods[i]


@dataclass(eq=False)
class Mood(Berry):

    depth: int = 1
    general: str = None
    music1: str = None
    music2: str = None
    sight: str = None

    @property
    def shallowness(self): return max(0, 4 - self.depth)

    def enable(self, app, win=None):
        if new := srub.FormDialog(copy.deepcopy(self), only_fields=Mood.__annotations__, parent=win).do():
            app.mood = self if dataclasses.astuple(self) == dataclasses.astuple(new) else new

    def clone(self, app):
        app.add_berry(type(self), **self.__dict__)


@dataclass(eq=False)
class Preset(Berry):

    wish: str = ...

    def run(self):               return self.wish
    def run_modified(self, win): return win.prompt_wish(self.wish)


@dataclass(eq=False)
class Directive(Berry):

    code: str = ...

    # pylint: disable=unused-argument
    def run(self, app, win):          return eval(self.code or 'None')
    def run_modified(self, app, win): return eval(win.prompt_wish(self.code) or 'None')


@dataclass(eq=False)
class Anchor(Berry):

    title_color = teal

    def ponder(self): pass


@dataclass(eq=False)
class Feed(Berry):

    url: str = ...
    cadence: float = None

    title_color = yellow

    def _post_title(self): return [small('?')] if show_missing and self.cadence is None else ()

    def __setattr__(self, name, value):
        if name == 'url' and value:
            value = (value.
                     replace('//m.vk.com/', '//vk.com/').
                     replace('//t.me/s/', '//t.me/').
                     replace('//web.telegram.org/a/#@', '//t.me/').
                     replace('//web.telegram.org/k/#@', '//t.me/').
                     replace('//web.telegram.org/z/#@', '//t.me/'))
        super().__setattr__(name, value)

    @property
    def browsable_url(self):
        url = self.url.replace('//t.me/', '//web.telegram.org/k/#@')
        if 'гуд' not in self.tags:
            url = url.replace('//vk.com/', '//m.vk.com/')
        return url

    def visit(self, app, raw=False):
        browser = firefox_fullscreen if '//t.me/' in self.url else firefox
        open_browser(browser, self.url if raw else self.browsable_url)
        self.trace['prev_visited'] = self.trace.get('visited')
        self.trace['visited'] = datetime.now()
        app.save_berries()

    def visit_raw(self, app):
        self.visit(app, raw=True)

    def unvisit(self, app):
        self.trace['visited'] = self.trace.pop('prev_visited', None)
        app.save_berries()


@dataclass(eq=False)
class Seek(Berry):

    url: str = None
    at: str = None
    notes: str = field(default='', metadata=dict(min_height=150))

    title_color = red

    def _after_title(self):
        if self.url:
            yield small(self.url)
        if self.at:
            yield '▸ ' + escape(self.at)
        if self.notes:
            yield from (small(line) for line in self.notes.splitlines())

    def open(self): open_browser(firefox, self.at if (self.at or '').startswith('http') else self.url)


@dataclass(eq=False)
class Resource(Berry):

    url: str = None
    params: list = None
    min_page: int = None
    max_page: int = None

    title_color = magenta

    def open(self, win):
        url = self.url
        if not url:
            return

        if self.params:
            param = srub.ListDialog(random.sample(self.params, len(self.params)), parent=win).do()
            if param is quit:
                return
            url %= param

        if self.min_page is not None:
            page = str(random.randint(self.min_page, self.max_page))
            if url.startswith('file://'):
                subprocess.run(('zathura', '--page', page, url), check=True)
                return
            Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD).set_text(page, -1)

        open_url(url)


@dataclass(eq=False)
class Picture(Berry):

    url: str = ...
    source: str = None
    keep_for: int = None
    pole: float = 0.5
    skip_sides: bool = False

    @property
    def expires(self):
        return None if self.keep_for is None else self.added + timedelta(days=self.keep_for)

    def _before_title(self):
        if self.expires:
            yield self.expires.strftime('<small>пропадёт %e %B (%A)</small>').lower().replace('  ', ' ')

    def show(self):               show_pictures(tamala.stashed(self.url, 'gornica'))
    def add_to_sight(self, app):  app.sight_window.add_pic(self)
    def set_backdrop_left(self):  self._set_backdrop(windir.left)
    def set_backdrop_right(self): self._set_backdrop(windir.right)
    def reset_backdrop(self):     set_backdrop(...)
    def open_source(self):        open_browser(firefox, self.source)

    def _set_backdrop(self, place):
        pixbuf = GdkPixbuf.Pixbuf.new_from_file(str(get_backdrop()))
        x = max(0, place.x)
        width = place.width - (x - place.x)
        self.make_narrow_pixbuf(place.width, place.height).copy_area(0, 0, width, place.height, pixbuf, x, place.y)
        _, path = tempfile.mkstemp(prefix='backdrop-', suffix='.png')
        pixbuf.savev(path, 'png', option_keys=[], option_values=[])
        set_backdrop(path)

    def make_pixbuf(self, width=-1, height=-1):
        return GdkPixbuf.Pixbuf.new_from_file_at_size(str(tamala.stashed(self.url, 'gornica')), width, height)

    def make_narrow_pixbuf(self, width, height):
        pixbuf = self.make_pixbuf(height=height)
        native_width = pixbuf.get_width()
        src_x = max(0, native_width * self.pole - width / 2)
        return pixbuf.new_subpixbuf(src_x, 0, width, pixbuf.get_height())


@dataclass(eq=False)
class Flyby(Berry):

    url: str = ...
    lat: float = ...
    lat_dev: float = 0
    lon: float = ...
    lon_dev: float = 0

    def show(self): show_videos(self.url)


@dataclass(eq=False)
class Song(Berry):

    path: str = ...
    artist: str = None
    album: str = None
    albumartist: str = None
    duration: timedelta = ...
    explicit_quality: bool = False

    title_color = green

    @property
    def key(self):
        return music.store / self.path

    @property
    def group(self):
        return self.albumartist or self.artist or self.album

    def _before_title(self):
        if ss := [s for x in (self.artist, self.album) for s in (x if isinstance(x, list) else [x]) if s]:
            yield escape(' · '.join(ss))
        if not self.title:
            yield '  '.join(('<big><i>без названия</i></big>', *self._post_title()))

    def _post_title(self):
        yield small(format_timedelta(self.duration))
        if show_missing and not self.explicit_quality:
            yield small('?')

    def play_next(self):
        self.add_next(play=music.MPDClient().status().get('state') != 'play')   # pylint: disable=no-member

    def edit_playlists(self, app, win):
        orig = self._playlists()
        if (r := srub.FormDialog(self, only_fields={'tags', 'quality'}, allow_remove=True, parent=win).do()) is None:
            return
        remove = r is not self
        self.explicit_quality = True
        new = set() if remove else self._playlists()

        # pylint: disable=no-member
        mpc = music.MPDClient()
        for p in orig - new:
            if (i := util.iter.find(mpc.listplaylist(p), self.path)) is not None:
                mpc.playlistdelete(p, i)
        for p in new - orig:
            mpc.playlistadd(p, self.path)

        if remove:
            del app.dynamic_berries[self.key]
            music.remove(self.path)
            return False

    def _playlists(self):
        return self.tags | ({str(self.quality)} if self.explicit_quality else set())

    def add_random(self):
        # pylint: disable=no-member
        status = (mpc := music.MPDClient()).status()
        min_ = 0 if (cur := status.get('song')) is None else (int(cur) + 1)
        max_ = int(status.get('playlistlength') or 0)
        mpc.addid(self.path, random.randint(min_, max_))

    def add_next(self, play=False):
        # pylint: disable=no-member
        status = (mpc := music.MPDClient()).status()
        if status.get('state') == 'play':
            id_ = mpc.addid(self.path, int(status['song']) + 1)
            if play:
                mpc.playid(id_)
        else:
            mpc.playid(mpc.addid(self.path, 0))
            if not play:
                mpc.stop()

    def add_last(self):
        music.MPDClient().add(self.path)    # pylint: disable=no-member

    def play_now(self):
        self.add_next(play=True)

    def same_group(self, app):
        return [b for b in app.berries if isinstance(b, Song) and b.group == self.group]

    def visit_artist(self, app):
        if artist := self._artist(app):
            artist.visit(app)

    def _artist(self, app):
        return util.iter.first(b for b in app.berries if isinstance(b, Artist) and b.title == self.group)


def load_songs(bs, pr):
    mpc = music.MPDClient()
    # pylint: disable=no-member

    had = set(bs.keys())

    for i, info in enumerate(total := mpc.listallinfo()):
        pr.update('песни', i / len(total), debounce=0.1)
        if path := info.get('file'):
            song = Song(path=path, title=info.get('title'))
            if song.key in bs:
                song = bs[song.key]
            bs[song.key] = song
            had.discard(song.key)
            song.quality = 2
            song.trace = dict(added=to_naive_local(dateutil.parser.parse(info['last-modified'])))
            song.artist = info.get('artist')
            song.album = info.get('album')
            song.albumartist = info.get('albumartist')
            song.duration = timedelta(seconds=int(info['time']))
            song.comment = music.load_comment(song.path)

    for key in had:
        if isinstance(key, Path) and music.store in key.parents:
            del bs[key]

    for i, playlist in enumerate(total := mpc.listplaylists()):
        pr.update('метки песен', i / len(total), debounce=0.1)
        p = playlist['playlist']
        for path in mpc.listplaylist(p):
            if song := bs.get(music.store / path):
                if p.isdigit():
                    song.quality = int(p)
                    song.explicit_quality = True
                else:
                    song.tags.add(p)


def to_naive_local(dt):
    return dt.astimezone(tz=None).replace(tzinfo=None)


@dataclass(eq=False)
class Artist(Feed):

    title_color = magenta

    def songs(self, app):
        return [b for b in app.berries if isinstance(b, Song) and b.group == self.title]


@dataclass(eq=False)
class Music(Berry):

    url: str = None
    artist_url: str = None
    stage: int = field(default=0, metadata=dict(tip='2=брать 3=искать 4=скачать 5=скачано'))
    at: str = None
    notes: str = field(default='', metadata=dict(min_height=150))

    title_color = red

    def _pre_title(self):  return [str(self.stage)]
    def _post_title(self): return ['↗'] if self.artist_url else ()

    def _after_title(self):
        if self.stage < 3:
            yield small('  '.join(self.tags))
        if self.url:
            yield small(self.url)
        if self.at:
            yield f'▸ {self.at}'

        lines = [small(line) for line in (self.notes or '').splitlines() if line != line.lstrip('+?')]
        if lines:
            if len(lines) > (limit := 5):
                lines = lines[:limit] + [small(f'[...+{len(lines)-limit}')]
            yield from lines

    def open(self):          open_browser(firefox, self.url) if self.url else self.search_yandex()  # pylint: disable=expression-not-assigned
    def open_artist(self):   open_browser(firefox, self.artist_url)
    def search_vk(self):     open_browser(firefox, f'https://vk.com/music?q={self.title}')
    def search_yandex(self): open_browser(firefox, f'https://music.yandex.ru/search?text={self.title}')

    def finalize(self, app):
        open_browser(firefox, self.artist_url)
        if app.add_berry(Artist, title=self.title.partition(' · ')[0], url=self.artist_url):
            app.remove_berry(self)
            return False


def show_pictures(*paths):
    subprocess.run(('xw', 'feh', '--fullscreen', '--hide-pointer', '--zoom', 'fill', *(str(p) for p in paths)),
                   check=True)

def get_backdrop():
    return Path(subprocess.run(('xfconf-query', '-c', 'xfce4-desktop',
                                '-p', '/backdrop/screen0/monitoreDP-1/workspace0/last-image'),
                               check=True, capture_output=True, text=True).stdout.strip())

def set_backdrop(path):
    if path is ...:
        path = Path.home() / '.local/share/backdrop.png'
    subprocess.run(('xfconf-query', '-c', 'xfce4-desktop',
                    '-p', '/backdrop/screen0/monitoreDP-1/workspace0/last-image', '-s', str(path)), check=True)

def show_videos(*urls):
    subprocess.run(('xw', 'vlc', '--crop', '16:10', '--fullscreen', '--loop', *urls), check=True)

def open_browser(browser, url):
    subprocess.run(('xw', *browser, url), check=True)

firefox = ('firefox',)
firefox_fullscreen = ('firefox', 'place=fullscreen')


def open_url(url):
    subprocess.run(('xw', 'xdg-open', url), check=True)


def big(s):   return '<span size="x-large">' + escape(s) + '</span>'
def small(s): return '<small>' + escape(s) + '</small>'
