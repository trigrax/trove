# pylint: disable=no-member

import os
from pathlib import Path
import re

import mpd
import mutagen
import mutagen.easyid3

store = Path.home() / 'my.bulk/music'


class MPDClient(mpd.MPDClient):     # pylint: disable=abstract-method

    def __init__(self):
        super().__init__()
        (password, host) = os.environ['MPD_HOST'].split('@')
        port = int(os.environ['MPD_PORT'])
        self.connect(host, port)
        self.password(password)

    def playlistpaths(self):
        return [entry.removeprefix(prefix) for entry in self.playlist() if entry.startswith(prefix := 'file: ')]


def load_comment(path):
    path = store / path
    if path.suffix in {'.mp3', '.mpga'}:
        entries = mutagen.easyid3.EasyID3(path).get('version', [])
    else:
        entries = mutagen.File(path).get('comment', [])
    entries = [entry for entry in entries if not any(re.match(pattern, entry) for pattern in comments_blacklist)]
    return '\n'.join(entries) or None


def remove(path):
    path = store / path
    path.unlink()
    for p in path.parents:
        if not any(p.iterdir()):
            p.rmdir()


comments_blacklist = [
    r'~',
    r'Collected ',
    r'Declipped ',
    r'EAC ',
    r'Encoded ',
    r'Exact Audio Copy',
    r'Track [0-9]',
    r'Visit ',
]
