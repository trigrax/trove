from datetime import datetime, timedelta
import functools

from util import ínspect
import util.logs
import util.strings
import weighted

from gornica import berries, music

log = util.logs.getLogger(__name__)


class Picker:

    def __init__(self, bs, history, mood, wish, strict=False):
        bs = list(bs)

        self.strict = strict
        self.now = datetime.now()
        self.classes = {cls.__name__: cls for cls in ínspect.recursive_subclasses(berries.Berry)}
        self.activated = history.collect(history.activated) if history else []
        self.mood = mood
        self._sung = _collect_sung(bs, history)

        self.formula = compile(wish, mode='eval', filename='<wish>')
        self.berry = None
        self.weighted = [(w, b) for b in bs if (w := self.weigh(b))]
        log.debug('weighted: %d out of %d', len(self.weighted), len(bs))

    def __iter__(self):
        if self.strict:
            return iter(b for (_, b) in sorted(self.weighted, key=lambda wb: wb[0], reverse=True))
        return iter(weighted.shuffle(self.weighted))

    def simulate(self, berry, runs=1000):
        counts = [0] * len(self.weighted)
        for _ in range(runs):
            shuffled = iter(self)
            for k in range(len(counts)):
                if next(shuffled) is berry:
                    for j in range(k, len(counts)):
                        counts[j] += 1
                    break
        return counts

    def weigh(self, b):
        self.berry = b
        return float(eval(self.formula, {}, self))

    def __getitem__(self, name):
        if name in __builtins__:
            return __builtins__[name]

        if cls := self.classes.get(name):
            return Weight(isinstance(self.berry, cls))

        if name.islower() and util.strings.is_cyrillic(name):
            return Weight(name in self.berry.tags)

        if name.startswith(prefix := 'M_'):
            return eval(self.mood_for(name.removeprefix(prefix)), {}, self)

        if name.startswith(prefix := '_'):
            return self.find(name.removeprefix(prefix))

        if (v := getattr(self, name, None)) is not None:
            return v

        return getattr(self.berry, name, Weight(0))

    @functools.lru_cache
    def mood_for(self, what):
        if not (wish := getattr(self.mood, what)):
            return self.const1
        log.debug('mood_for: %s: %s', what, wish)
        return compile(wish, mode='eval', filename=f'<mood.{what}>')

    const1 = compile('1', mode='eval', filename='const1')

    def find(self, needle):
        return Weight(needle.lower() in (self.berry.title or '').lower())

    @property
    def Q(self):
        return Weight(self.berry.quality ** self.mood.depth)

    @property
    def R(self):
        if not self.berry.added:
            return Weight(1)
        r = (self.berry.added - (self.now - timedelta(days=120))) / timedelta(days=30)
        return Weight(max(1, max(0, r) ** self.mood.shallowness))

    @property
    def E(self):
        if (keep_for := getattr(self.berry, 'keep_for', None)) is None:
            return Weight(1)
        return Weight(max(1, (3 ** self.mood.shallowness) / (keep_for + 1)))

    @property
    def V(self):
        match (getattr(self.berry, 'cadence', None), self.berry.trace.get('visited')):
            case (None, _):
                return Weight(self.berry in self.activated)
            case (0, _) | (_, None):
                return Weight(0)
            case (cadence, visited):
                if (r := (self.now - visited) / timedelta(days=cadence)) < 0.3:
                    return Weight(1)
                return Weight(1 - min(1, r))

    @property
    def sung(self):
        return self.berry in self._sung


def _collect_sung(bs, history):
    paths = {song.path for song in history.collect(history.played)} | set(music.MPDClient().playlistpaths())
    songs = [b for b in bs if isinstance(b, berries.Song) and b.path in paths]
    groups = {song.group for song in songs}
    artists = [b for b in bs if isinstance(b, berries.Artist) and b.title in groups]
    return songs + artists


class Weight(float):

    __slots__ = []

    def __repr__(self):
        return f'Weight({float(self)!r})'

    def __add__(self, other):      return Weight(float(self) * float(other))
    def __radd__(self, other):     return Weight(float(other) * float(self))
    def __sub__(self, other):      return Weight(float(self) * (1 - min(1, float(other))))
    def __rsub__(self, other):     return Weight(float(other) * (1 - min(1, float(self))))
    def __pos__(self):             return self
    def __neg__(self):             return Weight(1 - min(1, float(self)))
    def __truediv__(self, other):  return Weight(float(self) + float(other))
    def __rtruediv__(self, other): return Weight(float(other) + float(self))
    def __invert__(self):          return Weight(1 / float(self))
    def __pow__(self, p):          return Weight(float(self) ** p)
    __mul__ = __add__
    __rmul__ = __radd__
