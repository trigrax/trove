#!/usr/bin/python3

from collections import namedtuple
from functools import singledispatch
import subprocess
import sys
import time

import gi

gi.require_version('Gdk', '3.0')
gi.require_version('GdkX11', '3.0')
gi.require_version('Gtk', '3.0')
gi.require_version('Wnck', '3.0')
from gi.repository import Gdk, GdkX11, Gtk, Wnck

import util.gtk
import util.logs

log = util.logs.getLogger(__name__)

screen = ...
cur_timestamp = ...


def main():
    Gdk.init(sys.argv)
    load()
    exec(sys.argv[1], dict(globals(), args=sys.argv[2:]))


def load():
    global screen
    if screen is None:
        Wnck.set_client_type(Wnck.ClientType.PAGER)
    else:
        screen = None
        Wnck.shutdown()
    screen = Wnck.Screen.get_default()
    screen.force_update()
    global cur_timestamp
    cur_timestamp = GdkX11.x11_get_server_time(
        GdkX11.X11Window.lookup_for_display(Gdk.Display.get_default(), GdkX11.x11_get_default_root_xwindow()))


def resolve_window(win):
    return screen.get_active_window() if win is ... else win


class Place(namedtuple('Place', ('x', 'y', 'width', 'height'))):

    @property
    def useful_width(self):
        return self.width - left_border

    def to_geometry(self):
        return f'{self.useful_width}x{self.height}+{self.x}+{self.y}'

    def to_term(self, font_size):
        char_width = self.useful_width // font_size.width
        char_height = self.height // font_size.height
        return ('--geometry', f'{char_width}x{char_height}+{self.x}+{self.y}')

    def to_emacs(self):
        x = max(0, self.x)
        width = self.useful_width - 16 - (x - self.x)
        return ('--frame-parameters', '(' + ' '.join((f'(left . {x})',
                                                      f'(top . {self.y})',
                                                      f'(width text-pixels . {width})',
                                                      f'(height text-pixels . {self.height})')) + ')')

monitor = util.gtk.monitor_geometry()
side_width = 500
left_border = 19

left = Place(x=-left_border, y=0, width=side_width + left_border, height=monitor.height)
right = Place(x=monitor.width - side_width - left_border, y=0, width=side_width + left_border, height=monitor.height)
center = Place(x=side_width, y=0, width=monitor.width - 2*side_width - left_border, height=monitor.height)
bulk = Place(x=side_width, y=0, width=monitor.width - side_width, height=monitor.height)

fullscreen = Place(0, 0, monitor.width, monitor.height)     # values suitable for is_placed, otherwise a sentinel


FontSize = namedtuple('FontSize', ('width', 'height'))

term_normal = FontSize(12, 27)
term_small = FontSize(10, 22)


# ~/my/setup/share/emacs/init.el - look for `text-pixels'
# ~/my/setup/share/feh/themes


@singledispatch
def put(win, place):
    log.debug('put: %r', place)
    win = resolve_window(win)
    win.set_fullscreen(place is fullscreen)
    if place is not fullscreen:
        place = fixup_for_headerbar(place, win)
        win.unmaximize()
        win.set_geometry(Wnck.WindowGravity.STATIC,
                         (mask := Wnck.WindowMoveResizeMask).X | mask.Y | mask.WIDTH | mask.HEIGHT,
                         place.x, place.y, place.width, place.height)

@put.register
def put_gtk(win: Gtk.Window, place):
    if place is fullscreen:
        log.debug('put_gtk: fullscreen')
        win.fullscreen()
        return

    win.unfullscreen()

    if (place.x < 0 or place.y < 0) and not win.get_realized():
        log.debug('put_gtk: not realized yet, cannot place outside screen borders')
        util.gtk.connect_limit(win, 'draw', 2, lambda *_: util.gtk.idle_once(put_gtk, win, place))
        return

    fixed_height = place.height - (headerbar.get_allocated_height() if (headerbar := win.get_titlebar()) else 0)
    log.debug('put_gtk: %r, fixed_height=%r', place, fixed_height)
    win.unmaximize()
    win.move(place.x, place.y)
    win.resize(place.useful_width, fixed_height)


def twiddle(win):
    win = resolve_window(win)
    height = win.get_geometry().heightp
    height += (+1) if height % 2 else (-1)
    win.set_geometry(Wnck.WindowGravity.STATIC, Wnck.WindowMoveResizeMask.HEIGHT, 0, 0, 0, height)


def fixup_for_headerbar(place, win):
    if win.get_class_group_name() in {'Drawing', 'Gcolor3', 'Gitg', 'Gtk3-demo', 'Yelp'}:
        return Place(place.x - 25, place.y - 23, place.width + 51, place.height + 52)
    return place


def activate(class_group=None, class_instance=None, name=None, place=None):
    if candidates := list(find_by(screen.get_active_workspace(), class_group, class_instance, name, place)):
        top = top_visible = None
        for win in candidates:
            log.debug('activate: candidate %r: class_group=%r, class_instance=%r, geometry=%r, is_active=%r, sort_order=%r',
                      win.get_name(), win.get_class_group_name(), win.get_class_instance_name(), win.get_geometry(),
                      win.is_active(), win.get_sort_order())
            top = win if top is None else top
            top_visible = win if top_visible is None and not win.is_minimized() else top_visible

        if top and top.is_active():
            # We may be iterating via repeated invocations of windir. Ensure that we can reach all candidates
            # using the sort order provided by wnck, but pivoting on the order of the top (= last, in this case).
            candidates.sort(key=lambda win: (win.get_sort_order() <= top.get_sort_order(),
                                             win.get_sort_order()))
            target = candidates[0]
        else:   # We probably just want to switch to the last active (= top) of candidates.
            target = top_visible or top

        target.activate(cur_timestamp)
        return target

    log.debug('activate: no candidates for class_group=%r, class_instance=%r, place=%r',
              class_group, class_instance, place)
    return None


def launch(command, sync=False):
    log.debug('launch: %r', command)
    subprocess.run(command if sync else ('xw', *command), check=True)


def launch_at(command, place, class_group=None, class_instance=None, repeat_after=None):
    log.debug('launch_at: %r at %r', command, place)
    orig_xid = win.get_xid() if (win := screen.get_active_window()) else None
    launch(command)
    for i in range(50):
        log.debug('launch_at: attempt %d', i)
        time.sleep(0.003 * i)
        load()
        if (win := screen.get_active_window()) and win.get_xid() != orig_xid and win.get_name() != 'Desktop':
            log.info('launch_at: newly active: %s', win.get_name())
            if class_group and win.get_class_group_name() != class_group:
                log.info('launch_at: class_group mismatch: expected %r, got %r',
                         class_group, win.get_class_group_name())
            elif class_instance and win.get_class_instance_name() != class_instance:
                log.info('launch_at: class_instance mismatch: expected %r, got %r',
                         class_instance, win.get_class_instance_name())
            else:
                put(win, place)
                if repeat_after:
                    for t in (repeat_after if isinstance(repeat_after, list) else [repeat_after]):
                        time.sleep(t)
                        put(win, place)
                return win

    return None


def clear(place):
    for win in find_by(workspace=screen.get_active_workspace(), place=place):
        win.minimize()


def find_by(workspace=None, class_group=None, class_instance=None, name=None, place=None):
    for win in reversed(screen.get_windows_stacked()):  # top to bottom
        if workspace and not win.is_on_workspace(workspace):
            continue
        if class_group and win.get_class_group_name() != class_group:
            continue
        if class_instance and win.get_class_instance_name() != class_instance:
            continue
        if name and win.get_name() != name:
            continue
        if place and not is_placed(win, place):
            continue
        yield win


@singledispatch
def is_placed(win, place):
    win = resolve_window(win)
    place = fixup_for_headerbar(place, win)
    geom = win.get_geometry()
    log.debug('is_placed: %s: %r vs. %r', win.get_name(), geom, place)
    # Ignore vertical measurements because they don't match straightforwardly (e.g. yp=-31 vs. y=6).
    return near(geom.xp, place.x) and near(geom.widthp, place.width)

@is_placed.register
def is_placed_gtk(win: Gtk.Window, place):
    x, _ = win.get_position()
    width, _ = win.get_size()
    return near(x, place.x) and near(width, place.useful_width)


def in_same_place(win1, win2):
    return any(is_placed(win1, place) and is_placed(win2, place)
               for place in (left, center, bulk, right))


def near(x, p):
    return abs(x - p) < 10


def best_window(app, cls, options=None, default_place=bulk):
    current_desktop = GdkX11.X11Screen.get_current_desktop(Gdk.Screen.get_default())
    place = globals()[placename] if options and (placename := options.get('place')) else default_place
    log.debug('best_window: current_desktop=%r, place=%r', current_desktop, place)

    for win in app.get_windows():
        if is_placed(win, place) and (gdk_win := win.get_window()):
            desktop = GdkX11.X11Window.get_desktop(gdk_win)
            log.debug('best_window: %s: desktop=%r', win.get_title(), desktop)
            if desktop == current_desktop:
                return win

    win = cls(app)
    put(win, place)
    return win


if __name__ == '__main__':
    main()
