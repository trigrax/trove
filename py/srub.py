import dataclasses
from collections import Counter, namedtuple
import functools
import sys
import threading
import time
import traceback

import gi
import lru
from slugify import slugify

gi.require_version('Gdk', '3.0')
gi.require_version('Gio', '2.0')
gi.require_version('Gtk', '3.0')
from gi.repository import Gdk, Gio, Gtk

from util import ástral, ýaml
import util.gtk
import util.logs
import util.strings

import windir

log = util.logs.getLogger(__name__)


def dark():
    Gtk.Settings.get_default().props.gtk_application_prefer_dark_theme = True

def maybe_dark():
    Gtk.Settings.get_default().props.gtk_application_prefer_dark_theme = not ástral.is_sunny()


def install_excepthook():
    sys.excepthook, _ = _wrap(sys.excepthook)

def _wrap(orig):
    def excepthook(etype, value, tb):
        if threading.current_thread() is threading.main_thread():
            exception_dialog(etype, value, tb)
        orig(etype, value, tb)
    return excepthook, orig

def exception_dialog(etype, value, tb):
    if issubclass(etype, KeyboardInterrupt):
        return
    MessageDialog(Gtk.MessageType.ERROR, str(value), ''.join(traceback.format_exception(etype, value, tb))).do()


def set_accels(target, entries):
    if isinstance(target, Gtk.ApplicationWindow):
        prefix = 'win.'
        app = target.get_application()
        entries.append(('F1', show_help, target))
    else:
        prefix = 'app.'
        app = target

    name_counts = Counter(f.__name__ for (_, f, *_) in entries)

    for (accel, f, *args) in entries:
        name = f.__name__.replace('_', '-').strip('-')
        if name_counts[f.__name__] > 1 and args:
            name += '-' + slugify(str(args[0]))
        target.add_action(action := Gio.SimpleAction.new(name, None))
        action.connect('activate', _make_handler(f, args))
        app.set_accels_for_action(prefix + name, [accel])


def _make_handler(f, args):
    def handler(_action, _param):
        f(*args)
    handler.__name__ = f.__name__
    return handler


def show_help(win):
    app = win.get_application()
    entries = [(accel, name)
               for (prefix, names) in (('win.', win.list_actions()), ('app.', app.list_actions()))
               for name in names
               for accel in app.get_accels_for_action(prefix + name)]
    entries.sort(key=lambda e: (len(e[0]), e[0]))

    section = Gtk.ShortcutsSection(title='Shortcuts', visible=True, max_height=12)
    for i, (accel, name) in enumerate(entries):
        if i % section.props.max_height == 0:   # pylint: disable=no-member
            section.add(group := Gtk.ShortcutsGroup(title=''))
        group.add(Gtk.ShortcutsShortcut(action_name=name,
                                        title=util.strings.name_to_title(name.replace('-', '_')),
                                        accelerator=accel))

    # https://stackoverflow.com/a/61158451
    win = Gtk.ShortcutsWindow(application=app)
    win.add(section)
    win.show_all()


class Dialog(Gtk.Dialog):

    def __init__(self, *args, **kwargs):
        kwargs.setdefault('window_position', Gtk.WindowPosition.CENTER_ON_PARENT)
        kwargs.setdefault('decorated', kwargs.get('use_header_bar') or False)
        super().__init__(*args, **kwargs)
        self._response = None

    def run(self, *args, **kwargs):  # pylint: disable=arguments-differ
        resp = Gtk.ResponseType(super().run(*args, **kwargs))
        log.debug('%s: %s', type(self).__name__, resp)
        return resp

    def do(self, *args, **kwargs):
        r = self.run(*args, **kwargs)
        self.destroy()
        return r

    def run_main(self):
        sys.excepthook, orig_excepthook = _wrap(sys.excepthook)
        try:
            util.gtk.idle_once(self._run_main)
            Gtk.main()
            return self._response
        finally:
            sys.excepthook = orig_excepthook

    def _run_main(self):
        try:
            self._response = self.do()
        finally:
            Gtk.main_quit()


class MessageDialog(Gtk.MessageDialog, Dialog):

    def __init__(self, message_type=Gtk.MessageType.INFO, text='', secondary_text=None, **kwargs):  # more convenient positional args
        kwargs.setdefault('buttons', Gtk.ButtonsType.OK)
        Dialog.__init__(self, message_type=message_type, text=text, secondary_text=secondary_text, **kwargs)


def make_history(initial_items=(), size=40):
    history = lru.LRU(size)
    for item in initial_items:
        history[item] = None
    return history


class EntryDialog(Dialog):

    def __init__(self, text='', title='Entry', history=None, **kwargs):
        kwargs.setdefault('default_width', 300)
        super().__init__(title=title, **kwargs)
        self.entry = Gtk.Entry(text=text)
        self.entry.connect('activate', self._on_entry_activate)
        if history is not None:
            install_history(self.entry, history)
        self.get_content_area().add(self.entry)
        self.show_all()

    def _on_entry_activate(self, _entry):
        self.response(Gtk.ResponseType.OK)

    def run(self, *args, **kwargs):
        r = super().run(*args, **kwargs)
        return self.entry.get_text() if r is Gtk.ResponseType.OK else None


class ListDialog(Dialog):

    def __init__(self, entries, texts=None, markups=None, title='List', scrollable=False, **kwargs):
        super().__init__(title=title, **kwargs)

        self.entries = entries

        self.view = Gtk.TreeView(model=(store := Gtk.ListStore(int, str, str)), search_column=1,
                                 headers_visible=False)
        for (i, entry) in enumerate(self.entries):
            store.append([i,
                          texts[i] if texts else markups[i] if markups else str(entry),
                          markups[i] if markups else None])
        attrs = dict(markup=2) if markups else dict(text=1)
        self.view.append_column(Gtk.TreeViewColumn('Title', Gtk.CellRendererText(), **attrs))
        self.view.connect('row-activated', self._on_view_row_activated)

        if scrollable:
            self.get_content_area().add(scrollw := Gtk.ScrolledWindow(vexpand=True))
            scrollw.add(self.view)
        else:
            self.get_content_area().add(self.view)

        self.show_all()

    def _on_view_row_activated(self, _view, _path, _column):
        self.response(Gtk.ResponseType.OK)

    def run(self, *args, **kwargs):
        if (r := super().run(*args, **kwargs)) is not Gtk.ResponseType.OK:
            return quit
        model, paths = self.view.get_selection().get_selected_rows()
        r = [self.entries[model[path][0]] for path in paths]
        if self.view.get_selection().get_mode() is Gtk.SelectionMode.SINGLE:
            [r] = r
        return r


class TextDialog(Dialog):

    def __init__(self, text='', title='Text', **kwargs):
        kwargs.setdefault('border_width', 6)
        kwargs.setdefault('default_height', 400)
        kwargs.setdefault('default_width', 400)
        super().__init__(title=title, use_header_bar=True, **kwargs)

        self.get_content_area().add(scroller := Gtk.ScrolledWindow(expand=True, shadow_type=Gtk.ShadowType.IN))
        scroller.add(view := Gtk.TextView(expand=True))
        self.buffer = view.get_buffer()
        self.buffer.set_text(text)

        self.add_button('_OK', Gtk.ResponseType.OK)
        self.add_button('Cancel', Gtk.ResponseType.CANCEL)
        self.set_default_response(Gtk.ResponseType.OK)
        util.gtk.mark_suggested(self.get_widget_for_response(Gtk.ResponseType.OK))

        self.show_all()

    def run(self, *args, **kwargs):
        if super().run(*args, **kwargs) is not Gtk.ResponseType.OK:
            return None
        return self.buffer.get_text(self.buffer.get_start_iter(), self.buffer.get_end_iter(),
                                    include_hidden_chars=True)


class FormDialog(Dialog):

    def __init__(self, target, overrides=None, allow_remove=False, only_fields=None, title=..., **kwargs):
        kwargs.setdefault('use_header_bar', True)
        kwargs.setdefault('border_width', 6)
        kwargs.setdefault('default_width', 500)
        super().__init__(title=getattr(target, '__name__', type(target).__name__) if title is ... else title, **kwargs)

        self._target = target
        self._fields = {}
        self._codecs = {}

        self.get_content_area().add(grid := Gtk.Grid())
        grid.props.row_spacing = 3
        grid.props.margin_bottom = 6

        for i, field in enumerate(dataclasses.fields(target)):
            if not field.metadata.get('editable', True):
                continue
            if only_fields and field.name not in only_fields:
                continue

            title = util.strings.name_to_title(field.name)
            grid.attach(label := Gtk.Label.new_with_mnemonic('_' + title + ':'), 1, 1 + i, 1, 1)

            value = self._get_value(target, overrides, field)

            grid.attach(codec_button := Gtk.ToolButton(), 2, 1 + i, 1, 1)
            codec_button.get_style_context().add_class('dimmed')
            codec_button.connect('clicked', self._on_codec_button_clicked, field)
            codec = self._codec_for(field)
            self._set_codec(field, codec_button, codec)
            value = '' if value is None or value is ... else codec.encode(field, value)

            grid.attach(widget := self._to_widget(field, value), 3, 1 + i, 1, 1)
            if tip := field.metadata.get('tip'):
                widget.set_tooltip_text(tip)
            widget.props.hexpand = True
            label.set_mnemonic_widget(widget.get_child() if isinstance(widget, Gtk.ScrolledWindow) else widget)
            label.props.xalign = 1
            label.props.margin_end = 6
            self._fields[field] = widget

            # Without this, by default the widget gets focused with selecting (if it's an entry).
            if i == 0 and (unselect := getattr(widget, 'grab_focus_without_selecting', None)):
                unselect()  # pylint: disable=not-callable

        grid.set_focus_chain(list(self._fields.values()))   # skip codec buttons when tabbing

        self.add_button('_OK', Gtk.ResponseType.OK)
        if allow_remove:
            self.add_button('_Remove', Gtk.ResponseType.REJECT)
            util.gtk.mark_destructive(self.get_widget_for_response(Gtk.ResponseType.REJECT))
        self.add_button('Cancel', Gtk.ResponseType.CANCEL)
        self.set_default_response(Gtk.ResponseType.OK)
        util.gtk.mark_suggested(self.get_widget_for_response(Gtk.ResponseType.OK))

        self.show_all()

    def run(self, *args, **kwargs):
        r = super().run(*args, **kwargs)
        if r is Gtk.ResponseType.REJECT:
            return r
        if r is not Gtk.ResponseType.OK:
            return None

        result = self._target
        if isinstance(result, type):
            result = result(**{field.name: None for field in self._fields})
        for field, widget in self._fields.items():
            value = self._from_widget(widget)
            codec = self._codecs[field]
            setattr(result, field.name, codec.decode(field, value) if value else codec.empty(field))
        return result

    @staticmethod
    def _get_value(target, overrides, field):
        if overrides and field.name in overrides:
            return overrides[field.name]

        if isinstance(target, type):
            if field.default is not dataclasses.MISSING:
                return field.default
            if field.default_factory is not dataclasses.MISSING:
                return field.default_factory()
            return None

        return getattr(target, field.name)

    @classmethod
    def _to_widget(cls, field, value):
        if min_height := field.metadata.get('min_height'):
            scroller = Gtk.ScrolledWindow(height_request=min_height, shadow_type=Gtk.ShadowType.IN)
            scroller.add(view := Gtk.TextView(expand=True))
            view.get_buffer().set_text(value)
            return scroller

        return Gtk.Entry(text=value, activates_default=True)

    @classmethod
    def _to_checkbutton(cls, _field, value):
        return Gtk.CheckButton(active=bool(value), inconsistent=value is not True and value is not False)

    @classmethod
    def _from_widget(cls, widget):
        if isinstance(widget, Gtk.ScrolledWindow):
            return cls._from_widget(widget.get_child())

        if isinstance(widget, Gtk.TextView):
            buffer = widget.get_buffer()
            return buffer.get_text(buffer.get_start_iter(), buffer.get_end_iter(), include_hidden_chars=True)

        return widget.get_text()

    @classmethod
    def _from_checkbutton(cls, field, button):
        if button.props.inconsistent:
            raise RuntimeError(f'{field.name} is inconsistent')
        return button.props.active

    @staticmethod
    def _codec_for(field):
        if issubclass(field.type, str):
            return plain
        if field.type in (list[str], set[str]):
            return words
        return yaml

    def _on_codec_button_clicked(self, button, field):
        if (codec := ListDialog(codecs, texts=[codec.name for codec in codecs], parent=self).do()) is not quit:
            self._set_codec(field, button, codec)

    def _set_codec(self, field, button, codec):
        self._codecs[field] = codec
        button.props.label = codec.glyph


Codec = namedtuple('Codec', ('name', 'glyph', 'encode', 'decode', 'empty'))
codecs = [
    plain := Codec('plain', '', empty=lambda _: None,
                   encode=lambda _, v: v,
                   decode=lambda _, s: s),
    yaml := Codec('YAML', '▸', empty=lambda _: None,
                  encode=lambda _, v: ýaml.dump(v, default_flow_style=True).rstrip().removesuffix('...').rstrip(),
                  decode=lambda _, s: ýaml.unsafe_load(s)),
    words := Codec('words', '∴', empty=lambda f: f.type(),
                   encode=lambda _, v: ' '.join(v),
                   decode=lambda f, v: f.type(v.split())),
]

def install_history(entry, history):
    entry.connect('key-press-event', _history_on_key_press_event, history)
    entry.connect('activate', _history_on_activate, history)

def _history_on_key_press_event(entry, event, history):
    key = Gdk.keyval_name(event.keyval)

    if key == 'F3':
        if (text := ListDialog(history.keys(), parent=entry.get_toplevel()).do()) is not quit:
            entry.set_text(text)
        return True

    if idx := {'Up': -1, 'KP_Up': -1, 'Down': +1, 'KP_Down': +1}.get(key):
        if history:
            if (text := entry.get_text()) in (past := list(reversed(history.keys()))):
                idx += past.index(text)
            entry.set_text(past[idx % len(past)])
        return True

    return False

def _history_on_activate(entry, history):
    if text := entry.get_text():
        history[text] = None


class Progresser:

    def __init__(self, title, pulse_step=None):
        self.canceled = False
        self.last_update = 0

        self.dialog = Dialog(title=title)
        self.dialog.get_content_area().add(vbox := Gtk.Box())
        self.dialog.connect('response', lambda *_: self.cancel())
        if (app := Gtk.Application.get_default()) and (win := app.get_active_window()):
            self.dialog.move(*win.get_position())

        self.progressbar = Gtk.ProgressBar(valign=Gtk.Align.CENTER, margin_left=12, margin_right=6,
                                           show_text=True, text=title)
        if pulse_step:
            self.progressbar.props.pulse_step = pulse_step
        vbox.add(self.progressbar)

        self.button = Gtk.Button.new_from_icon_name('process-stop', Gtk.IconSize.SMALL_TOOLBAR)
        self.button.props.relief = Gtk.ReliefStyle.NONE
        self.button.connect('clicked', lambda _: self.cancel())
        vbox.add(self.button)

        self.dialog.show_all()

    def __enter__(self):
        self.dialog.present()
        util.gtk.process_events()
        return self

    def __exit__(self, _exc_type, exc_value, _traceback):
        if self.dialog:
            self.dialog.destroy()
        return False    # do not suppress exception

    def update(self, text=None, fraction=None, debounce=0):
        if self.canceled:
            raise Canceled()

        if (t := time.time()) - self.last_update < debounce:
            return
        self.last_update = t

        if text is not None:
            self.progressbar.props.text = text

        if fraction is None:
            self.progressbar.pulse()
        else:
            self.progressbar.props.fraction = fraction

        util.gtk.process_events()

    def threaded(self, f, *args, **kwargs):
        r = exc = None
        def runner():
            nonlocal r, exc
            try:
                r = f(*args, **kwargs)
            except Exception as e:
                exc = e
        thread = threading.Thread(target=runner)
        thread.start()
        while thread.is_alive():
            util.gtk.process_events()
            if self.canceled:
                raise Canceled()
            thread.join(timeout=0.1)
        if exc is not None:
            raise exc
        return r

    def cancel(self):
        self.canceled = True
        self.button.props.sensitive = False


def threaded(title, f, *args, **kwargs):
    with Progresser(title) as pr:
        return pr.threaded(f, *args, **kwargs)


def dummy(*_):
    pass


class Canceled(BaseException):  # similar to KeyboardInterrupt
    pass


def cancelable(f):
    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Canceled:
            return None
    return wrapper


def open_console(path):
    windir.load()
    if not windir.activate(class_instance='emacs', place=windir.left):
        windir.launch_at(('emacsclient', '--eval', f'(python-make-console "{path}")'), windir.left)
