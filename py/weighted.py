import random


def equal_weighted(xs):
    return ((1, x) for x in xs)


def shuffle(wxs, k=-1):
    population, weights = _collect(wxs)
    while population and k:
        [x] = random.choices(population, weights)
        yield x
        del population[i := population.index(x)]
        del weights[i]
        k -= 1


def interleave(wxs):
    population, weights = _collect(wxs)
    population = [iter(x) for x in population]
    while population:
        [iterator] = random.choices(population, weights)
        try:
            yield next(iterator)
        except StopIteration:
            del population[i := population.index(iterator)]
            del weights[i]


def _collect(wxs):
    population = []
    weights = []
    for (w, x) in wxs:
        if w > 0:
            population.append(x)
            weights.append(w)
    return population, weights
