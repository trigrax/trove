import hashlib
import json
from pathlib import Path
import warnings

import requests
import urllib3.exceptions

warnings.filterwarnings('ignore', category=urllib3.exceptions.InsecureRequestWarning)


class Session(requests.Session):

    def __init__(self, *args, tamala_control=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.proxies = {'http': (url := 'http://localhost:8080'), 'https': url}
        self.verify = False
        self.tamala_control = tamala_control or {}

    def request(self, method, url, headers=None, tamala_control=None, **kwargs): # pylint: disable=arguments-differ
        if (tamala_control := dict(self.tamala_control, **(tamala_control or {}))):
            control_s = ', '.join(k + '=' + json.dumps(v) for (k, v) in tamala_control.items())
            headers = dict(headers or {}, **{'Tamala-Control': control_s})
        return super().request(method, url, headers=headers, **kwargs)


def force(**settings):
    return dict(ignore_no_store=True, ignore_no_cache=True, ignore_must_revalidate=True, **settings)


session = Session()
get = session.get


def stashed(url, owner):
    if url.startswith(prefix := 'file://'):
        return Path(url.removeprefix(prefix))

    path = (root := Path('/tmp/stashed')) / hashlib.md5(url.encode()).hexdigest()
    if not path.exists():
        resp = get(url, headers={'User-Agent': f'Mozilla/5.0 (stashed: {owner})'},
                   tamala_control=force(min_freshness=999999999))
        resp.raise_for_status()
        root.mkdir(exist_ok=True)
        path.write_bytes(resp.content)
    return path
