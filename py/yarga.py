import argparse
from pathlib import Path
import shutil
import subprocess

import gi
import pgmagick

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

import util.gtk



def main():
    parser = argparse.ArgumentParser()
    sub = parser.add_subparsers(required=True)

    p = sub.add_parser('xfwm4')
    p.set_defaults(func=do_xfwm4)
    p.add_argument('--name', default='Yarga')
    p.add_argument('--no-install', action='store_true')
    p.add_argument('subargs', nargs='*')

    p = sub.add_parser('preview')
    p.set_defaults(func=do_preview)

    args = parser.parse_args()
    args.func(args)


def do_xfwm4(args):
    path = themes_path / args.name / 'xfwm4'
    if path.exists():
        shutil.rmtree(path)
    path.mkdir(parents=True)
    (path / 'themerc').write_text('button_layout=')

    for (state, subargs) in (('active', args.subargs), ('inactive', ('inactive', *args.subargs))):
        png = subprocess.run(('runghc', 'Strip.hs', *subargs), cwd=Path.home() / 'my/stead/yarga',
                             capture_output=True, check=True).stdout
        pgmagick.Image(pgmagick.Blob(png)).write(str(path / f'left-{state}.xpm'))

    if not args.no_install:
        install_xfwm4_theme(args.name)


themes_path = Path.home() / '.local/share/themes'


def install_xfwm4_theme(name):
    shutil.copytree(themes_path / name, temp := themes_path / 'yarga-temp')
    for theme in (temp.name, name):
        subprocess.run(('xfconf-query', '-c', 'xfwm4', '-p', '/general/theme', '-s', theme), check=True)
    shutil.rmtree(temp)


def do_preview(_):
    win = Gtk.Window()
    util.gtk.destroy_on_delete(win)
    win.connect('destroy', lambda *_: Gtk.main_quit())
    win.present()
    Gtk.main()


if __name__ == '__main__':
    main()
