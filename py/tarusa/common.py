import re

import gi
from shapely.geometry import LineString, MultiPolygon, Point, Polygon
import shapely.wkt
import yaml

gi.require_version('OsmGpsMap', '1.0')
from gi.repository import OsmGpsMap


def parse_point(s) -> Point:
    if m := re.match(r'([0-9]{1,3}\.[0-9]+),\s*([0-9]{1,3}\.[0-9]+)', s):
        return Point(float(m.group(2)), float(m.group(1)))

    if m := re.match(r'N?\s*([0-9]+)\s?°\s*([0-9]+)\s?[\'′]\s*([0-9.]+)\s?["″](?:\s+с\.\s?ш\.)?'
                     r',?\s+'
                     r'E?\s*([0-9]+)\s?°\s*([0-9]+)\s?[\'′]\s*([0-9.]+)\s?["″](?:\s+в\.\s?д\.)?',
                     s):
        (lat_deg, lat_min, lat_sec, lon_deg, lon_min, lon_sec) = m.groups()
        return Point(int(lon_deg) + float(lon_min)/60 + float(lon_sec)/(60**2),
                     int(lat_deg) + float(lat_min)/60 + float(lat_sec)/(60**2))

    return None


def point_from_shapely(pt: Point) -> OsmGpsMap.MapPoint: return OsmGpsMap.MapPoint.new_degrees(pt.y, pt.x)
def point_to_shapely(pt: OsmGpsMap.MapPoint) -> Point:   return Point(*reversed(pt.get_degrees()))


def _represent_wkt(dumper, ob):   return dumper.represent_scalar('!wkt', shapely.wkt.dumps(ob, trim=True))
def _construct_wkt(loader, node): return shapely.wkt.loads(loader.construct_scalar(node))
yaml.add_representer(LineString, _represent_wkt)
yaml.add_representer(MultiPolygon, _represent_wkt)
yaml.add_representer(Point, _represent_wkt)
yaml.add_representer(Polygon, _represent_wkt)
yaml.add_constructor('!wkt', _construct_wkt)

def _represent_osmgps(dumper, pt):   return dumper.represent_scalar('!osmgps', shapely.wkt.dumps(point_to_shapely(pt), trim=True))
def _construct_osmgps(loader, node): return point_from_shapely(_construct_wkt(loader, node))
yaml.add_representer(OsmGpsMap.MapPoint, _represent_osmgps)
yaml.add_constructor('!osmgps', _construct_osmgps)
