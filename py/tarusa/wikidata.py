import dataclasses
from dataclasses import dataclass
from datetime import date, datetime

import dateutil.parser
from munch import munchify
import shapely.geometry
from shapely.geometry import Point
import shapely.geometry.base
import shapely.wkt

import tamala
import util.format
import util.logs

log = util.logs.getLogger(__name__)


PROP_COORDS = 'P625'


def query(q, cls):
    resp = session.get('https://query.wikidata.org/sparql', params={'query': q},
                       headers={'Accept': 'application/sparql-results+json'})
    resp.raise_for_status()
    fields = {fld.name: fld for fld in dataclasses.fields(cls)}
    by_ident = {}
    for entry in resp.json().get('results').get('bindings'):
        f = cls(**dict(convert_field(fields, k, v) for k, v in entry.items()))
        if seen := by_ident.get(f.ident):
            aggregate(seen, f)
        else:
            by_ident[f.ident] = f
    return list(by_ident.values())


def convert_field(fields, k, v):
    name = {'item': 'ident',
            'itemLabel': 'title',
            'itemDescription': 'description'}.get(k, k)
    return name, convert_value(fields[name], v)

def convert_value(fd, v):
    value = v['value']
    if fd.type is object:
        return value
    if issubclass(fd.type, datetime):
        return convert_datetime(value)
    if issubclass(fd.type, date):
        return convert_date(value)
    if issubclass(fd.type, shapely.geometry.base.BaseGeometry):
        return convert_geom(value)
    return fd.type(value)

def convert_datetime(s):
    return dateutil.parser.parse(s) if s and s[0].isdigit() else None

def convert_date(s):
    return dt.date() if (dt := convert_datetime(s)) else dt

def convert_geom(s):
    return shapely.wkt.loads(s) if s else None


def aggregate(f1, f2):
    for fd in dataclasses.fields(f1):
        setattr(f1, fd.name, aggregate_field(fd, getattr(f1, fd.name), getattr(f2, fd.name)))

def aggregate_field(fd, v1, v2):
    if v1 and v2 and (func := (fd.metadata or {}).get('aggregator')):
        return func(v1, v2)
    return v1 or v2


def search(needle):
    resp = session.get('https://www.wikidata.org/w/api.php',
                       params={'action': 'query',
                               'list': 'search',
                               'format': 'json',
                               'srsearch': needle,
                               # https://www.mediawiki.org/wiki/Help:Extension:WikibaseCirrusSearch#haswbstatement
                               'haswbstatement': PROP_COORDS,
                               'srprop': 'snippet|titlesnippet',
                               'uselang': 'ru',
                               'srlimit': 100},
                       tamala_control=tamala.force(min_freshness=24 * 3600))
    resp.raise_for_status()
    return [FoundItem(ident=i.title,
                      titlesnippet=i.titlesnippet.replace('<span class="searchmatch">', '').replace('</span>', ''),
                      snippet=i.snippet)
            for i in munchify(resp.json()).query.search]


@dataclass
class FoundItem:
    ident: str
    titlesnippet: str
    snippet: str


def point(ident):
    resp = session.get(f'https://www.wikidata.org/wiki/Special:EntityData/{ident}.json')
    resp.raise_for_status()
    for claim in munchify(resp.json()).entities.get(ident).claims.get(PROP_COORDS, []):
        v = claim.mainsnak.datavalue.value
        return Point(v.longitude, v.latitude)


def get_shape(uri):
    _, _, path = uri.partition('//commons.wikimedia.org/data/main/')
    assert path
    resp = session.get('https://commons.wikimedia.org/w/index.php?title=' + path + '&action=raw',
                       tamala_control=tamala.force(min_freshness=24 * 3600))
    resp.raise_for_status()
    [f] = resp.json().get('data').get('features')
    return shapely.geometry.shape(f.get('geometry'))


session = tamala.Session()
session.headers['User-Agent'] = 'Tarusa (custom) python-requests'
