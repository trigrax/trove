import base64
import dataclasses
from dataclasses import dataclass, field
from datetime import date
import textwrap
from urllib.parse import quote as urlquote

import gi
from shapely.geometry import LineString, Point

gi.require_version('Gdk', '3.0')
gi.require_version('OsmGpsMap', '1.0')
from gi.repository import Gdk, OsmGpsMap

import util.color
from util.color import rgb

from tarusa import draw, osm
import tarusa.common    # pylint: disable=unused-import     # for YAML representers
from tarusa.common import point_from_shapely


@dataclass(eq=False)
class Feature:

    title: str
    point: Point

    def make_preimage(self, zoom):
        return make_badge(zoom, pt, list(self.labels), self.icon) if (pt := self.point) else None

    def make_track(self, _zoom):
        return None

    def __hash__(self):
        return hash(id(self))

    @property
    def labels(self):
        return []

    @property
    def icon(self):
        return None

    @property
    def lookups(self):
        yotitle = urlquote(self.title)
        title = yotitle.replace('Ё', 'Е').replace('ё', 'е')
        yield ('Википедия', f'https://ru.wikipedia.org/w/index.php?search={yotitle}')
        yield ('БРЭ', f'https://old.bigenc.ru/search?q={title}')
        yield ('ЭСБЕ', f'https://ru.wikisource.org/wiki/%D0%AD%D0%A1%D0%91%D0%95/{title}')


@dataclass
class Preimage:

    pixbuf: object
    point: Point
    xalign: float = 0.5
    yalign: float = 0.5


class Sizing:

    def size(self, zoom):
        raise NotImplementedError()


@dataclass
class Linear(Sizing):

    zoom1: int = 7
    size1: float = 12
    zoom2: int = 12
    size2: float = 19

    def size(self, zoom):
        if zoom < self.zoom1:
            return None
        return self.size1 + ((zoom - self.zoom1) / (self.zoom2 - self.zoom1)) * (self.size2 - self.size1)


@dataclass
class Label:

    text: str
    sizing: Sizing = dataclasses.field(default_factory=Linear)
    color: tuple[float, float, float] = rgb(0xF5F5F5)


@dataclass
class Icon:

    name: str
    sizing: Sizing = dataclasses.field(default_factory=lambda: Linear(6, 12, 7, 14))
    color: tuple[float, float, float] = rgb(0xF5F5F5)


def make_badge(zoom, point: Point, labels, icon=None, background_color=rgb(0x212020)):
    lines = []
    for label in labels:
        if size := label.sizing.size(zoom):
            for text in label.text.splitlines():
                lines.append(draw.Line(text, size, label.color))

    icon = draw.Icon(icon.name, int(size), icon.color) if icon and (size := icon.sizing.size(zoom)) else None

    if not lines and not icon:
        return None

    return Preimage(to_pixbuf(draw.badge(lines, icon, background_color)), point)


def make_way(zoom, line: LineString, resolution, sizing, color):
    if not (size := sizing.size(zoom)) or not (res := resolution.size(zoom)):
        return None
    track = OsmGpsMap.MapTrack()
    last_pt = Point(0, 0)
    for (x, y) in line.coords:
        if (pt := Point(x, y)).distance(last_pt) >= res:
            track.add_point(point_from_shapely(pt))
            last_pt = pt
    track.props.color = util.color.to_gdk(color)
    track.props.alpha = 1
    track.props.line_width = int(size)
    return track


def to_pixbuf(surface):
    return Gdk.pixbuf_get_from_surface(surface, 0, 0, surface.get_width(), surface.get_height())


@dataclass(eq=False)
class River(Feature):

    ident: str = ...
    mouth_point: Point = ...
    mouth: str = None
    discharge: float = None
    watershed_area: float = None

    @property
    def wikidata_id(self):
        return self.ident.rsplit('/', maxsplit=1)[-1]

    @property
    def labels(self):
        yield Label(self.title, sizing=Linear(zoom1=7, zoom2=12), color=rgb(0xC5F7FF))

    def make_track(self, zoom):
        return make_way(zoom, osm.fetch_stream(self),
                        resolution=Linear(5, 0.1, 10, 0.01),
                        sizing=Linear(zoom1=4, size1=1, zoom2=14, size2=5), color=rgb(0x005EC7))


@dataclass(eq=False)
class Region:

    title: str
    ident: str = ...
    shape: object = ...
    code: str = ...
    coat_of_arms: str = None


@dataclass(eq=False)
class Town(Feature):

    ident: str = ...
    inception: date = field(default=None, metadata=dict(aggregator=min))
    earliest_record: date = field(default=None, metadata=dict(aggregator=min))
    population: int = field(default=None, metadata=dict(aggregator=max))
    coat_of_arms: str = None

    @property
    def labels(self):
        yield Label(self.title)
        if self.inception:
            yield Label(f'★ {self.inception.year}', sizing=Linear(zoom1=9, zoom2=14))
        if self.earliest_record:
            yield Label(f'✎ {self.earliest_record.year}', sizing=Linear(zoom1=9, zoom2=14))
        if self.population:
            yield Label(f'⚴ {util.format.number(self.population)}', sizing=Linear(zoom1=9, zoom2=14))


@dataclass(eq=False)
class Station(Feature):

    ident: str = ...
    description: str = None
    railway_code: str = ...
    opening: date = None

    @property
    def labels(self):
        yield Label(self.title, sizing=Linear(zoom1=10, zoom2=15))
        yield Label(self.railway_code, sizing=Linear(zoom1=11, zoom2=16))
        if self.opening:
            yield Label(f'★ {self.opening.year}', sizing=Linear(zoom1=11, zoom2=16))

    @property
    def icon(self):
        return Icon('railway-halt', sizing=Linear(9, 9, 10, 10))

    @property
    def lookups(self):
        title = urlquote(self.title)
        yield ('Википедия', f'https://ru.wikipedia.org/w/index.php?search={title}')
        yield ('RailGallery', f'https://railgallery.ru/search.php?order=5&place1={title}')
        yield ('Паровоз ИС', f'https://www.parovoz.com/newgallery/?SHOW_ALL=1&HOWMANY=192&DESCR={title}')
        yield ('Фотолинии', f'https+post://railwayz.info/photolines/search/!searchstation={title}')
        yield ('Энциклопедия нашего транспорта', f'https://wiki.nashtransport.ru/index.php?search={title}')
        yield ('train-photo.ru', f'http+post://train-photo.ru/search.php!search_keywords={title}')
        yield ('Викиданные', self.ident)


@dataclass(eq=False)
class Heritage(Feature):

    ident: str = ...
    ethnicities: set[int] = ...
    description: str = ...

    @property
    def labels(self):
        yield Label(textwrap.shorten(self.title, 30, placeholder='...'), color=rgb(0xFFCCEE))

    @property
    def lookups(self):
        yield ('Описание', 'data:text/html;base64,' + base64.b64encode(self.description.encode()).decode())
