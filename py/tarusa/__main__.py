import sys

import tarusa.app


if __name__ == '__main__':
    tarusa.app.Application().run(sys.argv)
