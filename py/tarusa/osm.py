from pathlib import Path
import pickle

from shapely.geometry import LineString
import requests

import util.logs

log = util.logs.getLogger(__name__)


def fetch_stream(river) -> LineString:
    return LineString([(lon, lat) for (lat, lon) in _fetch_stream(river)])

def _fetch_stream(river) -> list[tuple[float, float]]:
    streams = load_streams()
    if stream := streams.get(river.wikidata_id):
        log.debug('fetch_stream: already have %s', river.wikidata_id)
        return stream

    log.debug('fetch_stream: query for %s', river.wikidata_id)
    query = (f'[out:json];'
             f'relation[wikidata="{river.wikidata_id}"];'
             f'way(r:"main_stream");'
             f'node(w);'
             f'out geom;')
    log.debug('_fetch_stream: query: %s', query)
    resp = session.post('https://overpass-api.de/api/interpreter', data={'data': query})
    resp.raise_for_status()
    stream = [(e['lat'], e['lon']) for e in resp.json().get('elements')]
    reorder_way(stream, (river.point.y, river.point.x), (river.mouth_point.y, river.mouth_point.x))
    streams[river.wikidata_id] = stream
    log.info('fetch_stream: got %d elements for %s', len(stream), river.wikidata_id)
    save_streams(streams)
    return stream


session = requests.Session()


def load_streams():
    return pickle.loads(streams_path.read_bytes()) if streams_path.exists() else {}

def save_streams(streams):
    streams_path.write_bytes(pickle.dumps(streams))

streams_path = Path.home() / 'my/survey/streams.pickle'


def reorder_way(points: list[tuple[float, float]], orig: tuple[float, float], dest: tuple[float, float]):
    cur = orig
    i = 0
    while i < len(points):
        closest_j = closest_d = None
        for j, pt in enumerate(points[i:], start=i):
            d = distance(cur, pt)
            if closest_d is None or closest_d > d:
                closest_d = d
                closest_j = j
        if closest_d > distance(cur, dest):
            log.debug('reorder_way: break at %r out of %r', i, len(points))
            del points[i:]
            break
        points[i], points[closest_j] = points[closest_j], points[i]
        cur = points[i]
        i += 1

def distance(pt1, pt2):
    (lat1, lon1) = pt1
    (lat2, lon2) = pt2
    return (lat1 - lat2) ** 2 + (lon1 - lon2) ** 2
