from dataclasses import dataclass
import itertools
from pathlib import Path
import sys
import threading

import gi

gi.require_version('Gdk', '3.0')
gi.require_version('GdkPixbuf', '2.0')
gi.require_version('Gtk', '3.0')
from gi.repository import Gdk, GdkPixbuf, Gtk

import srub
import util.gtk
from util.gtk import idle_once
import util.logs

log = util.logs.getLogger(__name__)


class Window(Gtk.Window):

    def __init__(self, with_subtitle=True, **kwargs):
        kwargs.setdefault('title', 'Образ')
        super().__init__(**kwargs)
        util.gtk.destroy_on_delete(self)
        self.set_position(Gtk.WindowPosition.CENTER_ALWAYS)

        self.headerbar = Gtk.HeaderBar(has_subtitle=with_subtitle)
        self.set_titlebar(self.headerbar)

        self.left_label = Gtk.Label(xpad=9)
        self.headerbar.pack_start(self.left_label)

        self.right_label = Gtk.Label(xpad=9)
        self.headerbar.pack_end(self.right_label)

        self.headerbar.show_all()

        self.spinner = Gtk.Spinner()
        self.spinner.start()
        self.headerbar.pack_start(self.spinner)

        self.image = Gtk.Image()
        self.add(self.image)
        self.image.show()

        self.pics = []
        self.cur = 0
        self.total = None
        self.update()

        self.connect('key-press-event', self.on_key_press_event)

        self.fetcher = self.fetch_step = self.fetch_thread = None

    def update(self):
        if not self.pics:
            self.headerbar.set_title('')
            self.headerbar.set_subtitle('')
            self.left_label.set_text('')
            self.right_label.set_text('')
            self.image.set_from_icon_name('image-missing', Gtk.IconSize.DIALOG)
            return

        pic = self.pics[self.cur]
        self.headerbar.set_title(pic.title)
        self.headerbar.set_subtitle(pic.subtitle)
        total = self.total or len(self.pics)
        self.left_label.set_markup(f'<small>{1+self.cur} / {total}</small>')
        self.right_label.set_markup(pic.info or '')
        self.image.set_from_pixbuf(pic.pixbuf)
        min_size, natural_size = self.image.get_preferred_size()
        log.debug('update: min_size=%r, natural_size=%r', min_size, natural_size)
        self.resize(min_size.width, min_size.height)

    def add_pic(self, pic):
        self.pics.append(pic)
        self.update()

    def set_total(self, total):
        self.total = total

    def on_key_press_event(self, _, event):
        match Gdk.keyval_name(event.keyval):
            case 'Escape':
                self.destroy()

            case 'Left' | 'KP_Left':
                self.cur = (self.cur - 1) % len(self.pics)
                self.update()

            case 'Right' | 'KP_Right':
                self.cur = (self.cur + 1) % len(self.pics)
                if self.fetcher and self.cur >= len(self.pics) - self.fetch_step:
                    self.fetch_more()
                self.update()

            case _:
                return False

        return True

    def fetch(self, fetcher, step=5):
        self.fetcher = fetcher
        self.fetch_step = step
        self.fetch_more()

    def fetch_more(self):
        if self.fetch_thread and self.fetch_thread.is_alive():
            return
        self.fetch_thread = threading.Thread(target=self.run_fetcher, daemon=True)
        self.fetch_thread.start()

    def run_fetcher(self):
        idle_once(self.set_loading, True)
        try:
            for x in itertools.islice(self.fetcher, self.fetch_step):
                if isinstance(x, str):
                    idle_once(self.set_total, x)
                else:
                    idle_once(self.add_pic, x)
        finally:
            idle_once(self.set_loading, False)

    def set_loading(self, loading):
        self.spinner.set_visible(loading)


@dataclass
class Picture:

    title: str
    pixbuf: GdkPixbuf.Pixbuf
    subtitle: str = None
    info: str = None


def run(fetcher):
    srub.dark()
    win = Window()
    win.connect('destroy', lambda *_: Gtk.main_quit())
    win.fetch(fetcher)
    win.present()
    Gtk.main()


def main():
    run(pic for arg in sys.argv[1:] for pic in pics_from_path(Path(arg)))


def pics_from_path(path):
    if path.is_dir():
        for p in path.iterdir():
            yield from pics_from_path(p)
        return
    yield Picture(title=path.stem, subtitle=path.parent.name, pixbuf=GdkPixbuf.Pixbuf.new_from_file(str(path)))


if __name__ == '__main__':
    main()
