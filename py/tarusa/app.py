# pylint: disable=no-member

import dataclasses
from dataclasses import dataclass
import os
from pathlib import Path
import subprocess
from urllib.parse import urlparse

import gi
from shapely.geometry import Point

gi.require_version('GLib', '2.0')
gi.require_version('Gdk', '3.0')
gi.require_version('Gio', '2.0')
gi.require_version('Gtk', '3.0')
gi.require_version('OsmGpsMap', '1.0')
from gi.repository import GLib, Gdk, Gio, Gtk, OsmGpsMap
from gi.repository.GLib import markup_escape_text as escape

import srub
import trapdoor     # pylint: disable=unused-import
from util import ýaml
import util.logs
import windir

from tarusa import external, features, wikidata
from tarusa.common import parse_point

log = util.logs.getLogger(__name__)

app = Gtk.Application.get_default

survey_path = Path.home() / 'my/survey'


class Application(Gtk.Application):

    def __init__(self, *args, **kwargs):
        GLib.set_prgname('tarusa')
        super().__init__(*args, application_id=(None if os.environ.get('DEBUG') else 'org.codeberg.trigrax.tarusa'),
                         flags=Gio.ApplicationFlags.HANDLES_OPEN,  **kwargs)
        self.goto_history = srub.make_history()
        self.features = []

    @property
    def c(self):
        return self.get_active_window()

    def do_startup(self, *_):   # pylint: disable=arguments-differ
        srub.install_excepthook()
        srub.dark()
        Gtk.Application.do_startup(self)
        self.set_accels()
        self.load_features()

    def set_accels(self):
        srub.set_accels(self, [
            ('<Control>grave', self.open_console),
        ])

    def do_activate(self, *_):  # pylint: disable=arguments-differ
        log.debug('Application.activate')
        windir.best_window(self, Window, {}, windir.bulk).present()

    def do_open(self, targets, *_): # pylint: disable=arguments-differ
        (win := windir.best_window(self, Window, {}, windir.bulk)).present()
        for t in targets:
            (lat_s, lon_s, *maybe_zoom) = urlparse(t.get_uri()).path.strip('/').split(',')
            lat, lon = float(lat_s), float(lon_s)
            zoom = int(maybe_zoom[0]) if maybe_zoom else 11
            win.map.set_center_and_zoom(lat, lon, zoom)
            break

    def load_features(self):
        with srub.Progresser('загружаю') as pr:
            for f in (survey_path / 'features').glob('*.yaml'):
                pr.update(f.stem)
                self.features.extend(ýaml.unsafe_load(f.read_text()))

    def open_console(self):
        srub.open_console(Path(__file__).parent / 'app.scratch.py')


class Window(Gtk.ApplicationWindow):

    def __init__(self, application, **kwargs):
        super().__init__(application=application, title='Таруса', **kwargs)
        self.app = application
        util.gtk.destroy_on_delete(self)

        self.state = {}
        self.last_feature = None
        self.trail = []

        self.map = OsmGpsMap.Map(map_source=12345,    # with the default map_source=0 OsmGpsMap forces blank tiles
                                 repo_uri='https://a.tile-cyclosm.openstreetmap.fr/cyclosm/#Z/#X/#Y.png',
                                 tile_cache_base=str(Path.home() / 'my.bulk/map-cache'), expand=True,
                                 # Prevent dragging because it somehow messes up geographic_to_screen
                                 # and because it reveals grey areas which looks bad.
                                 drag_limit=99999999)
        self.map.set_keyboard_shortcut(OsmGpsMap.MapKey_t.ZOOMIN, Gdk.keyval_from_name('KP_Next'))
        self.map.set_keyboard_shortcut(OsmGpsMap.MapKey_t.ZOOMOUT, Gdk.keyval_from_name('KP_Prior'))
        self.map.connect('key-press-event', self.on_map_key_press_event)
        self.map.connect('button-press-event', self.on_map_button_press_event)
        self.map.connect('popup-menu', self.on_map_popup_menu)
        self.map.connect('notify::zoom', self.on_map_notify_zoom)
        self.add(self.map)
        self.map.show()

        self.layers = []

        srub.set_accels(self, [
            ('KP_Home', self.go_home),
            ('F3', self.go_to),
            ('F4', self.show_external),
            ('<Alt>F4', self.show_external_legend),
        ])

        self.go_home()

    def layer_add(self, layer):
        self.layers.append(layer)
        self.map.layer_add(layer)

    def layer_remove_all(self):
        self.map.layer_remove_all()
        self.layers[:] = []

    def layer_remove(self, layer):
        if self.map.layer_remove(layer):
            self.layers.remove(layer)

    def get_degree_bounds(self):
        (bbox_nw, bbox_se) = self.map.get_bbox()
        (max_lat, min_lon) = bbox_nw.get_degrees()
        (min_lat, max_lon) = bbox_se.get_degrees()
        return (min_lat, max_lat, min_lon, max_lon)

    def go_home(self):
        (lat, lon) = eval((Path.home() / 'my/setup/location.py').read_text())
        self.go_to_point(Point(lon, lat))

    @srub.cancelable
    def add_by(self, pred):
        with srub.Progresser('подгружаю') as pr:
            for feat in self.app.features:
                pr.update(debounce=1)
                if feat not in self.state and eval(pred.__code__, dict(features.__dict__, f=feat)):
                    self.add_feature(feat)

    @srub.cancelable
    def remove_by(self, pred):
        with srub.Progresser('выгружаю') as pr:
            for feat in list(self.state):
                pr.update(debounce=1)
                if eval(pred.__code__, dict(features.__dict__, f=feat)):
                    self.remove_feature(feat)

    @srub.cancelable
    def go_to(self):
        if not (s := srub.EntryDialog(history=self.app.goto_history, parent=self).do()):
            return

        if pt := parse_point(s):
            self.go_to_point(pt)
            return

        if not (found := srub.threaded('ищу в Викиданных', wikidata.search, s)):
            srub.MessageDialog(Gtk.MessageType.ERROR, 'Не нашлось', parent=self).do()
            return

        target = srub.ListDialog(found,
                                 markups=[f'{escape(it.titlesnippet)}\n<small>{escape(it.snippet)}</small>'
                                          for it in found], # pylint: disable=not-an-iterable
                                 texts=[it.snippet for it in found],    # pylint: disable=not-an-iterable
                                 parent=self, scrollable=True, default_width=600, default_height=800).do()
        if target is quit:
            return

        if not (pt := srub.threaded('достаю координаты', wikidata.point, target.ident)):
            # haswbstatement=P625 doesn't work, at least not always
            srub.MessageDialog(Gtk.MessageType.ERROR, 'У этого нет координат', parent=self).do()
            return

        self.go_to_point(pt, zoom=15)

    def go_to_point(self, pt, zoom=10):
        self.trail_add(pt)
        self.map.set_center_and_zoom(pt.y, pt.x, zoom)

    def show_external(self):
        external.show_map(self)

    def show_external_legend(self):
        external.show_legend(self)

    def trail_add(self, pt):
        self.trail.append(pt)

    def on_map_key_press_event(self, map_, event):
        name = Gdk.keyval_name(event.keyval)
        mod = event.state

        if direction := direction_keys.get(name):
            (shift_lat, shift_lon) = direction
            (min_lat, max_lat, min_lon, max_lon) = self.get_degree_bounds()
            shift_lat *= (max_lat - min_lat) / 10
            shift_lon *= (max_lon - min_lon) / 10
            if mod & Gdk.ModifierType.CONTROL_MASK:
                shift_lat /= 10
                shift_lon /= 10
            map_.set_center(map_.props.latitude + shift_lat, map_.props.longitude + shift_lon)

        else:
            return False

        return True

    def on_map_button_press_event(self, _, event):
        if event.type == Gdk.EventType.BUTTON_PRESS:
            if event.triggers_context_menu():
                self.popup_menu(event)
            else:
                self.handle_map_click(event)
            return True

        return False

    def on_map_popup_menu(self, _):
        self.popup_menu(event=None)
        return True

    def popup_menu(self, event=None):
        pt = OsmGpsMap.MapPoint.new_degrees(self.map.props.latitude, self.map.props.longitude)
        log.debug('popup_menu: map is at %r, %r', pt.rlat, pt.rlon)
        if event:
            pt = self.map.get_event_location(event)
            log.debug('popup_menu: event is at %r, %r', pt.rlat, pt.rlon)

        menu = Gtk.Menu()

        for feat in self.features_at(pt) if event else []:
            menu.add(item := Gtk.MenuItem.new_with_label(feat.title))
            item.props.sensitive = False
            for (name, url) in feat.lookups:
                menu.add(item := Gtk.MenuItem.new_with_mnemonic('_' + name))
                item.connect('activate', self.on_lookup_activate, feat, url)
            menu.add(Gtk.SeparatorMenuItem())

        menu.add(item := Gtk.MenuItem.new_with_mnemonic('_Координаты'))
        item.connect('activate', lambda _: copy_coords(pt))

        menu.add(Gtk.SeparatorMenuItem())

        for (name, url) in external.resources_at(pt, self.map.props.zoom):
            menu.add(item := Gtk.MenuItem.new_with_mnemonic('_' + name))
            item.connect('activate', lambda _, url: self.open_resource(url), url)

        for item in menu.get_children():
            item.connect('activate', lambda _: menu.destroy())

        menu.show_all()

        # The supposed simple menu.popup_at_widget(..., event) doesn't work
        # (nothing pops up and also keyboard input gets broken for a moment).
        if event:
            menu.popup_at_pointer(event)
        else:
            menu.popup_at_widget(self.map, Gdk.Gravity.SOUTH_EAST, Gdk.Gravity.SOUTH_EAST, None)

    def handle_map_click(self, event):
        pt = self.map.get_event_location(event)
        self.trail_add(pt)
        for feat in self.features_at(pt):
            for (_, url) in feat.lookups:
                self.open_resource(url)
                self.last_feature = feat
                return

    def on_lookup_activate(self, _, feat, url):
        self.open_resource(url)
        self.last_feature = feat

    def add_feature(self, feat):
        if feat not in self.state:
            state = self.state[feat] = FeatureState()
            self.render(feat, state)

    def remove_feature(self, feat):
        self.unrender(self.state.pop(feat))

    def render(self, feat, state):
        if (not state.image) and (preimage := self.make_preimage(feat, state)):
            state.image = self.map.image_add_with_alignment(preimage.point.y, preimage.point.x, preimage.pixbuf,
                                                            preimage.xalign, preimage.yalign)
            state.image_bbox = image_bbox(self.map, state.image)

        if (not state.track) and (track := self.make_track(feat, state)):
            self.map.track_add(track)
            state.track = track

        log.debug('render %s %r: %r', type(feat).__name__, feat.title, state)

    def unrender(self, state):
        if state.image_bbox:
            self.map.image_remove(state.image)
            state.image = state.image_bbox = None

        if state.track:
            self.map.track_remove(state.track)
            state.track = None

    def rerender(self, feat, state):
        self.unrender(state)
        self.render(feat, state)

    def make_preimage(self, feat, state):
        if (p := state.preimages_cache.get((zoom := self.map.props.zoom), ...)) is ...:
            p = state.preimages_cache[zoom] = feat.make_preimage(zoom)
            if p:
                log.debug('make_preimage: %s: %rx%r', feat.title, p.pixbuf.get_width(), p.pixbuf.get_height())
        return p

    def make_track(self, feat, state):
        if (t := state.tracks_cache.get((zoom := self.map.props.zoom), ...)) is ...:
            t = state.tracks_cache[zoom] = feat.make_track(zoom)
            if t:
                log.debug('make_track: %s: %r points', feat.title, t.n_points())
        return t

    def features_at(self, pt):
        for feat, state in self.state.items():
            if bbox_contains(state.image_bbox, pt):
                yield feat

    @srub.cancelable
    def on_map_notify_zoom(self, *_):
        with srub.Progresser('перерисовываю') as pr:
            for i, (feat, state) in enumerate(self.state.items()):
                self.rerender(feat, state)
                pr.update(fraction=i / len(self.state), debounce=1)

    def open_resource(self, url):
        if url.startswith('data:'):
            subprocess.run(('xw', 'lowbrow', url), check=True)
        else:
            Gtk.show_uri_on_window(self, url, Gdk.CURRENT_TIME)


@dataclass
class FeatureState:

    preimages_cache: dict[int, features.Preimage] = dataclasses.field(default_factory=dict)
    tracks_cache: dict[int, OsmGpsMap.MapTrack] = dataclasses.field(default_factory=dict)

    image: OsmGpsMap.MapImage = None
    image_bbox: tuple[tuple[float, float], tuple[float, float]] = None
    track: OsmGpsMap.MapTrack = None


def image_bbox(map_, image):
    (x, y) = map_.convert_geographic_to_screen(image.props.point)
    width = image.props.pixbuf.get_width()
    height = image.props.pixbuf.get_height()
    xalign = image.props.x_align
    yalign = image.props.y_align
    return (map_.convert_screen_to_geographic(x - width*xalign, y - height*yalign),
            map_.convert_screen_to_geographic(x + width*(1-xalign), y + height*(1-yalign)))


def bbox_contains(bbox, pt):
    return bbox and bbox[0].rlat >= pt.rlat >= bbox[1].rlat and bbox[0].rlon <= pt.rlon <= bbox[1].rlon


def copy_coords(pt: OsmGpsMap.MapPoint):
    (lat, lon) = pt.get_degrees()
    log.debug('copy_coords: %r, %r', lat, lon)
    Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD).set_text(f'{lat:.4f}, {lon:.4f}', -1)


direction_keys = {
    'Up':       (+1,  0),
    'KP_Up':    (+1,  0),
    'Right':    ( 0, +1),
    'KP_Right': ( 0, +1),
    'Down':     (-1,  0),
    'KP_Down':  (-1,  0),
    'Left':     ( 0, -1),
    'KP_Left':  ( 0, -1),
}
