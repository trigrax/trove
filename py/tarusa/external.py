from collections import namedtuple
from dataclasses import dataclass
from datetime import datetime
from pathlib import Path

import gi
import numpy
import scipy.interpolate
import scipy.optimize
import yaml

gi.require_version('GObject', '2.0')
gi.require_version('Gdk', '3.0')
gi.require_version('GdkPixbuf', '2.0')
gi.require_version('Gtk', '3.0')
gi.require_version('OsmGpsMap', '1.0')
from gi.repository import GObject, Gdk, GdkPixbuf, Gtk, OsmGpsMap

import srub
import tamala
from util import ýaml
from util.color import rgb
from util.iter import first
import util.logs
import windir

from tarusa import draw
import tarusa.common    # pylint: disable=unused-import     # for YAML representers

log = util.logs.getLogger(__name__)


ImagePoint = namedtuple('ImagePoint', ('x', 'y'))
ControlPoint = namedtuple('ControlPoint', ('image_point', 'point'))

@dataclass
class Map:

    title: str
    url: str
    source: str
    alpha: float = 0.5
    nw: ImagePoint = None
    se: ImagePoint = None
    legend_nw: ImagePoint = None
    legend_se: ImagePoint = None
    control_points: list[ControlPoint] = None

    def prepare(self):
        return GdkPixbuf.Pixbuf.new_from_file(str(tamala.stashed(self.url, 'tarusa')))

    def extract_legend(self, prepared):
        orig = prepared
        return orig.new_subpixbuf(self.legend_nw.x, self.legend_nw.y,
                                  self.legend_se.x - self.legend_nw.x, self.legend_se.y - self.legend_nw.y)

    def extract(self, prepared, map_):
        imgpt1, imgpt2 = self.best_rectangle(map_)
        canvas = GdkPixbuf.Pixbuf.new(width=imgpt2.x - imgpt1.x, height=imgpt2.y - imgpt1.y,
                                      colorspace=GdkPixbuf.Colorspace.RGB, has_alpha=True, bits_per_sample=8)
        canvas.fill(0)  # transparent black
        orig = prepared
        orig.copy_area(src_x=(src_x := max(self.nw.x, imgpt1.x)), src_y=(src_y := max(self.nw.y, imgpt1.y)),
                       width=min(self.se.x, imgpt2.x) - src_x, height=min(self.se.y, imgpt2.y) - src_y,
                       dest_x=max(0, self.nw.x - imgpt1.x), dest_y=max(0, self.nw.y - imgpt1.y),
                       dest_pixbuf=canvas)

        alloc = map_.get_allocation()
        return canvas.scale_simple(alloc.width, alloc.height, GdkPixbuf.InterpType.BILINEAR)

    def best_rectangle(self, target):
        (nw_pt, se_pt) = target.get_bbox()
        center_pt = OsmGpsMap.MapPoint(rlat=(nw_pt.rlat + se_pt.rlat) / 2, rlon=(nw_pt.rlon + se_pt.rlon) / 2)

        nw_guess = self._estimate_image_point(nw_pt)
        se_guess = self._estimate_image_point(se_pt)
        initial_guess = numpy.array([*nw_guess, *se_guess])

        result = scipy.optimize.minimize(lambda rect: self.error_function(target, target.get_allocation(),
                                                                          nw_pt, center_pt, se_pt, rect),
                                         initial_guess, method='L-BFGS-B')
        log.debug('best_rectangle: initial_guess=%r, result=%r', initial_guess, result)

        if not result.success:
            raise RuntimeError(result.message)
        (nw_x, nw_y, se_x, se_y) = result.x
        return ImagePoint(nw_x, nw_y), ImagePoint(se_x, se_y)

    def error_function(self, target, target_allocation, nw_pt, center_pt, se_pt, candidate):
        (nw_x, nw_y, se_x, se_y) = candidate
        x_distension = target_allocation.width / (se_x - nw_x)
        y_distension = target_allocation.height / (se_y - nw_y)

        total_error = 0

        for ctl in self.control_points:
            (our_x, our_y) = target.convert_geographic_to_screen(ctl.point)
            their_x = (ctl.image_point.x - nw_x) * x_distension
            their_y = (ctl.image_point.y - nw_y) * y_distension
            error = (our_x - their_x) ** 2 + (our_y - their_y) ** 2

            if nw_pt.rlat >= ctl.point.rlat >= se_pt.rlat and nw_pt.rlon <= ctl.point.rlon <= se_pt.rlon:
                weight = 9000
            else:
                weight = (ctl.point.rlat - center_pt.rlat) ** 2 + (ctl.point.rlon - center_pt.rlon) ** 2

            total_error += error * weight

        return total_error

    def _estimate_image_point(self, pt):
        points_array = numpy.array([[pt.rlat, pt.rlon] for (_, pt) in self.control_points])
        imgpts_array = numpy.array([[x, y] for ((x, y), _) in self.control_points])
        rbf_x = scipy.interpolate.RBFInterpolator(points_array, imgpts_array[:, 0])
        rbf_y = scipy.interpolate.RBFInterpolator(points_array, imgpts_array[:, 1])
        pt_array = numpy.array([[pt.rlat, pt.rlon]])
        x = rbf_x(pt_array)[0]
        y = rbf_y(pt_array)[0]
        log.debug('_estimate_image_point: %r: x=%r, y=%r', pt, x, y)
        return ImagePoint(int(round(x)), int(round(y)))


def load_maps():
    return yaml.unsafe_load(maps_path.read_text())

def save_maps(maps):
    maps_path.write_text(ýaml.dump(maps, spacing=1))

maps_path = Path.home() / 'my/survey/maps.yaml'


def show_map(win):
    maps = load_maps()
    map_ = srub.ListDialog([None] + maps, texts=[''] + [map_.title for map_ in maps], parent=win).do()
    if map_ is quit:
        return
    if map_ is None:
        win.layer_remove_all()
        return

    if map_.control_points:
        win.layer_add(Layer(win.map, map_))
    else:
        dialog = ControlPointsDialog(map_, parent=win)
        dialog.connect('response', save_control_points, maps, win)
        dialog.present()


def save_control_points(dialog, resp, maps, win):
    dialog.destroy()
    if Gtk.ResponseType(resp) is not Gtk.ResponseType.OK:
        return

    map_ = dialog.map
    [map_.nw, map_.legend_nw, map_.legend_se, map_.se, *image_points] = dialog.image_points
    points = win.trail[-len(image_points):]
    map_.control_points = [ControlPoint(imgpt, pt) for (imgpt, pt) in zip(image_points, points)]
    save_maps(maps)


class ControlPointsDialog(srub.Dialog):

    def __init__(self, map_, **kwargs):
        kwargs.setdefault('title', map_.title)
        kwargs.setdefault('default_width', 900)
        kwargs.setdefault('default_height', 500)
        super().__init__(use_header_bar=True, **kwargs)

        self.map = map_
        self.image_points = []

        self.scroller = Gtk.ScrolledWindow(expand=True)
        self.get_content_area().add(self.scroller)
        self.scroller.add(Gtk.Image.new_from_file(str(tamala.stashed(map_.url, 'tarusa'))))
        self.scroller.add_events(Gdk.EventMask.BUTTON_PRESS_MASK)
        self.scroller.connect('button-press-event', self.on_scroller_button_press_event)

        self.add_button('_Годится', Gtk.ResponseType.OK)
        self.add_button('Отбой', Gtk.ResponseType.CANCEL)
        self.set_default_response(Gtk.ResponseType.OK)
        util.gtk.mark_suggested(self.get_widget_for_response(Gtk.ResponseType.OK))

        self.update_header()
        self.show_all()

    def on_scroller_button_press_event(self, _, event):
        x = int(round(event.x + self.scroller.get_hadjustment().get_value()))
        y = int(round(event.y + self.scroller.get_vadjustment().get_value()))
        self.image_points.append(ImagePoint(x, y))
        log.info('ControlPointsDialog: image_points=%r', self.image_points)
        self.update_header()

    def update_header(self):
        hbar = self.get_header_bar()
        hbar.set_title(self.map.title)
        hbar.set_subtitle(f'нажато: {len(self.image_points)}')


class Layer(GObject.Object, OsmGpsMap.MapLayer):

    def __init__(self, target, map_):
        self.target = target
        self.map = map_
        self.prepared = map_.prepare()
        self.position = self.pixbuf = None
        self.images = []
        GObject.Object.__init__(self)

    def do_render(self, _):
        pt1, pt2 = self.target.get_bbox()
        log.debug('Layer.render: %r - %r', pt1.get_degrees(), pt2.get_degrees())
        if (position := (pt1.rlat, pt1.rlon, pt2.rlat, pt2.rlon)) == self.position:
            return
        self.position = position
        self.pixbuf = self.map.extract(self.prepared, self.target)

    def do_draw(self, _, ctx):
        log.debug('Layer.draw: %r on %r', self.pixbuf, ctx)
        if self.pixbuf:
            Gdk.cairo_set_source_pixbuf(ctx, self.pixbuf, 0, 0)
            ctx.paint_with_alpha(self.map.alpha)

    def do_busy(self):                    return False
    def do_button_press(self, _, _event): pass

    def display_control_points(self):
        self.undisplay_control_points()
        for ctl in self.map.control_points:
            pixbuf = to_pixbuf(draw.badge(lines=[], icon=draw.Icon('home', 24, rgb(0xFFFFFF)),
                                          background_color=rgb(0x4D0021)))
            self.images.append(self.target.image_add(*ctl.point.get_degrees(), pixbuf))

    def undisplay_control_points(self):
        for img in self.images:
            self.target.image_remove(img)


def to_pixbuf(surface):
    return Gdk.pixbuf_get_from_surface(surface, 0, 0, surface.get_width(), surface.get_height())


def show_legend(win):
    if layer := first(layer for layer in win.layers if isinstance(layer, Layer)):
        legend = layer.map.extract_legend(layer.prepared)
        legend.savev(path := f'/tmp/{layer.map.title}.legend.png', 'png', option_keys=[], option_values=[])
        windir.load()
        windir.launch_at(('xw', 'feh', '--geometry', windir.left.to_geometry(),
                          '--auto-zoom', '--scale-down', '--image-bg=#333333', path),
                         windir.left, class_group='feh')


def resources_at(pt, zoom):
    (lat, lon) = pt.get_degrees()
    z = zoom - 1
    return [
        ('Яндекс Карты', f'https://yandex.ru/maps/?ll={lon}%2C{lat}&z={z}&l=sat%2Cskl'),
        ('Google Maps', f'https://www.google.com/maps/@{lat},{lon},{z}z'),
        ('topographic-map.com', f'https://ru-ru.topographic-map.com/map/?center={lat}%2C{lon}&zoom={z}'),
        ('PastVu', f'https://pastvu.com?g={lat},{lon}&z={z}&s=osm&t=mapnik&type=1&y=1945&y2=1990'),
        ('35awards.com', f'https://35awards.com/map#lt={lat}&ln={lon}&z={z}'),
        ('SunCalc', f'https://www.suncalc.org/#/{lat},{lon},{z}/' + datetime.now().strftime('%Y.%m.%d/%H:%M/1/3')),
    ]
