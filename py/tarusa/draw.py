# pylint: disable=no-member

from collections import namedtuple
import pkgutil

import cairo
import gi

gi.require_version('Gio', '2.0')
gi.require_version('Rsvg', '2.0')
from gi.repository import Gio, Rsvg

from util.color import unrgb
import util.logs

log = util.logs.getLogger(__name__)


Line = namedtuple('Line', ['text', 'size', 'color'])

Icon = namedtuple('Icon', ['name', 'size', 'color'])


def badge(lines, icon, background_color,
          padding=6, border_color=..., border_width=0, margin=1, opacity=0.9, shadow_depth=1):
    if border_color is ...:
        border_color = icon.color if icon else lines[0].color

    width = height = x = y = 0

    around = shadow_depth + margin + border_width + padding
    x += around
    y += around

    if icon:
        x += icon.size + padding
        width = around + icon.size + around

    line_positions = []
    line_spacing = padding
    hack = 2
    ctx = cairo.Context(cairo.ImageSurface(cairo.FORMAT_ARGB32, 0, 0))
    for i, line in enumerate(lines):
        if i > 0:
            y += line_spacing
        ctx.set_font_size(line.size)
        extents = ctx.text_extents(line.text)
        y -= hack + extents.y_bearing
        line_positions.append(pos := (int(round(x - hack - extents.x_bearing)), int(round(y))))
        log.debug('line %d %r: extents=%r, pos=%r', i, line.text, extents, pos)
        width = max(width, int(round(x + (extents.width - extents.x_bearing) + around)))

    if icon:
        y = max(y, around + icon.size)

    height = int(round(y + around))

    surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, width, height)
    ctx = cairo.Context(surface)
    ctx.set_line_width(1)

    depth = 0

    # Shadow
    for _ in range(shadow_depth):
        ctx.set_source_rgba(0, 0, 0, 0.15*(depth+1))
        rect(ctx, depth, depth, width-1-depth, height-1-depth)
        depth += 1

    # Background
    ctx.set_source_rgba(*background_color, opacity)
    rect_path(ctx, depth, depth, width-1-depth, height-1-depth)
    ctx.fill()

    depth += margin

    # Border
    for _ in range(border_width):
        ctx.set_source_rgba(*border_color, 1)
        rect(ctx, depth, depth, width-1-depth, height-1-depth)
        depth += 1

    depth += padding

    if icon:
        handle = load_icon(icon.name, icon.color)
        viewport = Rsvg.Rectangle()
        viewport.x = viewport.y = depth
        viewport.width = viewport.height = icon.size
        handle.render_document(ctx, viewport)

    for i, line in enumerate(lines):
        ctx.set_source_rgba(*line.color, 1)
        ctx.set_font_size(line.size)
        ctx.move_to(*line_positions[i])
        ctx.show_text(line.text)

    return surface


def rect(ctx, x1, y1, x2, y2):
    rect_path(ctx, x1, y1, x2, y2)
    ctx.stroke()


def rect_path(ctx, x1, y1, x2, y2):
    ctx.move_to(x1 + 0.5, y1 + 0.5)
    ctx.line_to(x2 - 0.5, y1 + 0.5)
    ctx.line_to(x2 - 0.5, y2 - 0.5)
    ctx.line_to(x1 + 0.5, y2 - 0.5)
    ctx.close_path()


def load_icon(name, color):
    if (key := (name, color)) not in icon_handles_cache:
        icon_handles_cache[key] = _load_icon(name, color)
    return icon_handles_cache[key]

icon_handles_cache = {}

def _load_icon(name, color):
    data = pkgutil.get_data('tarusa', f'icons/{name}.svg')
    data = data.replace(b'<path ', f'<path fill="#{unrgb(color)}" '.encode())
    log.debug('_load_icon %s %s: %r', name, color, data)
    stream = Gio.MemoryInputStream.new_from_data(data)
    return Rsvg.Handle.new_from_stream_sync(stream, base_file=None, flags=Rsvg.HandleFlags.FLAGS_NONE,
                                            cancellable=None)
