from dataclasses import dataclass
from datetime import date, timedelta
import html
import re
import sys

import gi
import mechanicalsoup
import roman

gi.require_version('GLib', '2.0')
gi.require_version('GdkPixbuf', '2.0')
gi.require_version('Gio', '2.0')
from gi.repository import GLib, GdkPixbuf, Gio
from gi.repository.GLib import markup_escape_text as escape

import tamala
from tarusa import depict
from util.iter import first, true


class Agent(mechanicalsoup.StatefulBrowser):

    def __init__(self, **kwargs):
        super().__init__(session=tamala.session, user_agent='Tarusa python-requests', **kwargs)

    def open_gallery(self, keyword=None, year=None, month=None, page_size=48):
        params = {'SHOW_ALL': 1, 'HOWMANY': page_size}
        if keyword:
            params['DESCR'] = keyword
        if year:
            params['TAKENON'] = year
        if month:
            params['MONTH'] = month
        self.open('https://www.parovoz.com/newgallery/', params=params, timeout=15)

    def pagination(self):
        return self.get_current_page().find('font', size='-1').find_all(('a', 'b'))

    def total(self):
        return self.pagination()[-1].get_text().split('-')[-1]

    def next_page_link(self):
        past = False
        for tag in self.pagination():
            if past:
                return tag
            past = tag.name == 'b'
        return None

    def photos(self):
        thumbnails = {}

        for link in self.links(url_regex='pg_view'):
            if link.img:
                thumbnails[link['href']] = self.absolute_url(link.img['src'])
                continue

            ph = Photo(thumbnail_url=thumbnails.pop(link['href']), title=link.get_text())

            if (i := link.find_next_sibling('i')):
                ph.region = i.get_text()

            for s in link.parent.find_all(string=date_format):
                if 'опубл.' in s:
                    ph.published = extract_date(s)
                else:
                    ph.taken = extract_date(s)

            yield ph

    def depict(self):
        yield self.total()

        for photo in self.photos():
            resp = self.get(photo.image_url)
            resp.raise_for_status()
            stream = Gio.MemoryInputStream.new_from_bytes(GLib.Bytes(resp.content))
            pixbuf = GdkPixbuf.Pixbuf.new_from_stream(stream, None)
            title, subtitle = titles(photo)
            yield depict.Picture(title, pixbuf, subtitle, info(photo))

        if link := self.next_page_link():
            self.follow_link(link)
            yield from self.depict()


@dataclass
class Photo:

    thumbnail_url: str
    title: str
    region: str = None
    taken: date = None
    published: date = None

    @property
    def image_url(self):
        return self.thumbnail_url.replace('/icons', '').replace('-s', '')


def titles(photo):
    if (sep := ', ') not in (s := photo.title):
        return lower_first(cleanup(s)), ''
    t1, _, t2 = s.partition(sep)
    return cleanup(t2), lower_first(cleanup(t1))


def info(photo):
    return '<small>' + '\n'.join(true(escape(photo.region),
                                      photo.taken and photo.taken.strftime('%B %Y'))) + '</small>'

def cleanup(s):
    for (pat, rep) in ((r'\s*{[^}]+}', r''),
                       (r' - ', r' — ')):
        s = re.sub(pat, rep, s)
    return escape(s)


def lower_first(s):
    return (s[0].lower() + s[1:]) if s else s


date_format = re.compile(r'(сегодня|вчера|([0-9]{1,2}).([IVX]{1,4}).([0-9]{4}))')

def extract_date(s):
    match re.search(date_format, s).groups():
        case ('сегодня', *_):
            return date.today()
        case ('вчера', *_):
            return date.today() - timedelta(days=1)
        case (_, d, m, y):
            return date(int(y), roman.fromRoman(m), int(d))
    return None


def fix_escaping(s):
    return html.escape(html.unescape(s))


def main():
    agent = Agent()
    agent.open_gallery(first(sys.argv[1:]))
    depict.run(agent.depict())


if __name__ == '__main__':
    main()
