import html
import html.parser


class MarkupConverter(html.parser.HTMLParser):

    @classmethod
    def convert(cls, html_code):
        c = cls()
        c.feed(html_code)
        c.close()
        return ''.join(c.result)

    tags = {
        'b':      'b',
        'em':     'i',
        'i':      'i',
        'strong': 'b',
        'u':      'u',
    }

    def __init__(self):
        super().__init__()
        self.result = []

    def handle_starttag(self, tag, attrs):
        match tag:
            case _ if newtag := self.tags.get(tag):
                self.result.append(f'<{newtag}>')
            case 'br':
                self.result.append('\n')

    def handle_endtag(self, tag):
        match tag:
            case _ if newtag := self.tags.get(tag):
                self.result.append(f'</{newtag}>')

    def handle_data(self, data):
        self.result.append(html.escape(data))

    def get_pango_markup(self):
        return ''.join(self.result)


if __name__ == '__main__':
    print(MarkupConverter.convert(input()))
