from datetime import date, datetime, timedelta
from pathlib import Path

import astral
import dateutil.tz


def location():
    name = ''
    region = 'Russia'
    (lat, lon) = eval((Path.home() / 'my/setup/location.py').read_text())
    tzname = 'Europe/Moscow'
    elevation = 100
    return astral.Location((name, region, lat, lon, tzname, elevation))


def is_sunny():
    loc = location()
    return loc.sunrise() <= datetime.now(dateutil.tz.tzlocal()) <= loc.sunset()


def pivots():
    loc = location()
    morrow = date.today() + timedelta(days=1)
    return (loc.dawn(),
            loc.solar_noon(),
            loc.time_at_elevation(10, astral.SUN_SETTING),
            loc.sunset(),
            loc.dusk(),
            loc.solar_midnight(morrow),
            loc.time_at_elevation(-10, astral.SUN_RISING, morrow))



if __name__ == '__main__':
    print(*(t.strftime('%H:%M') for t in pivots()))
