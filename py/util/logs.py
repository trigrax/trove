from pathlib import Path
import logging
import multiprocessing
import os
import sys

import xdg.BaseDirectory


getLogger = logging.getLogger


def debug(enable=True):
    logging.getLogger().setLevel(logging.DEBUG if enable else logging.INFO)


if (_process := multiprocessing.current_process()).name == 'MainProcess':
    if (_main_path := Path(getattr(sys.modules['__main__'], '__file__', ''))):
        _process.name = str(_main_path.relative_to(_base) if (_base := Path.home() / 'my/py') in _main_path.parents
                            else _main_path)

logging.basicConfig(
    filename=Path(xdg.BaseDirectory.get_runtime_dir()) / 'py.log',
    level=logging.DEBUG if os.environ.get('DEBUG') else logging.INFO,
    format='%(asctime)s  %(process)-6d %(levelname)-7s %(processName)s: %(name)s: %(message)s', datefmt='%H:%M:%S',
)


def _wrap(orig):
    def excepthook(type_, value, traceback):
        if issubclass(type_, Exception):     # ignore KeyboardInterrupt and other direct BaseException descendants
            logging.getLogger().error('uncaught exception', exc_info=(type_, value, traceback))
        orig(type_, value, traceback)
    return excepthook

sys.excepthook = _wrap(sys.excepthook)
