import functools

import gi

gi.require_version('GLib', '2.0')
gi.require_version('Gdk', '3.0')
gi.require_version('Gtk', '3.0')
from gi.repository import GLib, Gdk, Gtk

import util.logs
import util.strings

log = util.logs.getLogger(__name__)


def idle_once(f, *args, **kwargs):
    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        f(*args, **kwargs)
        return False
    GLib.idle_add(wrapper, *args, **kwargs)


def connect_limit(obj, signal, n, f, *args, **kwargs):
    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        nonlocal n
        if (n := n - 1) == 0:
            obj.disconnect_by_func(wrapper)
        f(*args, **kwargs)
    obj.connect(signal, wrapper, *args, **kwargs)


def process_events():
    while Gtk.events_pending():
        Gtk.main_iteration()


def mark_suggested(widget):
    widget.get_style_context().add_class('suggested-action')

def mark_destructive(widget):
    widget.get_style_context().add_class('destructive-action')


def focus(widget):
    if widget.is_visible():
        if widget.get_can_focus() and not widget.has_focus() and not isinstance(widget, Gtk.Bin):
            widget.grab_focus()
            return True
        if isinstance(widget, Gtk.Container):
            for child in widget.get_children():
                if focus(child):
                    return True
    return False


def monitor_geometry():
    return Gdk.Display.get_default().get_primary_monitor().get_geometry()


def toggle_dark_theme():
    p = Gtk.Settings.get_default().props
    p.gtk_application_prefer_dark_theme = not p.gtk_application_prefer_dark_theme


def destroy_on_delete(win):
    win.connect('delete-event', lambda _win, _: win.destroy())
