def replace_bad_chars(s, rep='_'):
    """Return `s` with reserved filename characters replaced with `rep`."""
    for c in r'<>:"/\\|?*':
        s = s.replace(c, rep(c) if callable(rep) else rep)
    return s


MAX_FILENAME_LENGTH = 255
