def force_str(x):
    return x if isinstance(x, str) else x.decode()


def joinlines(ss):
    return ''.join(s + '\n' for s in ss)


def name_to_title(name):
    return ' '.join(_TITLE_SUBST.get(word, word.capitalize() if i == 0 else word)
                    for i, word in enumerate(name.replace('_', ' ').split()))

_TITLE_SUBST = {word.lower(): word for word in (
    'Firefox',
    'I',
    'II',
    'III',
    'NW',
    'SE',
    'TLS',
    'URI',
    'URL',
    'VK',
    'Yandex',
)}


def is_cyrillic(s):
    return all(0x400 <= ord(c) <= 0x4FF for c in s)
