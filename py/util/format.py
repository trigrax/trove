from datetime import timedelta


def number(n):
    s = str(n)
    if len(s) > 6:
        return f'{s[:-6]} {s[-6:-3]} {s[-3:]}'
    if len(s) > 4:
        return f'{s[:-3]} {s[-3:]}'
    return s


def shorten_seq(seq, target=30):
    return str(seq) if (length := len(seq)) <= target else str(seq[:target]) + f'[...+{length-target}]'


def format_timedelta(d):
    total = int(d.total_seconds())
    seconds = total % 60
    minutes = int(total / 60) % 60
    hours = int(int(total / 60) / 60)
    return f'{hours}:{minutes:02}:{seconds:02}' if hours else f'{minutes}:{seconds:02}'


def parse_timedelta(s):
    ss = s.split(':')
    return timedelta(hours=int(ss[-3]) if len(ss) == 3 else 0,
                     minutes=int(ss[-2]), seconds=int(ss[-1]))
