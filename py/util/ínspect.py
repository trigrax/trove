from util.iter import listify


@listify
def recursive_subclasses(cls):
    for sub in cls.__subclasses__():
        yield sub
        yield from recursive_subclasses(sub)
