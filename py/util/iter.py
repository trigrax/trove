from functools import wraps


def find(seq, x, sentinel=None):
    try:
        return seq.index(x)
    except ValueError:
        return sentinel


def first(xs, default=None):
    for x in xs:
        return x
    return default


def single(xs):
    [x] = xs
    return x


def true(*xs):
    return [x for x in xs if x]


def listify(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        return list(f(*args, **kwargs))
    return wrapper
