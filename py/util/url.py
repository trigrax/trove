import urllib.parse

from util.iter import listify


@listify
def realms(url):
    parsed = urllib.parse.urlparse(url)
    domains = parsed.netloc.strip('.').split('.')
    path = p.split('/') if (p := parsed.path.strip('/')) else []
    for i in range(len(domains)):
        yield '.'.join(domains[-i-1:])
    for i in range(len(path)):
        yield '/'.join([parsed.netloc] + path[:i+1])


def realm(url):
    parsed = urllib.parse.urlparse(url)
    r = parsed.netloc
    path = p.split('/') if (p := parsed.path.strip('/')) else []
    if r in {'2ch.hk', 'yandex.ru', 'google.com', 'vk.com', 'm.vk.com', 't.me'} and path:
        r += '/' + path[0]
        if r == 't.me/s' and len(path) > 1:
            r += '/' + path[1]
    return r


def readable(url):
    url = url.removeprefix('http://')
    url = url.removeprefix('https://')
    url = url.strip('/')
    return url
