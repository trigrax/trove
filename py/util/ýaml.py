import yaml
from yaml import safe_load, unsafe_load     # pylint: disable=unused-import
import yaml.dumper


def dump(data, stream=None, Dumper=yaml.dumper.Dumper, *, spacing=0, **kwargs):
    kwargs.setdefault('allow_unicode', True)
    kwargs.setdefault('sort_keys', False)
    if spacing > 0:
        Dumper = make_spacing_dumper(spacing)
    return yaml.dump(data, stream=stream, Dumper=Dumper, **kwargs)


class flowlist(list):
    pass

def _represent_flowlist(dumper, data):
    return dumper.represent_sequence('tag:yaml.org,2002:seq', data, flow_style=True)

yaml.add_representer(flowlist, _represent_flowlist)


def make_spacing_dumper(spacing):   # https://github.com/yaml/pyyaml/issues/127#issuecomment-525800484
    class SpacingDumper(yaml.dumper.Dumper):
        def write_line_break(self, data=None):
            super().write_line_break(data)
            if len(self.indents) <= spacing:
                super().write_line_break()
    return SpacingDumper
