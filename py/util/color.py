import gi

gi.require_version('Gdk', '3.0')
from gi.repository import Gdk


def rgb(x):
    r = ((x >> 16) % 0x100) / 0xFF
    g = ((x >>  8) % 0x100) / 0xFF
    b = ((x >>  0) % 0x100) / 0xFF
    return (r, g, b)


def unrgb(color):
    (r, g, b) = color
    return f'{int(r*0xFF):02X}{int(g*0xFF):02X}{int(b*0xFF):02X}'


def to_gdk(color):
    gdk = Gdk.RGBA()
    (gdk.red, gdk.green, gdk.blue) = color
    return gdk
