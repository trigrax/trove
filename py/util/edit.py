from pathlib import Path
import subprocess
import tempfile

from util import ýaml
import util.logs

log = util.logs.getLogger(__name__)


def inplace(objects):
    _, path = tempfile.mkstemp(suffix='.yaml')
    Path(path).write_text(ýaml.dump(objects, spacing=1))
    subprocess.run(('autoemacs', path), check=True)
    if not (result := Path(path).read_text()):
        return None
    new_objects = ýaml.unsafe_load(result)
    assert len(new_objects) == len(objects)

    removed = []
    i = j = 0
    while i < len(objects):
        log.debug('inplace: %d/%d: %r - %r', i, j, objects[i], new_objects[j])
        if new_objects[j] is None:
            removed.append(objects[i])
            del objects[i]
        else:
            objects[i].__class__ = new_objects[j].__class__
            objects[i].__dict__ = new_objects[j].__dict__
            i += 1
        j += 1
    return removed
