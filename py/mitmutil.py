"""Utilities for mitmproxy scripting.

Put into ``mock.py``:

    from mitmutil import *

    def request(flow):
        flow.response = text_response()

Then run::

    mitmproxy -s mock.py -m reverse:localhost:25

"""

import email
import json

from mitmproxy import ctx   # pylint: disable=unused-import
import mitmproxy.http


def make_response(status_code=200, content=b'', headers=None, content_type=None):
    headers = dict(headers or {})
    headers.setdefault('Date', email.utils.formatdate(usegmt=True))
    if content_type:
        headers['Content-Type'] = content_type
    return mitmproxy.http.Response.make(status_code, content, headers)


def text_response(text='Hello world!\r\n', status_code=200, headers=None):
    return make_response(status_code, text, headers, content_type='text/plain; charset=utf-8')


def json_response(data=None, status_code=200, headers=None):
    if not isinstance(data, bytes):
        data = json.dumps(data, indent=4, ensure_ascii=False).encode('utf-8')
    return make_response(status_code, data, headers, content_type='application/json')
