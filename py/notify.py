from datetime import timedelta
from pathlib import Path
import subprocess


def send(summary, body=None, icon=None, expire_time=None):
    subprocess.run((['notify-send']) +
                   (['--icon', icon] if icon else []) +
                   (['--expire-time', str(int(expire_time / timedelta(milliseconds=1)))] if expire_time else []) +
                   ([summary]) +
                   ([body] if body else []),
                   check=True)


def ring(sound):
    subprocess.run(('xw', 'paplay', str(Path('/usr/share/sounds') / sound)), check=True)
