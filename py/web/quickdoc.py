from collections import namedtuple
import itertools
from pathlib import Path
import re
from urllib.parse import urlencode, urljoin, urlparse

import bottle
import bs4
import dominate
import dominate.tags as h
import requests_file

import tamala
import util.logs

log = util.logs.getLogger(__name__)
bottle.debug(True)

application = bottle.Bottle()


@application.route('/')
def handle():
    start = Ref('start', bottle.request.query.get('start', default_start))  # pylint: disable=no-member
    q = bottle.request.query.get('q', '')   # pylint: disable=no-member
    results = look(start, q)

    with (doc := dominate.document(title='Quickdoc')):
        with doc.head:
            h.link(rel='stylesheet', href='/basic.css')
            h.style(stylesheet)
        with h.form():
            if start != default_start:
                h.p(h.a('◀', _class='icon', href='?'), h.a(start.url, href=start.url))
            with h.p():
                h.input_(name='start', value=start.url, type='hidden')
                h.input_(name='q', value=q, autofocus=True)
                h.button('look')

        results_with_keys = zip(results, itertools.chain(keys, itertools.repeat(None)))
        for ((title, url), key) in results_with_keys:
            with h.p():
                with h.div():
                    if key:
                        h.kbd(key)
                    h.a('▸', _class='icon', href='?' + urlencode({'start': url}), accesskey=key)
                    h.a(title, href=file_to_http(url))
                h.div(h.small(url))

    return doc.render()


stylesheet = '''
body { font-size: larger }
.icon { text-decoration: none }
kbd { font-size: small }
'''


keys = 'qwerasdfzxc'


def look(start, q):
    context = [start]
    for step in q.split('!'):
        log.debug('look: context=%r', context)
        urls = set()
        scored = []
        for ctx in context:
            for ref in load(ctx.url):
                if ref.url in urls:
                    continue
                if not (sc := score(ctx, step, ref)):
                    continue
                urls.add(ref.url)
                scored.append((sc, ref))
        scored.sort(reverse=True)
        context = [ref for (_, ref) in scored]
    return context


Ref = namedtuple('Ref', ('title', 'url'))

default_start = f'file://{Path.home()}/my/setup/quickdoc.html'


def load(url):
    log.debug('load: url=%s', url)

    if url == haddock:
        return list_haddock()

    if url != (path := url.removeprefix('file://')) and (path := Path(path)).is_dir():
        return [Ref(str(p.relative_to(path)), f'file://{p}') for p in path.rglob('*.html')]

    try:
        resp = session.get(url, timeout=5)
        resp.raise_for_status()
        soup = bs4.BeautifulSoup(resp.text, 'html5lib')
    except Exception:
        log.exception('failed to load %s', url)
        return []

    items = []

    for a in soup.find_all('a'):
        if href := a.get('href'):
            items.append(Ref(a.get_text(strip=True), urljoin(resp.url, href)))

    return items


def score(ctx, step, ref):
    url = urlparse(ref.url.removeprefix(ctx.url))
    needles = extract_words(step)
    haystack = {w.lower()
                for item in (url.path, url.fragment, ref.title)
                for w in extract_words(item)}
    sc = []
    for needle in reversed(needles):
        for i, w in enumerate(haystack):
            k = len(haystack) - i
            if w == needle:
                sc.append((k, 0, 0))
            elif w.startswith(needle):
                sc.append((0, k, 0))
            elif needle in w:
                sc.append((0, 0, k))
            else:
                continue
            break
    if len(sc) < len(needles):
        return None
    sc.append(-len(ref.url))
    log.debug('matches: ref=%r, sc=%r, needles=%r, haystack=%r', ref, sc, needles, haystack)
    return sc


def extract_words(s):
    return [w.lower() for w in re.findall(r'\w+', s, re.UNICODE)]


session = tamala.Session(tamala_control=dict(heuristic_freshness=6 * 3600))
session.mount('file://', requests_file.FileAdapter())
session.headers['User-Agent'] = 'quickdoc python-requests'


paths = {
    '/pkgdoc/': (pkgdoc := Path('/usr/share/doc')),
    '/gtkdoc/': Path('/usr/share/gtk-doc'),
}

def file_to_http(url):
    if url != (path := url.removeprefix('file://')):
        path = Path(path)
        for p1, p2 in paths.items():
            if p2 in path.parents:
                return p1 + str(path.relative_to(p2))
    return url


haddock = 'urn:haddock'

def list_haddock():
    dirs = [d for d in (pkgdoc / 'ghc-doc/html/libraries').iterdir() if d.is_dir()]
    dirs += pkgdoc.glob('libghc-*-doc/html')
    for d in dirs:
        for f in d.iterdir():
            if f.suffix == '.html' and not f.stem.startswith(('index', 'doc-index')):
                module = '.'.join(f.stem.split('-'))
                yield Ref(module, f'file://{f}')
