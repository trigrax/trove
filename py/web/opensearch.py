import html

import bottle
import dominate
import dominate.tags as h

bottle.debug(True)

application = bottle.Bottle()


engines = {
    'quickdoc': ('Quickdoc', 'http://localhost:8000/quickdoc?q={searchTerms}'),
    'man': ('man', 'http://localhost:8000/cgi-bin/man/man2html?query={searchTerms}'),
    'vk': ('VK', 'https://vk.com/search?q={searchTerms}'),
}


@application.route('/')
def index():
    with (doc := dominate.document(title='Search engines')):
        with doc.head:
            for shortcut, (title, _) in engines.items():
                h.link(rel='search', type='application/opensearchdescription+xml', title=title, href=shortcut)
    return doc.render()


@application.route('/<shortcut>')
def description(shortcut):
    (title, url) = engines[shortcut]
    bottle.response.content_type = 'application/opensearchdescription+xml'
    return template.format(title=html.escape(title), url=html.escape(url), self=bottle.request.url)


template = '''
<OpenSearchDescription xmlns="http://a9.com/-/spec/opensearch/1.1/"
                       xmlns:moz="http://www.mozilla.org/2006/browser/search/">
  <ShortName>{title}</ShortName>
  <Description>{title}</Description>
  <InputEncoding>UTF-8</InputEncoding>
  <Url type="text/html" template="{url}"/>
  <Url type="application/opensearchdescription+xml" rel="self" template="{self}" />
</OpenSearchDescription>
'''
