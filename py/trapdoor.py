import argparse
import code
import gc
import io
import os
from pathlib import Path
import socket
import socketserver
import sys
import threading
import warnings

import xdg.BaseDirectory

import util.logs

log = util.logs.getLogger(__name__)

sock_root = Path(xdg.BaseDirectory.get_runtime_dir()) / 'trapdoor'


def run_server():
    warnings.filterwarnings('ignore', category=DeprecationWarning, message='`formatargspec`')
    sock_root.mkdir(exist_ok=True)
    sock = sock_root / str(os.getpid())
    if sock.exists():
        sock.unlink()
    Server.current = Server(str(sock), Handler, bind_and_activate=True)
    threading.Thread(target=Server.current.serve_forever, daemon=True).start()


class Server(socketserver.ThreadingMixIn, socketserver.UnixStreamServer):

    daemon_threads = True

    current = None
    selected_module = None
    current_wfiles = []


class Handler(socketserver.StreamRequestHandler):

    def handle(self):
        if module := Server.selected_module:
            Server.selected_module = None
            self.handle_module(module)
        else:
            self.handle_select()

    def handle_select(self):
        path = self.rfile.readline().decode().strip()
        log.debug('want: %s', path)
        probing = path != (path := path.lstrip('?'))
        path = path.replace('.scratch', '')
        for module in sys.modules.values():
            have = getattr(module, '__file__', None)
            log.debug('have: %s', have)
            if have == path:
                if probing:
                    main_path = sys.modules['__main__'].__file__    # pylint: disable=no-member
                    self.wfile.write(f'{main_path}\n'.encode())
                else:
                    Server.selected_module = module
                    self.wfile.write(b'OK\n')

    def handle_module(self, module):
        self.wfile = io.TextIOWrapper(self.wfile, write_through=True)   # pylint: disable=attribute-defined-outside-init
        self.rfile = io.TextIOWrapper(self.rfile, write_through=True)   # pylint: disable=attribute-defined-outside-init
        try:
            Server.current_wfiles.append(self.wfile)
            code.interact(local=module.__dict__, readfunc=self.readfunc)
        finally:
            Server.current_wfiles.remove(self.wfile)

    def readfunc(self, prompt=''):
        self.wfile.write(prompt)
        line = self.rfile.readline()
        if line == '':
            raise EOFError()
        return line.rstrip('\n')


class Output(io.RawIOBase):

    def __init__(self, f):
        super().__init__()
        self.f = f

    def write(self, b):
        r = self.f.write(b)
        for wfile in Server.current_wfiles:
            wfile.write(b)
        return r

    def flush(self):
        self.f.flush()

sys.stdout = Output(sys.stdout)
sys.stderr = Output(sys.stderr)


def run_client():
    parser = argparse.ArgumentParser()
    parser.add_argument('path', type=Path)
    path = parser.parse_args().path.resolve()

    available = []
    for sock in sock_root.iterdir():
        if name := ask(sock, path, probe=True):
            available.append((sock, name))
            print(len(available), sock.name, name, sep='\t')

    if r := (1 if len(available) == 1 else input('Whither? ')):
        (sock, _) = available[int(r) - 1]
        ask(sock, path)
        os.execlp('nc', 'nc', '-Uq0', str(sock))


def ask(sock, path, probe=False):
    try:
        with socket.socket(socket.AF_UNIX) as s:
            s.connect(str(sock).encode())
            log.debug('ask: connected to %s', sock)
            s.sendall((f'?{path}\n' if probe else f'{path}\n').encode())
            return s.recv(1024).decode().strip()
    except OSError:
        log.debug('cannot ask %s', sock, exc_info=True)
        return None


def update(module_file):
    updated = 0
    for module_name, module in sys.modules.items():
        if getattr(module, '__file__', None) == module_file:
            log.debug('update: %r %r', id(module), module)
            mapping = {(module_name, name): x
                       for name, x in module.__dict__.items()
                       if isinstance(x, type) and x.__module__ == module_name}
            for obj in gc.get_objects():
                key = (getattr(obj.__class__, '__module__', None), getattr(obj.__class__, '__name__', None))
                if new := mapping.get(key):
                    log.debug('update: %r: %r -> %r %r', id(obj), id(obj.__class__), id(new), obj)
                    if obj.__class__ is not new:
                        updated += 1
                        obj.__class__ = new
    return updated


if __name__ == '__main__':
    run_client()
else:
    run_server()
