#!/bin/bash -e

orig=$HOME/my/setup/ware/Adwaita-red.tar.gz
temp=$HOME/tmp/build-theme
dest=$HOME/.local/share/themes/Adwaita-red-x

echo "Extract $orig into $temp"
rm -rf $temp
mkdir -p $temp
cd $temp
tar -xzf $orig

echo "Replace radius"
sed -ri 's/radius:\s*[0-9]+[a-z]*/radius: 0/g' */*/*.css
sed -ri 's/radio \{ border-radius: 0/radio \{ border-radius: 100/g' */*/*.css

echo "Replace font-size"
sed -ri 's/font-size:\s*10pt/font-size: 12pt/g' */*/*.css
sed -ri 's/font-size:\s*11pt/font-size: 13pt/g' */*/*.css
sed -ri 's/font-size:\s*12pt/font-size: 14pt/g' */*/*.css

echo "Tweak destructive-action"
sed -ri '/destructive-action/ { s/\}/background-image: none; background-color: #D96600; border-color: #B35400; \}/ }' */*/*.css

echo "Remove index.theme"
rm */index.theme

echo "Move to $dest"
mkdir -p $(dirname $dest)
rm -rf $dest
mv * $dest
