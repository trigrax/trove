(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auth-source-save-behavior nil)
 '(auto-revert-avoid-polling t)
 '(auto-save-default nil)
 '(blink-cursor-blinks 0)
 '(blink-cursor-mode t)
 '(comint-input-ignoredups t)
 '(compilation-error-regexp-alist
   '(python-tracebacks-and-caml
     ("^ *\\(?1:\\(?2:[A-Za-z+./_-][0-9A-Za-z+./_-]*\\): *\\(?3:[0-9]+\\)\\(?:[:.] *\\(?4:[0-9]+\\)\\)?\\)\\([-0-9:.]+\\)?:" 2 3 4 nil 1)))
 '(compilation-message-face 'default)
 '(compilation-scroll-output 'first-error)
 '(completion-show-help nil)
 '(create-lockfiles nil)
 '(cursor-type '(bar . 1))
 '(dabbrev-case-fold-search nil)
 '(default-frame-alist
   '((right-divider-width . 1)
     (bottom-divider-width . 1)
     (left . 500)
     (top . 0)
     (width text-pixels . 866)
     (height text-pixels . 1200)))
 '(delete-selection-mode t)
 '(diff-font-lock-prettify t)
 '(diff-hl-draw-borders nil)
 '(diff-hl-flydiff-mode t)
 '(display-buffer-alist
   '(("\\*vc-diff\\*" display-buffer-full-frame)
     ("\\*\\(?:Messages\\|Warnings\\|Local Variables\\|Help\\|eldoc\\|Apropos\\|Backtrace\\|Disabled Command\\|Shell Command Output\\|Async Shell Command\\|grep\\|Occur\\|xref\\|compilation\\|haskell-compilation\\|requ \\|vc-dir\\|vc-change-log\\|pylint\\|shell\\|Python\\|haskell\\*\\|gud\\)"
      (display-buffer-in-side-window)
      (side . bottom)
      (window-height . 0.25))
     ("\\*Org Links"
      (display-buffer-in-side-window)
      (side . bottom)
      (window-height . 1)
      (slot . 2))))
 '(display-buffer-base-action
   '((display-buffer-reuse-window display-buffer-reuse-mode-window)))
 '(eglot-ignored-server-capabilities '(:documentHighlightProvider :foldingRangeProvider))
 '(eldoc-echo-area-use-multiline-p nil)
 '(epg-pinentry-mode 'loopback)
 '(even-window-sizes nil)
 '(expand-region-fast-keys-enabled nil)
 '(expand-region-show-usage-message nil)
 '(expand-region-subword-enabled t)
 '(frame-resize-pixelwise t)
 '(global-auto-revert-mode t)
 '(global-hl-todo-mode t)
 '(global-subword-mode t)
 '(go-fontify-function-calls nil)
 '(gofmt-args '("-s"))
 '(gofmt-show-errors 'echo)
 '(grep-use-null-device nil)
 '(hl-todo-include-modes '(prog-mode conf-mode))
 '(hl-todo-keyword-faces
   '(("TODO" . "#C75B18")
     ("FIXME" . "#FF6200")
     ("XXX" . "#C75B18")))
 '(indent-tabs-mode nil)
 '(inhibit-startup-screen t)
 '(isearch-allow-scroll 'unlimited)
 '(isearch-lazy-count t)
 '(js-indent-level 2)
 '(line-number-mode nil)
 '(make-backup-files nil)
 '(markdown-asymmetric-header t)
 '(markdown-command
   '("pandoc" "--from=markdown" "--to=html5" "--standalone" "--metadata" "title= " "--css" "/home/vasiliy/my/setup/share/pandoc.css"))
 '(markdown-disable-tooltip-prompt t)
 '(markdown-fontify-code-blocks-natively t)
 '(markdown-hide-urls t)
 '(markdown-nested-imenu-heading-index nil)
 '(menu-bar-mode nil)
 '(mode-line-compact 'long)
 '(mode-line-format
   '("%e" mode-line-front-space mode-line-modified mode-line-remote mode-line-frame-identification mode-line-buffer-identification "   " mode-line-position
     (vc-mode vc-mode)
     "  " mode-line-modes mode-line-misc-info mode-line-end-spaces))
 '(mode-line-percent-position '(-3 "%o"))
 '(mouse-wheel-progressive-speed nil)
 '(next-error-message-highlight 'keep)
 '(org-agenda-restore-windows-after-quit t)
 '(org-agenda-start-on-weekday nil)
 '(org-agenda-window-setup 'other-window)
 '(org-catch-invisible-edits 'error)
 '(org-edit-fixed-width-region-mode 'fundamental-mode)
 '(org-edit-src-content-indentation 0)
 '(org-emphasis-alist
   '(("*" bold)
     ("_" italic)
     ("=" org-verbatim verbatim)
     ("~" org-code verbatim)
     ("+" org-verbatim)))
 '(org-extend-today-until 3)
 '(org-file-apps
   '(("pdf" . system)
     ("djvu" . system)
     (auto-mode . emacs)
     (system . "xdg-open %s")))
 '(org-fold-catch-invisible-edits 'error)
 '(org-goto-interface 'outline-path-completion)
 '(org-hide-emphasis-markers t)
 '(org-highlight-links '(bracket plain tag date))
 '(org-level-color-stars-only t)
 '(org-link-search-must-match-exact-headline nil)
 '(org-log-repeat nil)
 '(org-modules nil)
 '(org-provide-todo-statistics nil)
 '(org-read-date-popup-calendar nil)
 '(org-replace-disputed-keys t)
 '(org-special-ctrl-a/e t)
 '(org-src-window-setup 'other-window)
 '(org-startup-folded t)
 '(org-startup-indented t)
 '(org-support-shift-select 'always)
 '(org-tags-column 0)
 '(org-use-effective-time t)
 '(org-use-tag-inheritance nil)
 '(org-yank-adjusted-subtrees t)
 '(package-archives
   '(("gnu" . "https://elpa.gnu.org/packages/")
     ("nongnu" . "https://elpa.nongnu.org/nongnu/")
     ("melpa" . "https://melpa.org/packages/")))
 '(package-selected-packages
   '(hcl-mode go-dlv hl-todo markdown-mode haskell-mode sparql-mode goto-chg move-text diff-hl expand-region reverse-im go-mode))
 '(pulse-delay 0.05)
 '(python-forward-sexp-function nil)
 '(python-shell-completion-native-enable nil)
 '(python-shell-dedicated 'buffer)
 '(python-shell-setup-codes '(python-shell-p python-shell-g python-shell-u))
 '(reverse-im-input-methods '("russian-computer" "greek"))
 '(reverse-im-mode t)
 '(revert-without-query '(".*"))
 '(ring-bell-function 'ignore)
 '(scroll-bar-mode nil)
 '(scroll-error-top-bottom t)
 '(server-kill-new-buffers nil)
 '(server-raise-frame nil)
 '(shell-command-prompt-show-cwd t)
 '(show-paren-mode nil)
 '(sql-product 'ms)
 '(tab-width 4)
 '(text-scale-mode-step 1.05)
 '(tool-bar-mode nil)
 '(truncate-lines t)
 '(uniquify-buffer-name-style 'post-forward nil (uniquify))
 '(vc-find-revision-no-save t)
 '(vc-git-print-log-follow t)
 '(vc-revert-show-diff 'kill)
 '(what-cursor-show-names t)
 '(window-divider-default-bottom-width 1)
 '(window-divider-default-places t)
 '(window-divider-default-right-width 1)
 '(window-divider-mode t)
 '(x-gtk-use-native-input t)
 '(xref-after-jump-hook '(xref-pulse-momentarily)))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(comint-highlight-prompt ((t (:foreground "dark magenta"))))
 '(compilation-line-number ((t nil)))
 '(compilation-mode-line-fail ((t (:inherit compilation-error :weight bold))))
 '(completions-first-difference ((t (:inherit highlight))))
 '(custom-group-tag ((t (:inherit variable-pitch :foreground "DodgerBlue3" :weight bold :height 1.2))))
 '(custom-group-tag-1 ((t (:inherit variable-pitch :foreground "dark magenta" :weight bold :height 1.2))))
 '(custom-variable-tag ((t (:foreground "DodgerBlue3" :weight bold))))
 '(custom-visibility ((t (:inherit link :height 0.85))))
 '(diff-hl-insert ((t (:foreground "green4" :background "#ccf0cc" :inherit diff-added))))
 '(dired-directory ((t (:foreground "DodgerBlue3"))))
 '(dired-header ((t (:foreground "#7b007b" :weight bold))))
 '(error ((t (:foreground "brown2" :weight bold))))
 '(escape-glyph ((t (:foreground "DodgerBlue3"))))
 '(fixed-pitch ((t (:family "DejaVu Sans Mono"))))
 '(flymake-warning ((t (:underline (:color "orange3" :style wave)))))
 '(font-lock-builtin-face ((t nil)))
 '(font-lock-comment-face ((t (:foreground "gray50"))))
 '(font-lock-constant-face ((t nil)))
 '(font-lock-doc-face ((t (:inherit font-lock-comment-face))))
 '(font-lock-function-name-face ((t (:inherit font-lock-type-face))))
 '(font-lock-keyword-face ((t (:weight bold))))
 '(font-lock-string-face ((t (:foreground "#ba3100"))))
 '(font-lock-type-face ((t (:foreground "#7b007b"))))
 '(font-lock-variable-name-face ((t nil)))
 '(fringe ((t (:background "gray95"))))
 '(highlight ((t (:background "blanched almond"))))
 '(homoglyph ((t (:foreground "DodgerBlue3"))))
 '(isearch ((t (:background "dark magenta" :foreground "white"))))
 '(italic ((t (:underline "dark gray"))))
 '(lazy-highlight ((t (:background "#ffe4c2"))))
 '(link ((t (:foreground "DodgerBlue3" :underline t))))
 '(match ((t (:inherit highlight))))
 '(minibuffer-prompt ((t (:foreground "DodgerBlue3"))))
 '(mode-line ((t (:family "Sans" :height 0.9 :box (:line-width (1 . 1) :color "gray13") :foreground "gainsboro" :background "gray13"))))
 '(mode-line-inactive ((t (:inherit mode-line :background "gainsboro" :foreground "grey20" :box (:line-width 1 :color "gainsboro")))))
 '(org-agenda-structure ((t (:foreground "#7b007b" :weight bold :height 1.1))))
 '(org-block ((t (:inherit org-verbatim))))
 '(org-code ((t (:inherit org-verbatim))))
 '(org-date ((t (:foreground "DeepPink4" :height 0.85))))
 '(org-done ((t (:foreground "ForestGreen" :weight bold :height 0.85))))
 '(org-hide ((t (:inherit fixed-pitch :foreground "white"))))
 '(org-meta-line ((t (:foreground "gray" :height 0.6))))
 '(org-quote ((t (:foreground "dark green"))))
 '(org-scheduled ((t nil)))
 '(org-scheduled-today ((t nil)))
 '(org-special-keyword ((t (:inherit font-lock-keyword-face :height 0.85))))
 '(org-tag ((t (:foreground "forest green" :weight bold :height 0.8))))
 '(org-todo ((t (:foreground "firebrick" :weight bold :height 0.85))))
 '(org-verbatim ((t (:inherit fixed-pitch :foreground "firebrick4"))))
 '(org-warning ((t (:foreground "firebrick"))))
 '(outline-1 ((t (:inherit fixed-pitch :foreground "#3c007c" :weight bold))))
 '(outline-2 ((t (:inherit outline-1 :foreground "#7c0065"))))
 '(outline-3 ((t (:inherit outline-2 :foreground "#7c0000"))))
 '(outline-4 ((t (:inherit outline-3 :foreground "#147c00"))))
 '(outline-5 ((t (:inherit outline-4 :foreground "#007c63"))))
 '(outline-6 ((t (:inherit outline-5 :foreground "#00407c"))))
 '(outline-7 ((t (:inherit outline-6 :foreground "#7a7a7a"))))
 '(outline-8 ((t (:inherit outline-7))))
 '(region ((t (:background "#dbf6ff" :distant-foreground "gtk_selection_fg_color"))))
 '(sh-quoted-exec ((t (:inherit font-lock-function-name-face))))
 '(success ((t (:foreground "SeaGreen4" :weight bold))))
 '(warning ((t (:foreground "dark orange" :weight bold))))
 '(window-divider ((t (:foreground "grey70")))))


(global-set-key (kbd "C-<backspace>") 'backward-delete-word)
(global-set-key (kbd "C-<delete>") 'delete-word)
(global-set-key (kbd "C-a") 'mark-whole-buffer)
(global-set-key (kbd "C-f") 'isearch-forward)
(define-key isearch-mode-map (kbd "C-f") 'isearch-repeat-forward)
(global-set-key (kbd "C-M-f") 'isearch-forward-regexp)
(global-set-key (kbd "C-h") 'query-replace)
(global-set-key (kbd "C-M-h") 'query-replace-regexp)
(global-set-key (kbd "C-s") 'save-buffer)
(global-set-key (kbd "C-w") 'kill-current-buffer)
(global-set-key (kbd "C-M-w") 'kill-buffer-and-window)
(global-set-key (kbd "C-o") 'find-file)
(global-set-key (kbd "C-M-o") 'find-file-at-point)
(global-set-key (kbd "C-z") 'undo)
(global-set-key (kbd "C-v") 'yank)
(global-set-key (kbd "C-b") 'switch-to-buffer)
(global-set-key (kbd "C-<prior>") 'previous-buffer)
(global-set-key (kbd "C-<next>") 'next-buffer)
(global-set-key (kbd "C-<tab>") 'other-window)
(global-set-key (kbd "C-<escape>") 'delete-window)
(global-set-key (kbd "<begin>") 'back-to-indentation)
(global-set-key (kbd "M-[") 'backward-sexp)
(global-set-key (kbd "M-]") 'forward-sexp)
(global-set-key (kbd "C-x r a") 'align-regexp)
(global-set-key (kbd "M-<kp-divide>") 'goto-last-change)
(global-set-key (kbd "M-<kp-multiply>") 'goto-last-change-reverse)
(global-set-key (kbd "C-=") 'er/expand-region)
(global-set-key (kbd "M-l") 'what-line)
(global-set-key (kbd "M-\\") 'dabbrev-expand)
(global-set-key (kbd "<f7>") 'compile)
(global-set-key (kbd "S-<f7>") 'recompile)
(global-set-key (kbd "<f8>") 'grep)
(global-set-key (kbd "C-`") 'next-error)

(define-prefix-command 'f9-map)
(global-set-key (kbd "<f9>") 'f9-map)
(global-set-key (kbd "<f9> v") 'vc-dir-root)
(global-set-key (kbd "<f9> <f7>") 'kill-compilation)
(global-set-key (kbd "<f9> l") 'org-store-link)

(move-text-default-bindings)

(global-unset-key (kbd "S-<down-mouse-1>"))
(global-set-key (kbd "S-<mouse-1>") 'mouse-save-then-kill)
(global-set-key (kbd "<mouse-3>") 'mouse-buffer-menu)
(global-set-key (kbd "M-<mouse-3>") 'imenu)

(add-to-list 'auto-mode-alist '("\\.sparql$" . sparql-mode))

(setenv "PAGER" "emacspager")


;; Enable commands

(put 'dired-find-alternate-file 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'scroll-left 'disabled nil)
(put 'upcase-region 'disabled nil)


;; Don't save deleted words to kill ring

(defun delete-word (arg)
  (interactive "p")
  (delete-region (point) (progn (forward-word arg) (point))))

(defun backward-delete-word (arg)
  (interactive "p")
  (delete-word (- arg)))


;; Editing miscellanea

(defun expand-region-to-lines ()
  (interactive)
  (unless (region-active-p) (push-mark nil nil t))
  (when (< (point) (mark)) (exchange-point-and-mark))
  (save-excursion
    (goto-char (mark))
    (beginning-of-line)
    (push-mark))
  (end-of-line)
  (when (char-after) (forward-char 1)))

(defun open-line-before (n)
  (interactive "p")
  (save-excursion (end-of-line 0) (open-line n)))

(defun open-line-after (n)
  (interactive "p")
  (save-excursion (end-of-line 1) (open-line n)))

(global-set-key (kbd "M-=") 'expand-region-to-lines)
(global-set-key (kbd "M-S-<up>") 'open-line-before)
(global-set-key (kbd "M-S-<down>") 'open-line-after)


;; Text

(add-hook 'text-mode-hook
          (lambda ()
            (visual-line-mode t)
            (toggle-truncate-lines 0)))


;; Org

(add-hook 'outline-mode-hook
          (lambda ()
            (define-key outline-mode-map (kbd "M-<backspace>") 'outline-up-heading)))

(add-hook 'org-mode-hook
          (lambda ()
            (variable-pitch-mode 1)
            (org-fold-core-set-folding-spec-property 'org-link :isearch-open nil)
            (org-fold-core-set-folding-spec-property 'org-link :isearch-ignore t)
            (define-key org-mode-map (kbd "C-<tab>") nil)
            (define-key org-mode-map [remap move-beginning-of-line] nil)
            (define-key org-mode-map (kbd "C-<up>") 'org-previous-visible-heading)
            (define-key org-mode-map (kbd "C-<down>") 'org-next-visible-heading)
            (define-key org-mode-map (kbd "<home>") 'move-beginning-of-line)
            (define-key org-mode-map (kbd "<begin>") 'org-beginning-of-line)
            (define-key org-mode-map (kbd "C-n") 'org-copy-subtree)
            (define-key org-mode-map (kbd "C-k") 'org-cut-subtree)
            (define-key org-mode-map (kbd "C-v") 'org-yank)
            (define-key org-mode-map (kbd "C-y") 'org-paste-subtree)
            (define-key org-mode-map (kbd "C-j") 'org-goto)
            (define-key org-mode-map (kbd "C-e") 'org-insert-link)
            (define-key org-mode-map (kbd "C-p") 'org-open-at-point)
            (define-key org-mode-map (kbd "C-S-<up>") 'org-backward-heading-same-level)
            (define-key org-mode-map (kbd "C-S-<down>") 'org-forward-heading-same-level)
            (define-key org-mode-map (kbd "C-c z") 'org-mark-ring-goto)
            (define-key org-mode-map (kbd "C-c a") 'org-agenda)))

(add-hook 'org-agenda-mode-hook 'variable-pitch-mode)

(advice-add #'org-insert-heading-respect-content :after (lambda (&rest _) (open-line-after 1)))


;; Shortcuts

(defun find-today ()
  (interactive)
  (find-file "~/today"))

(defun find-inbox ()
  (interactive)
  (find-file "~/my/current")
  (goto-line 3)
  (org-fold-show-children))

(global-set-key (kbd "<f9> <f9>") 'find-today)
(global-set-key (kbd "<f9> <f10>") 'find-inbox)


;; Windows

(require 'windmove)

(defun quit-side-windows (kill)
  (interactive "P")
  (walk-windows (lambda (w) (when (window-parameter w 'window-side) (quit-window kill w)))))

(global-set-key (kbd "<kp-insert>") 'quit-side-windows)
(global-set-key (kbd "M-<kp-insert>") 'window-toggle-side-windows)
(global-set-key (kbd "C-M-<up>") 'windmove-up)
(global-set-key (kbd "C-M-<right>") 'windmove-right)
(global-set-key (kbd "C-M-<down>") 'windmove-down)
(global-set-key (kbd "C-M-<left>") 'windmove-left)


;; Tab sizes

(add-hook 'emacs-lisp-mode-hook (lambda () (setq tab-width 8)))
(add-hook 'python-mode-hook (lambda () (setq tab-width 4)))
(add-hook 'js-mode-hook (lambda () (setq tab-width 2)))
(add-hook 'html-mode-hook (lambda () (setq tab-width 2)))


;; Kill file name

(defun kill-file-name (&optional with-line)
  (interactive "P")
  (let ((name (or (buffer-file-name) default-directory)))
    (when with-line (setq name (format "%s:%d" name (line-number-at-pos))))
    (kill-new name)
    (message "%s" name)))

(global-set-key (kbd "<kp-delete>") 'kill-file-name)


;; Dired

(add-hook 'dired-mode-hook
          (lambda ()
            (define-key dired-mode-map (kbd "C-t") nil)))


;; Compilation

(add-hook 'compilation-mode-hook (lambda () (setq-local process-connection-type nil))) ; don't use a pty


;; VC

(defun vc-refresh-all ()
  (interactive)
  (dolist (buf (buffer-list))
    (with-current-buffer buf
      (vc-refresh-state)
      (diff-hl-update))))

(global-set-key (kbd "<f9> V") 'vc-refresh-all)


;; diff-hl

(global-diff-hl-mode)
(global-set-key (kbd "M-<prior>") 'diff-hl-previous-hunk)
(global-set-key (kbd "M-<next>") 'diff-hl-next-hunk)


;; Eldoc

(defun eldoc-pop-to-browser ()
  (interactive)
  (with-current-buffer (eldoc-doc-buffer)
    (let* ((pos (previous-single-property-change (point-max) 'help-echo))
           (raw-url (get-text-property (- pos 1) 'help-echo))
           (url (string-replace "https://" "http://" raw-url)))
      (browse-url url))
    (when-let ((win (get-buffer-window)))
      (quit-window nil win))))

(global-set-key (kbd "<f9> d") 'eldoc-pop-to-browser)


;; Go to random line

(defun goto-random-line ()
  (interactive)
  (let ((beg (line-number-at-pos (if (use-region-p) (region-beginning) (point-min))))
        (end (line-number-at-pos (if (use-region-p) (region-end) (point-max)))))
    (push-mark)
    (deactivate-mark)
    (while (progn
             (goto-line (+ beg (random (- end beg))) nil t)
             (or
              (get-pos-property (point) 'invisible)
              ;; Skip lines that contain only whitespace
              (= (current-indentation) (- (line-end-position) (line-beginning-position))))))
    (pulse-momentary-highlight-one-line (point))))

(global-set-key (kbd "C-t") 'goto-random-line)


;; Hexl

(add-hook 'hexl-mode-hook
          (lambda ()
            (keymap-unset hexl-mode-map "C-b")
            (keymap-unset hexl-mode-map "C-o")
            (keymap-unset hexl-mode-map "C-w")))


;; Eglot

(require 'eglot)

(add-to-list 'eglot-stay-out-of 'flymake)

(add-to-list 'eglot-server-programs
             '(go-mode . ("gopls" :initializationOptions (:linkTarget "localhost:8011"))))

(add-hook 'eglot-managed-mode-hook
          (lambda ()
            (keymap-set eglot-mode-map "C-c f" 'eglot-format-buffer)
            (keymap-set eglot-mode-map "C-c i" 'eglot-code-action-organize-imports)
            (keymap-set eglot-mode-map "C-c r" 'eglot-rename)))


;; Python

(defun python-check-default ()
  (interactive)
  (python-check (concat python-check-command " " buffer-file-name)))

(defconst python-shell-p "\nfrom pprint import pprint as __p")
(defconst python-shell-g "\ndef __g(*args, **kwargs): import util.gtk; return util.gtk.idle_once(*args, **kwargs)")
(defconst python-shell-u "\ndef __u(*args, **kwargs): import trapdoor; return trapdoor.update(*args, **kwargs)")

(defun run-python-trapdoor (&optional show)
  (interactive '(t))
  (run-python (concat "python3 -m trapdoor " (buffer-file-name)) t show))

(defun python-shell-send-update-file ()
  (interactive)
  (python-shell-send-string "__u(__file__)" nil t))

(defun python-make-console (file)
  (select-frame-set-input-focus
   (make-frame '((left . 0) (top . 0) (width text-pixels . 465) (height text-pixels . 1200))))
  (find-file file)
  (text-scale-set -1) (toggle-truncate-lines 0) (toggle-word-wrap 1)
  (save-selected-window
    (run-python-trapdoor nil)
    (switch-to-buffer-other-window (python-shell-get-buffer))
    (text-scale-set -1) (toggle-truncate-lines 0) (toggle-word-wrap 1)))

(add-hook 'python-mode-hook
          (lambda ()
            (setq python-eldoc-get-doc nil)
            (define-key python-mode-map (kbd "C-c c") 'python-check-default)
            (define-key python-mode-map (kbd "C-c t") 'run-python-trapdoor)
            (define-key python-mode-map (kbd "C-c u") 'python-shell-send-update-file)))


;; Haskell

(add-hook 'haskell-mode-hook
          (lambda ()
            (define-key haskell-mode-map (kbd "<f6>") 'haskell-compile)
            (define-key haskell-mode-map (kbd "C-c c") 'haskell-mode-stylish-buffer)
            (define-key haskell-mode-map (kbd "C-c i") 'haskell-navigate-imports)))


;; Markdown

(require 'markdown-mode)

(defun markdown-export-file-name (&optional extension)
  (when (buffer-file-name)
    (make-directory "/tmp/markdown-export" t)
    (concat "/tmp/markdown-export/" (file-name-base (buffer-file-name)) (or extension ".html"))))


;; Requ

(defvar requ-other-buffer)

(defun requ ()
  (interactive)
  (let ((script)
        (response (generate-new-buffer "*response*"))
        (diag (generate-new-buffer "*requ diagnostics*")))
    (save-mark-and-excursion
      (save-restriction
        (narrow-to-page)

        ;; Extract the script.
        (goto-char (point-min))
        (mark-paragraph)
        (setq script (string-chop-newline
                      (replace-regexp-in-string "^[/#*-]+ *" "" (buffer-substring (point) (mark)))))
        (exchange-point-and-mark)
        (skip-chars-forward "\n")

        ;; Send the request.
        (let ((process (make-process
                        :name "requ"
                        :command (list "bash" "-o" "pipefail" "-c" script)
                        :connection-type 'pipe
                        :buffer response
                        :stderr diag
                        :sentinel #'requ-sentinel)))
          (process-send-region process (point) (point-max))
          (process-send-eof process))))

    (with-current-buffer response (set (make-local-variable 'requ-other-buffer) diag))
    (message "Request started")))

(defun requ-sentinel (process event)
  (message "Request %s" (string-chop-newline event))
  (let ((response (process-buffer process)))
    (when (buffer-live-p response)
      (let ((response-size (with-current-buffer response (buffer-size)))
            (diag (with-current-buffer response (get-buffer requ-other-buffer))))
        (mapc (lambda (buf) (with-current-buffer buf
                              (normal-mode)
                              (view-mode)
                              (goto-char (point-min))))
              (list response diag))
        (when (> response-size 0) (pop-to-buffer response))
        (unless (and (string= event "finished\n")
                     (> response-size 0))
          (pop-to-buffer diag))))))

(global-set-key (kbd "C-M-<kp-enter>") 'requ)
