from collections import namedtuple
from pathlib import Path

from util.strings import joinlines


def main():
    print(joinlines(make_lines(entry
                               for key, char in punct.items()
                               for entry in (Entry(('Multi_key', key), char, ...),
                                             Entry(('Multi_key', key, key), char, ...),
                                             Entry(('Multi_key', key, 'space'), char + ' ', ...)))))

    print(joinlines(make_lines(transformed
                               for line in Path('/usr/share/X11/locale/en_US.UTF-8/Compose').read_text().splitlines()
                               if (entry := parse_line(line)) and (transformed := transform(entry)))))


Entry = namedtuple('Entry', ('keys', 's', 'uniname'))


def parse_line(line):
    if not line.startswith('<'):
        return None

    (keys, s, uniname) = ([], None, None)
    for word in line.split():
        if uniname is not None:
            uniname += (' ' if uniname else '') + word
            continue
        if word.startswith('<'):
            keys.append(word.strip('<>'))
        if word.startswith('"'):
            s = word.strip('"')
        if word == '#':
            uniname = ''
    return Entry(tuple(keys), s, uniname)


def make_lines(entries):
    parts = [(' '.join(f'<{k}>' for k in e.keys), (f': "{e.s}"')) for e in entries]
    width = max(len(s) for (s, _) in parts)
    return [keysp.ljust(width) + ' ' + symp for (keysp, symp) in parts]


def transform(e):
    if e.keys[0] == 'Multi_key' and (c1 := punct.get(k1 := e.keys[1])) and (c2 := to_char(k2 := e.keys[2])):
        if k1 != k2 and (s := c1 + c2) not in {'—>'}:
            return Entry(e.keys[:3], s, ...)

    return None


punct = {
    'Cyrillic_BE': '«',
    'Cyrillic_YU': '»',
    'Cyrillic_be': '«',
    'Cyrillic_yu': '»',
    'apostrophe':  '’',
    'colon':       '“',
    'minus':       '—',
    'quotedbl':    '”',
}

chars = {
    'Cyrillic_A':    'А',
    'Cyrillic_E':    'Э',
    'Cyrillic_ER':   'Р',
    'Cyrillic_GHE':  'Г',
    'Cyrillic_I':    'И',
    'Cyrillic_IE':   'Е',
    'Cyrillic_KA':   'К',
    'Cyrillic_O':    'О',
    'Cyrillic_U':    'У',
    'Cyrillic_YA':   'Я',
    'Cyrillic_YERU': 'Ъ',
    'Cyrillic_YU':   'Ю',
    'Cyrillic_a':    'а',
    'Cyrillic_e':    'э',
    'Cyrillic_er':   'р',
    'Cyrillic_ghe':  'г',
    'Cyrillic_i':    'и',
    'Cyrillic_ie':   'е',
    'Cyrillic_ka':   'к',
    'Cyrillic_o':    'о',
    'Cyrillic_u':    'у',
    'Cyrillic_ya':   'я',
    'Cyrillic_yeru': 'ъ',
    'Cyrillic_yu':   'ю',
    'KP_Divide':     '/',
    'apostrophe':    '\'',
    'asciicircum':   '^',
    'asciitilde':    '~',
    'backslash':     '\\',
    'colon':         ':',
    'comma':         ',',
    'greater':       '>',
    'less':          '<',
    'minus':         '-',
    'parenleft':     '(',
    'parenright':    ')',
    'period':        '.',
    'plus':          '+',
    'quotedbl':      '"',
    'semicolon':     ';',
    'slash':         '/',
    'space':         ' ',
    'underscore':    '_',
}

def to_char(key):
    if len(key) == 1:
        return key
    return chars.get(key)


if __name__ == '__main__':
    main()
