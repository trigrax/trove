#!/usr/bin/python3

import argparse
import getpass
import os
from pathlib import Path
import shlex
import shutil
import subprocess
import sys

from util.strings import joinlines


diffs_store = Path.home() / 'my/setup/share/debtweak'
debs_store = Path.home() / 'my.bulk/debtweak'
staging_area = Path.home() / 'tmp/debtweak'


def main():
    if not os.environ.get('DEBUG'):
        sys.excepthook = excepthook

    args = parse_args()

    staging_area.mkdir(parents=True)
    chdir(staging_area)

    if '/' not in args.target and '=' not in args.target:
        args.target += '/' + release_codename()     # `apt-get source` ignores APT pinning
    run('apt-get', 'source', args.target)
    [stage] = [d for d in staging_area.iterdir() if d.is_dir()]
    chdir(stage)

    [package] = [fields[1]
                 for line in (stage / 'debian/control').read_text().splitlines()
                 if (fields := line.split()) and fields[0].title() == 'Source:']
    message(f'source package is {package}')

    version = ('--newversion', args.newversion) if args.newversion else ('--local', getpass.getuser())
    run('debchange', *version, 'Locally customized with debtweak.')

    if (diff := diffs_store / f'{package}.diff').exists():
        run('patch', '-p1', input=diff.read_bytes())

    prompt(f'edit the code in {stage}', 'continue')

    run('dpkg-source', '--commit', input=(patch_name := 'debtweak').encode(),
        env=dict(os.environ, EDITOR='true', VISUAL='true'))
    run('mk-build-deps', '--install', '--remove', '--root-cmd', 'sudo', 'debian/control')
    run('dpkg-buildpackage')
    run('sudo', 'apt-get', 'remove', '--auto-remove', '--purge', '--yes', f'{package}-build-deps')
    debs = list(staging_area.glob('*.deb'))
    run('sudo', 'dpkg', '--install', *filter_selected(debs))

    prompt('check the result', 'save it')

    diffs_store.mkdir(parents=True, exist_ok=True)
    patch = stage / 'debian/patches' / patch_name
    erase_patch_header(patch)
    move(patch, diff)

    debs_store.mkdir(parents=True, exist_ok=True)
    for deb in debs:
        move(deb, debs_store / deb.name)

    shutil.rmtree(staging_area)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--newversion', default=None)
    parser.add_argument('target')
    return parser.parse_args()


def chdir(d):
    message(f'entering directory: {d}')
    os.chdir(d)


def run(*args, **kwargs):
    args = [str(arg) for arg in args]
    message(f'executing: {shlex.join(args)}')
    return subprocess.run(args, check=True, **kwargs)


def prompt(action, proceed_to):
    print(f'-------- now {action}')
    print(f'-------- press Enter to {proceed_to}, Ctrl+C to abort')
    return input()


def move(src, dst):
    message(f'moving: {src} -> {dst}')
    src.rename(dst)


def release_codename():
    return run('lsb_release', '--codename', '--short', capture_output=True, text=True).stdout.split()[-1]


def filter_selected(debs):
    res = run('dpkg', '--get-selections', capture_output=True, text=True)
    selected = {line.split()[0].split(':')[0] for line in res.stdout.splitlines()}
    return [deb for deb in debs if deb.name.split('_')[0] in selected]


def erase_patch_header(f):
    lines = f.read_text().splitlines()
    for i, line in enumerate(lines):
        if line.startswith('--- '):
            lines = lines[i:]
            break
    f.write_text(joinlines(lines))


def excepthook(_type, exc, _traceback):
    if not isinstance(exc, KeyboardInterrupt):
        print('debtweak: fatal:', exc, file=sys.stderr)


def message(msg):
    print(f'\033[1mdebtweak: {msg}\033[0;0m')


if __name__ == '__main__':
    main()
